<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" type="image/png" href="{{ asset("img/favicon.ico") }}">
  <title>
    Pago con mercado Pago
  </title>
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- CSS Files -->
  <link href="{{ asset("css/argon-design-system.css?v=1.2.0") }} " rel="stylesheet" />
</head>

<body class="landing-page">
<!-- Navbar -->
<nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light headroom">
  <div class="container">
    <a class="navbar-brand mr-lg-5" href="https://emprendedoresconproposito.online">
      <img src="{{ asset("img/logo.png") }} ">
    </a>
  </div>
</nav>
<!-- End Navbar -->
<div class="wrapper">
  <div class="section section-hero section-shaped">
    <div class="shape shape-style-3 shape-default">
      <span class="span-150"></span>
      <span class="span-50"></span>
      <span class="span-50"></span>
      <span class="span-75"></span>
      <span class="span-100"></span>
      <span class="span-75"></span>
      <span class="span-50"></span>
      <span class="span-100"></span>
      <span class="span-50"></span>
      <span class="span-100"></span>
    </div>
    <div class="page-header">
      <div class="container shape-container d-flex align-items-center py-lg">
        <div class="col px-0">
          <div class="row align-items-center justify-content-center">
            <div class="col-lg-6 text-center">
              <h1 class="text-white display-1">Pagar con Mercado pago</h1>
              <h2 class="display-4 font-weight-normal text-white">Haga click para proceder a su compra</h2>
              <div class="btn-wrapper mt-4">
                <form action="/api/createRecibo/{{ $curso }}/{{ $user }}" method="POST">
                  <script
                    src="https://www.mercadopago.com.mx/integrations/v1/web-payment-checkout.js"
                    data-preference-id="{{ $preference->id }}">
                  </script>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="separator separator-bottom separator-skew zindex-100">
      <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="https://www.w3.org/2000/svg">
        <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
      </svg>
    </div>
  </div>
</div>
<script src="{{ asset("js/core/jquery.min.js") }} " type="text/javascript"></script>
<script src="{{ asset("js/core/popper.min.js") }} " type="text/javascript"></script>
<script src="{{ asset("js/core/bootstrap.min.js") }} " type="text/javascript"></script>
<script src="https://www.mercadopago.com/v2/security.js"></script>
</body>
</html>
