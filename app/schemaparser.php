<?php

if (!file_exists('database/tmp/schema.xml')) {
    echo 'No existe el archivo de esquema temporal';
    return false;
}

if (!file_exists('database/schema.xml')) {
    copy('database/tmp/schema.xml', "database/schema.xml");
    echo 'database/schema.xml creado';
    return false;
}

// files
$schemaTmp = simplexml_load_file("database/tmp/schema.xml");
if (!$schemaTmp) {
    echo 'schemaTmp error al cargar';
    return false;
}

$schemaDb = simplexml_load_file("database/schema.xml");
if (!$schemaDb) {
    echo 'schemaDb error al cargar';
    return false;
}
//Guardar behaviors existentes
$saved = array();
foreach ($schemaDb as $table) {
    foreach ($table->children() as $thing) {
        if ($thing->getName() == "behavior") {
            $parentTable = $thing->xpath("..")[0]->attributes()['name']->__toString();
            $arr = array($parentTable => $thing);
            array_push($saved, $arr);
        }
    }
}
//var_dump($thing);

//Retirar behaviors para comparacion
$schemaDbComp = preg_split("/\s{4}<behavior>\X+<\/behavior>\n/", $schemaDb->asXML());
$schemaDbComp = implode($schemaDbComp);
//var_dump($schemaDbComp);

if ($schemaTmp->asXML() == $schemaDbComp) {
    echo "sin cambios\n";
    //return true;
}

//Agrega los behaviors al nuevo schema (temp)
foreach ($schemaTmp as $table) {
    foreach ($saved as $beh) {
        if (array_key_exists("" . $table->attributes()['name']->__toString(), $beh)) {
            //var_dump($beh[$table->attributes()["name"]->__toString()]["name"]->__toString());
            $child = $table->addChild("behavior");
            $child->addAttribute("name", $beh[$table->attributes()["name"]->__toString()]["name"]->__toString());
            //$table->addChild("behavior", $beh[$table->attributes()['name']->__toString()]); //TODO Ajustar el salto de página y los espacios
            //$element->addAttribute("name",$beh[$table->attributes()['name']->__toString()]."");
        }
    }
}
//var_dump($beh);
//

$myfile = fopen('database/schema.xml', "w") or die("Unable to open file!");
fwrite($myfile, $schemaTmp->asXML());
fclose($myfile);

echo "!\n";


//--- Construir las clases para el modelo
function getAbr(String $word)
{
    $temp = substr($word, 0, 3);
    if ($temp == "tbl" || $temp == "cat") {
        return substr($word, strlen($word) - 6, strlen($word));
    } else {
        $fst = substr($word, "0", 1);
        $lst = substr($word, 1, strlen($word));
        $lst = str_replace("a", "", "" . $lst);
        $lst = str_replace("e", "", "" . $lst);
        $lst = str_replace("i", "", "" . $lst);
        $lst = str_replace("o", "", "" . $lst);
        $lst = str_replace("u", "", "" . $lst);
        $lst = $fst . substr($lst . "", 0, 2);
        while (strlen($lst) < 3) {
            $lst .= "x";
        }
        return $lst;
    }
}
$tblComments = array();

// MAIN
function makeTblCode($table, $dbName, $schemaTmp, &$tblComments)
{
    //$table = $schemaTmp->xpath("//table[@name='catentcls']")[0];
    $nameTbl = $table->attributes()['name']->__toString();
    if ($nameTbl == 'oauth_provider' || $nameTbl == 'migrations' || $nameTbl == 'password_resets' || $nameTbl == 'failed_jobs') return;
    $phpNameTbl = strtoupper(substr($nameTbl, 0, 1)) . substr($nameTbl, 1, strlen($nameTbl));

    $conn = mysqli_connect("localhost", "root", "", $dbName);
    $result = $conn->query("show full columns from " . $nameTbl . "");
    $camposNulos = array();
    $camposComments = array();
    $campoSize = array();
    $primaryPos = null;
    $uuidPos = null;
    $tableComment = "";
    $i = 0;
    while ($row = $result->fetch_row()) {
        array_push($camposNulos, $row[3]);
        array_push($camposComments, $row[8]);
        if ($row[4] == "PRI") {
            $primaryPos = $i;
        }
        if ($row[8] == "Uuid") {
            $uuidPos = $i;
        }
        array_push($campoSize, intval(preg_split("/\D+/", $row[1])[1]));
        $i++;
    }

    $result = $conn->query("show table status where NAME=\"" . $nameTbl . "\"");
    while ($row = $result->fetch_row()) {
        $tblComments += array($nameTbl => $row[17]);
    }

    if (is_null($primaryPos)) {
        echo "There isn't a primary key field in table " . $nameTbl . ".\nExit\n";
        return;
    }
    if (is_null($uuidPos)) {
        echo "There isn't a uuid field in table " . $nameTbl . ".\n";
        return;
    }
//var_dump($primaryPos);
    mysqli_close($conn);

    $tipoTbl = substr($nameTbl, 0, 3);
    if ($tipoTbl == "tbl" || $tipoTbl == "cat") {
        $sixletters = substr($nameTbl, strlen($nameTbl) - 6, strlen($nameTbl));
    } else {
        $sixletters = $nameTbl;
    }
    $abrTblName = getAbr($nameTbl);
    $camposName = array();
    $camposPhpName = array();
    $foraneas = array();
    $foraneasPhpName = array();

    $campoType = array();
//$campoSize = array(); TODO declarado arriba
    foreach ($table->children() as $element) {
        if ($element->getName() == "column") {
            $elName = $element->attributes()['name']->__toString();
            $elPhpName = $element->attributes()['phpName']->__toString();
            $elType = $element->attributes()['type']->__toString();
            array_push($camposName, $elName);
            array_push($camposPhpName, $elPhpName);
            array_push($campoType, $elType);
//        if(!is_null($element->attributes()['size'])) {
//            $elSize = $element->attributes()['size']->__toString();
//            array_push($campoSize, $elSize);
//        } TODO OBSOLETO
        }
        if ($element->getName() == "foreign-key") {
            //$foreignTbl = $element->attributes()['foreignTable']->__toString();
            //$refs = array();
            foreach ($element->children() as $ref) {
                //$foreign = $ref->attributes()['foreign']->__toString();
                $local = $ref->attributes()['local']->__toString();
                //$temparr = array("".$);
                array_push($foraneas, $local);
                $foreign = $table->xpath("//column[@name='$local']")[0]->attributes()['phpName']->__toString();
                array_push($foraneasPhpName, $foreign);
            }
        }
    }

//Obtener version corta de variables
    $abrCampos = array();
    for ($i = 0; $i < count($camposPhpName); $i++) {
        $abrCampo = getAbr($camposPhpName[$i]);
        $abrCampo = strtolower($abrCampo);
        array_push($abrCampos, $abrCampo);
        //print_r($i." : ".$camposPhpName[$i]." | ".$abrCampo." - ".$abrCampos[$i]."\n");
    }
// Revisar si no hay repeticion de variables
    for ($j = 0; $j < count($abrCampos); $j++) {
        for ($i = 0; $i < count($abrCampos); $i++) {
            if ($j != $i) {
                if ($abrCampos[$j] == $abrCampos[$i]) {
                    $abrCampos[$i] = $abrCampos[$i] . "x";
                }
            }
        }
    }

//Modelo
    $modelCode =
        "<?php

use Base\\" . $phpNameTbl . " as Base" . $phpNameTbl . ";

class " . $phpNameTbl . " extends Base" . $phpNameTbl . "
{
    public static function crt" . $sixletters . "(array \$data , \Propel\Runtime\Connection\ConnectionInterface \$connection = null)
    {
        \$" . $abrTblName . " = new \\" . $phpNameTbl . "();
        try{
            ";
    for ($i = 0; $i < count($camposName); $i++) {
        if ($i != $primaryPos) {
            if ($camposNulos[$i] == "YES") {
                $modelCode .=
                    "if(array_key_exists('" . $camposName[$i] . "', \$data)){
                if(!is_null(\$data['" . $camposName[$i] . "'])){
                    \$" . $abrTblName . "->set" . $camposPhpName[$i] . "(\$data['" . $camposName[$i] . "']);
                }
            }
            ";
            } else {
                $modelCode .=
                    "if(array_key_exists('" . $camposName[$i] . "', \$data)){
                if(!is_null(\$data['" . $camposName[$i] . "'])){
                    \$" . $abrTblName . "->set" . $camposPhpName[$i] . "(\$data['" . $camposName[$i] . "']);
                }else{
                    throw new \Propel\Runtime\Exception\PropelException('" . $camposName[$i] . " cannot be null');
                }
            }
            ";
            }
        }
    }
    $modelCode .=
        "\$" . $abrTblName . "->save(\$connection);
        } catch (\Propel\Runtime\Exception\PropelException \$e) {
            Illuminate\Support\Facades\Log::debug(\$e);
            return false;
        }
        return \$" . $abrTblName . ";
    }

    public static function rmv" . $sixletters . "($" . $camposName[0] . ", "
        . "\Propel\Runtime\Connection\ConnectionInterface \$connection = null)
    {
        $" . $abrTblName . " = \\" . $phpNameTbl . "Query::create()
            ->filterBy" . $camposPhpName[0] . "($" . $camposName[0] . ")
            ->findOne(\$connection);

        if(!$" . $abrTblName . ") return false;

        try {
            $" . $abrTblName . "->delete(\$connection);
        } catch (\Propel\Runtime\Exception\PropelException \$e) {
            Illuminate\Support\Facades\Log::debug(\$e);
            return false;
        }

        return true;
    }

    public static function upd" . $sixletters . "(array \$data , \Propel\Runtime\Connection\ConnectionInterface \$connection = null)
    {
        $" . $abrTblName . " = \\" . $phpNameTbl . "Query::create()
            ->filterBy" . $camposPhpName[$primaryPos] . "(\$data['" . $camposName[$primaryPos] . "'])
            ->findOne(\$connection);

        if(!$" . $abrTblName . ") return false;

        try{
            ";
    for ($i = 0; $i < count($camposName); $i++) {
        if ($i != $primaryPos) {
            if ($camposNulos[$i] == "YES") {
                $modelCode .=
                    "\$" . $abrTblName . "->set" . $camposPhpName[$i] . "(array_key_exists('" . $camposName[$i] . "', \$data) ? \$data['" . $camposName[$i] . "'] : null);
            ";
            } else {
                $modelCode .=
                    "if(array_key_exists('" . $camposName[$i] . "', \$data)){
                if(!is_null(\$data['" . $camposName[$i] . "'])){
                    \$" . $abrTblName . "->set" . $camposPhpName[$i] . "(\$data['" . $camposName[$i] . "']);
                }else{
                    throw new \Propel\Runtime\Exception\PropelException('" . $camposName[$i] . " cannot be null');
                }
            }
            ";
            }
        }
    }
    $modelCode .=
        "\$" . $abrTblName . "->save(\$connection);
        } catch (\Propel\Runtime\Exception\PropelException \$e) {
            Illuminate\Support\Facades\Log::debug(\$e);
            return false;
        }
            return $" . $abrTblName . ";
    }

    public static function dsp" . $sixletters . "(";
    for ($i = 0; $i < count($foraneas); $i++) {
        $modelCode .= "\$fil" . $foraneas[$i] . ", ";
    }
    $modelCode .= "\Propel\Runtime\Connection\ConnectionInterface \$connection = null)
    {
        \$all" . $abrTblName . " = \\" . $phpNameTbl . "Query::create();";
    for ($i = 0; $i < count($foraneas); $i++) {
        $modelCode .= "if(\$fil" . $foraneas[$i] . " != 0){
            \$all" . $abrTblName . " = \$all" . $abrTblName . "->filterBy" . $foraneasPhpName[$i] . "(\$fil" . $foraneas[$i] . ");
        }";
    }
    $modelCode .= "

        \$all" . $abrTblName . " = \$all" . $abrTblName . "->find();

        if(!\$all" . $abrTblName . ") return false;

        return \$all" . $abrTblName . ";
    }

    public static function fno" . $sixletters . "($" . $camposName[0] . ", ";
    $modelCode .= "\Propel\Runtime\Connection\ConnectionInterface \$connection = null)
    {
        $" . $abrTblName . " = \\" . $phpNameTbl . "Query::create()
            ->filterBy" . $camposPhpName[0] . "($" . $camposName[0] . ")
            ->findOne(\$connection);

        if(!$" . $abrTblName . ") return false;

        return $" . $abrTblName . ";
    }

    public static function fnu" . $sixletters . "(\$uuid,";
    $modelCode .= "\Propel\Runtime\Connection\ConnectionInterface \$connection = null)
    {
        $" . $abrTblName . " = \\" . $phpNameTbl . "Query::create()
            ->filterByUuid(\$uuid)
            ->findOne(\$connection);

        if(!$" . $abrTblName . ") return false;

        return $" . $abrTblName . ";
    }

    //TODO *CRUD Generator control separator line* (Don't remove this line!)";


    if (file_exists('database/generated-classes/' . $phpNameTbl . ".php")) {
        //TODO salvar el codigo personalizado
        $archivo = fopen('database/generated-classes/' . $phpNameTbl . ".php", "r") or die("Unable to open file!");
        $oldcode = "";
        while (($buffer = fgets($archivo)) !== false) {
            $oldcode .= $buffer;
        }
        $oldimports = explode("class", $oldcode)[0];
        $oldcode = preg_split("/\/\/TODO \*CRUD Generator control separator line\*\s\(Don't remove this line!\)/", $oldcode);

        $tmpCode = explode("class", $modelCode)[1];

        $modelCode = $oldimports . "class" . $tmpCode . $oldcode[1];
        //var_dump($modelCode);
    } else {
        $modelCode .= "\n}";
    }

    $archivo = fopen('database/generated-classes/' . $phpNameTbl . ".php", "w") or die("Unable to open file!");
    fwrite($archivo, $modelCode);
    fclose($archivo);
    echo "Modelo generado para '" . $phpNameTbl . "'" . PHP_EOL;

    if (!file_exists('app/Http/Controllers/' . $phpNameTbl . "Controller.php")) {

// Controlador
        $controllerCode =
            "<?php

namespace App\\Http\\Controllers;

use App\\ReturnHandler;
use App\\TransactionHandler;
use Illuminate\\Http\\Request;
use Illuminate\\Support\\Facades\\Validator;
use " . $phpNameTbl . ";

class " . $phpNameTbl . "Controller extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        // return view('app." . $phpNameTbl . ".main');
        \$entcls = " . $phpNameTbl . "::dsp" . $sixletters . "(0);
        \$entcls = array(\"data\" => \$entcls->toArray());

        return json_encode(\$entcls);
    }

    // store (C)
    public function create(Request \$request)
    {
        // 1.- Validacion del request TODO *Modificar*
        \$rules = [\n";
        for ($i = 0; $i < count($camposPhpName); $i++) {
            if ($camposComments[$i] == 'Internal') continue;
            if ($camposPhpName[$i] == 'Uuid') {
                $controllerCode .= "\t\t\t'" . $camposPhpName[$i] . "' => 'required|uuid|size:36',\n";
                continue;
            }
            $nullable = $camposNulos[$i] == 'YES' ? "nullable" : "required";
            if ($i != $primaryPos && $i != $uuidPos) {
                switch ($campoType[$i]) {
                    case 'BIGINT':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|uuid|size:36',\n";
                        break;
                    case 'SMALLINT':
                    case 'INTEGER':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|integer|min:0|max:" . str_pad("9", $campoSize[$i], "9") . "',\n";
                        break;
                    case 'DOUBLE':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|numeric|min:0|max:" . str_pad("9", $campoSize[$i], "9") . "',\n";
                        break;
                    case 'CLOB':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|json',\n";
                        break;
                    case 'LONGVARCHAR':
                    case 'CHAR':
                    case 'VARCHAR':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|max:" . $campoSize[$i] . "',\n";
                        break;
                    case 'DATE':
                    case 'TIMESTAMP':
                    case 'TIMESTAMPTZ':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|date_format:\"Y-m-d\TH:i:sO\"',\n";
                        break;
                    case 'BOOLEAN':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|boolean',\n";
                        break;
                    default:
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "',\n";
                        break;
                }
            }
        }
        $controllerCode .= "\t\t];

        \$msgs = [ // TODO *Customizable*\n";
        for ($i = 0; $i < count($camposPhpName); $i++) {
            if ($i == $primaryPos) continue;
            if ($camposComments[$i] == 'Internal') continue;

            if ($camposNulos[$i] == 'NO')
                $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".required' => 'Validacion fallada en " . $camposComments[$i] . ".required',\n";

            if ($camposPhpName[$i] == 'Uuid' || $campoType[$i] == 'BIGINT') {
                $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".uuid' => 'Uuid no válido',\n";
                $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".size' => 'Uuid no válido',\n";
            }

            if ($i != $primaryPos && $i != $uuidPos) {
                switch ($campoType[$i]) {
                    case 'SMALLINT':
                    case 'INTEGER':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".integer' => 'Validacion fallada en " . $camposComments[$i] . ".integer',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".min' => 'Validacion fallada en " . $camposComments[$i] . ".min',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".max' => 'Validacion fallada en " . $camposComments[$i] . ".max',\n";
                        break;
                    case 'DOUBLE':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".numeric' => 'Validacion fallada en " . $camposComments[$i] . ".numeric',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".min' => 'Validacion fallada en " . $camposComments[$i] . ".min',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".max' => 'Validacion fallada en " . $camposComments[$i] . ".max',\n";
                        break;
                    case 'CLOB':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".json' => 'Validacion fallada en " . $camposComments[$i] . ".json',\n";
                        break;
                    case 'LONGVARCHAR':
                    case 'CHAR':
                    case 'VARCHAR':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".string' => 'Validacion fallada en " . $camposComments[$i] . ".string',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".max' => 'Validacion fallada en " . $camposComments[$i] . ".max',\n";
                        break;
                    case 'DATE':
                    case 'TIMESTAMP':
                    case 'TIMESTAMPTZ':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".date_format' => 'Validacion fallada en " . $camposComments[$i] . ".date_format',\n";
                        break;
                    case 'BOOLEAN':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".boolean' => 'Validacion fallada en " . $camposComments[$i] . ".boolean',\n";
                        break;
                }
            }
        }

        $controllerCode .= "\t\t];

        \$validator = Validator::make(\$request->toArray(), \$rules, \$msgs)->errors()->all();

        if(!empty(\$validator)){
            return ReturnHandler::rtrerrjsn(\$validator[0]);
        }

        // 2.- Peticion a variables TODO *Modificar*
        \$timestamp = date(DATE_ISO8601);
        \$data = [
";
        for ($i = 0; $i < count($abrCampos); $i++) {
            if ($camposComments[$i] == 'Internal') continue;
            if ($i != $primaryPos) {
                if($campoType[$i] == 'BIGINT') {
                    $controllerCode .= "\t\t\t'uui" . substr($camposName[$i],3,strlen($camposName[$i])-3) . "' => request('" . $camposComments[$i] . "'),\n";
                    continue;
                }
                $controllerCode .= "\t\t\t'" . $camposName[$i] . "' => request('" . $camposComments[$i] . "'),\n";
            }
        }

        $controllerCode .=
            "\t\t\t'created_at' => \$timestamp\n".
            "\t\t];

        // 3.- Iniciar transaccion
        \$trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio TODO *Modificar*\n";

        for($k = 0; $k < count($camposName); $k++) {
            if ($k != $primaryPos) {
                if($campoType[$k] == "BIGINT") {
                    $tblsix = substr($camposName[$k], strlen($camposName[$k]) - 6, 6);
                    $tblxpath = $schemaTmp->xpath("//reference[@local='". $camposName[$k] ."']/..")[0];
                    $tblname = $tblxpath->attributes()['foreignTable']->__toString();
                    $tblname[0] = strtoupper($tblname[0]);
                    $ftblsixletters = $tblxpath->children()[0]->attributes()['foreign']->__toString();
                    $ftblsixletters = substr($ftblsixletters, strlen($ftblsixletters) - 6, 6);
                    $controllerCode .= "        \$" . $tblsix . " = \\" . $tblname . "::fnu" . $ftblsixletters . "(\$data['uui" . substr($camposName[$k], strlen($camposName[$k]) - 6, 6) . "'], \$trncnn);
        if(\$" . $tblsix . " instanceof \\" . $tblname . "){
            \$data['" . $camposName[$k] . "'] = \$" . $tblsix . "->getIdn" . $ftblsixletters . "();
        }\n";

                }
            }
        }

        $controllerCode .= "
        \$result = \\" . $phpNameTbl . "::crt" . $sixletters . "(\$data, \$trncnn);

        // 6.- Commit y return
        if(!\$result){
            TransactionHandler::rollback(\$trncnn);
            return ReturnHandler::rtrerrjsn('');
        }

        TransactionHandler::commit(\$trncnn);
        return ReturnHandler::rtrsccjsn('Guardado correctamente');
    }

    // update (U)
    public function update(Request \$request)
    {
        // 1.- Validacion del request TODO *Modificar*
        \$rules = [\n";
        for ($i = 0; $i < count($camposPhpName); $i++) {
            if ($camposComments[$i] == 'Internal') continue;
            if ($camposPhpName[$i] == 'Uuid') {
                $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => 'required|uuid|size:36',\n";
                continue;
            }
            $nullable = $camposNulos[$i] == 'YES' ? "nullable" : "required";
            if ($i != $primaryPos && $i != $uuidPos) {
                switch ($campoType[$i]) {
                    case "BIGINT":
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|uuid|size:36',\n";
                        break;
                    case 'SMALLINT':
                    case 'INTEGER':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|integer|min:0|max:" . str_pad("9", $campoSize[$i], "9") . "',\n";
                        break;
                    case 'DOUBLE':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|numeric|min:0|max:" . str_pad("9", $campoSize[$i], "9") . "',\n";
                        break;
                    case 'CLOB':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|json',\n";
                        break;
                    case 'LONGVARCHAR':
                    case 'CHAR':
                    case 'VARCHAR':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|max:" . $campoSize[$i] . "',\n";
                        break;
                    case 'DATE':
                    case 'TIMESTAMP':
                    case 'TIMESTAMPTZ':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|date_format:\"Y-m-d\TH:i:sO\"',\n";
                        break;
                    case 'BOOLEAN':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|boolean',\n";
                        break;
                    default:
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "',\n";
                        break;
                }
            }
        }
        $controllerCode .= "\t\t];

        \$msgs = [ // TODO *Customizable*\n";
        for ($i = 0; $i < count($camposPhpName); $i++) {
            if ($camposComments[$i] == 'Internal') continue;
            if ($camposPhpName[$i] == 'Uuid') {
                $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".required' => 'Validacion fallada en " . $camposComments[$i] . ".required',\n";
                $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".uuid' => 'Validacion fallada en " . $camposComments[$i] . ".uuid',\n";
                $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".size' => 'Validacion fallada en " . $camposComments[$i] . ".size',\n";
                continue;
            }
            if ($i != $primaryPos && $i != $uuidPos) {
                switch ($campoType[$i]) {
                    case 'BIGINT':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".uuid' => 'Validacion fallada en " . $camposComments[$i] . ".uuid',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".size' => 'Validacion fallada en " . $camposComments[$i] . ".size',\n";
                        break;
                    case 'SMALLINT':
                    case 'INTEGER':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".required' => 'Validacion fallada en " . $camposComments[$i] . ".required',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".integer' => 'Validacion fallada en " . $camposComments[$i] . ".integer',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".min' => 'Validacion fallada en " . $camposComments[$i] . ".min',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".max' => 'Validacion fallada en " . $camposComments[$i] . ".max',\n";
                        break;
                    case 'DOUBLE':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".required' => 'Validacion fallada en " . $camposComments[$i] . ".required',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".numeric' => 'Validacion fallada en " . $camposComments[$i] . ".numeric',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".min' => 'Validacion fallada en " . $camposComments[$i] . ".min',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".max' => 'Validacion fallada en " . $camposComments[$i] . ".max',\n";
                        break;
                    case 'CLOB':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".json' => 'Validacion fallada en " . $camposComments[$i] . ".json',\n";
                        break;
                    case 'LONGVARCHAR':
                    case 'CHAR':
                    case 'VARCHAR':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".required' => 'Validacion fallada en " . $camposComments[$i] . ".required',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".string' => 'Validacion fallada en " . $camposComments[$i] . ".string',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".max' => 'Validacion fallada en " . $camposComments[$i] . ".max',\n";
                        break;
                    case 'DATE':
                    case 'TIMESTAMP':
                    case 'TIMESTAMPTZ':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".required' => 'Validacion fallada en " . $camposComments[$i] . ".required',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".date_format' => 'Validacion fallada en " . $camposComments[$i] . ".date_format',\n";
                        break;
                    case 'BOOLEAN':
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => 'required|boolean',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".required' => 'Validacion fallada en " . $camposComments[$i] . ".required',\n";
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . ".boolean' => 'Validacion fallada en " . $camposComments[$i] . ".boolean',\n";
                        break;
                    default:
                        $controllerCode .= "\t\t\t'" . $camposComments[$i] . "' => 'required',\n";
                        break;
                }
            }
        }

        $controllerCode .= "
        ];

        \$validator = Validator::make(\$request->toArray(), \$rules, \$msgs)->errors()->all();

        if(!empty(\$validator)){
            return ReturnHandler::rtrerrjsn(\$validator[0]);
        }

        // 2.- Peticion a variables TODO *Modificar*
        \$uui". $sixletters . " = request('" . $camposPhpName[$uuidPos] . "');
";
        for ($i = 0; $i < count($camposName); $i++) {
            if ($i != $primaryPos) {
                if ($campoType[$i] == 'BIGINT') {
                    $controllerCode .= "\t\t\$uui" . substr($camposName[$i], 3, strlen($camposName[$i]) - 3) . " = request('" . $camposComments[$i] . "');\n";
                    continue;
                }
            }
        }

        $controllerCode .=
            "\t\t\$timestamp = date(DATE_ISO8601);

        // 3.- Iniciar Transaccion
        \$trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        \$" . $sixletters . " = \\" . $phpNameTbl . "::fnu" . $sixletters . "(\$uui" . $sixletters . ", \$trncnn);
        if(!\$" . $sixletters . " instanceof \\" . $phpNameTbl . "){
            TransactionHandler::rollback(\$trncnn);
            return ReturnHandler::rtrerrjsn('\$" . $sixletters . " false');
        }\n\n";
        $controllerCode .="\n\t\t\$data = [
";
        for ($i = 0; $i < count($abrCampos); $i++) {
            if ($camposComments[$i] == 'Internal') continue;
            if ($i == $primaryPos) {
                $controllerCode .= "\t\t\t'" . $camposName[$i] . "' => $" . $sixletters . "->get" . $camposPhpName[$primaryPos] . "(),\n";
            } else if ($i == $uuidPos) {
                $controllerCode .= "\t\t\t'" . $camposName[$i] . "' => $" . $sixletters . "->getUuid(),\n";
            } else if ($campoType[$i] == 'BIGINT') {
                continue;
            } else {
                $controllerCode .= "\t\t\t'" . $camposName[$i] . "' => request('" . $camposComments[$i] . "'),\n";
            }
        }
        $controllerCode .=
            "\t\t\t'updated_at' => \$timestamp\n".
            "        ];\n\n";


        for($k = 0; $k < count($camposName); $k++) {
            for ($k = 0; $k < count($camposName); $k++) {
                if ($k != $primaryPos) {
                    if ($campoType[$k] == "BIGINT") {
                        $tblsix = substr($camposName[$k], strlen($camposName[$k]) - 6, 6);
                        $tblxpath = $schemaTmp->xpath("//reference[@local='" . $camposName[$k] . "']/..")[0];
                        $tblname = $tblxpath->attributes()['foreignTable']->__toString();
                        $tblname[0] = strtoupper($tblname[0]);
                        $ftblsixletters = $tblxpath->children()[0]->attributes()['foreign']->__toString();
                        $ftblsixletters = substr($ftblsixletters, strlen($ftblsixletters) - 6, 6);
                        $controllerCode .= "        \$" . $tblsix . " = \\" . $tblname . "::fnu" . $ftblsixletters . "(\$uui" . $tblsix . ", \$trncnn);
        if(\$" . $tblsix . " instanceof \\" . $tblname . "){
            \$data['" . $camposName[$k] . "'] = \$" . $tblsix . "->getIdn" . $ftblsixletters . "();
        }\n";
                    }
                }
            }
        }


        $controllerCode .="        \$result = \\" . $phpNameTbl . "::upd" . $sixletters . "(\$data, \$trncnn);

        // 6.- Commit & return
        if(!\$result){
            TransactionHandler::rollback(\$trncnn);
            return ReturnHandler::rtrerrjsn('Ocurrió un error inesperado');
        }

        TransactionHandler::commit(\$trncnn);
        return ReturnHandler::rtrsccjsn('Actualizado correctamente');
    }

        // destroy (R)
    public function destroy(Request \$request)
    {
        // 1.- Validacion del request
        \$rules = [
            'Uuid' => 'required|uuid|size:36',
        ];

        \$msgs = [ // TODO *Customizable*
            'Uuid.required' => 'Objeto no válido',
            'Uuid.uuid' => 'Objeto no válido',
            'Uuid.size' => 'Objeto no válido',
        ];

        \$validator = Validator::make(\$request->toArray(), \$rules, \$msgs)->errors()->all();

        if(!empty(\$validator)){
            return ReturnHandler::rtrerrjsn(\$validator[0]);
        }

        // 2.- Request a variables
        \$uuid = request('Uuid');

        // 3.- Iniciar Transaccion
        \$trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        \$" . $sixletters . " = \\" . $phpNameTbl . "::fnu" . $sixletters . "(\$uuid, \$trncnn);
        if(!\$" . $sixletters . " instanceof \\" . $phpNameTbl . "){
            TransactionHandler::rollback(\$trncnn);
            return ReturnHandler::rtrerrjsn('\$" . $sixletters . " false');
        }

        \$result = \\" . $phpNameTbl . "::rmv" . $sixletters . "(\$" . $sixletters . "->get" . $camposPhpName[$primaryPos] . "(), \$trncnn);

        // 6.- Commit & return
        if(!\$result){
            TransactionHandler::rollback(\$trncnn);
            return ReturnHandler::rtrerrjsn('Ocurrió un inesperado');
        }

        TransactionHandler::commit(\$trncnn);
        return ReturnHandler::rtrsccjsn('Eliminado correctamente');

    }


// Views

    // Show table(D)
    public function table(Request \$request)
    {

    }

    // Display one(D)
    public function show(Request \$request)
    {
        \$id = request('id');
        \$" . $sixletters . " = " . $phpNameTbl . "::fno" . $sixletters . "(\$id);
        if(!\$" . $sixletters . ")
            return ReturnHandler::rtrerrjsn(\"\");
        \$" . $sixletters . " = array(\"data\" => $" . $sixletters . "->toArray());

        return json_encode(\$" . $sixletters . ");
    }

}
";


        $archivo = fopen('app/Http/Controllers/' . $phpNameTbl . "Controller.php", "w") or die("Unable to open file!");
        fwrite($archivo, $controllerCode);
        fclose($archivo);

        echo "Controlador generado para '" . $phpNameTbl . "'" . PHP_EOL;
    } else {
        $controllerOld = fopen('app/Http/Controllers/' . $phpNameTbl . "Controller.php", "r");
        $controllerNew = "";
        $times = 0;
        while (!feof($controllerOld)) {
            $line = fgets($controllerOld);
            $controllerNew .= $line;

            //Add Rules
            if (preg_match('/\$rules = \[/', $line) && $times < 6) {
                $times++;
                $line = fgets($controllerOld);
                $newKeys = $camposComments;
                while (!preg_match('/\];/', $line)) {
                    $key = explode('=>', preg_replace("/\s/", "", str_replace("'", "", $line)))[0];
                    for ($i = 0; $i < count($newKeys); $i++) {
                        if ($newKeys[$i] == $key) {
                            $newKeys[$i] = 'Internal';
                            //echo "unset: ".$key.PHP_EOL;
                        }
                    }

                    if (in_array($key, $camposComments))
                        $controllerNew .= $line;

                    $line = fgets($controllerOld);
                }

                for ($i = 0; $i < count($newKeys); $i++) {
                    if ($newKeys[$i] == 'Internal') continue;
                    if ($newKeys[$i] == 'Uuid') {
                        $controllerNew .= "\t\t\t'" . $camposPhpName[$i] . "' => 'required|uuid|size:36',\n";
                        continue;
                    }
                    $nullable = $camposNulos[$i] == 'YES' ? "nullable" : "required";
                    if ($i != $primaryPos && $i != $uuidPos) {
                        switch ($campoType[$i]) {
                            case 'BIGINT':
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|integer|min:0|max:" . str_pad("9", 18, "9") . "',\n";
                                break;
                            case 'SMALLINT':
                            case 'INTEGER':
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|integer|min:0|max:" . str_pad("9", $campoSize[$i], "9") . "',\n";
                                break;
                            case 'DOUBLE':
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|numeric|min:0|max:" . str_pad("9", $campoSize[$i], "9") . "',\n";
                                break;
                            case 'CLOB':
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|json',\n";
                                break;
                            case 'LONGVARCHAR':
                            case 'CHAR':
                            case 'VARCHAR':
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|max:" . $campoSize[$i] . "',\n";
                                break;
                            case 'DATE':
                            case 'TIMESTAMP':
                            case 'TIMESTAMPTZ':
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|date_format:\"Y-m-d\TH:i:sO\"',\n";
                                break;
                            case 'BOOLEAN':
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "|boolean',\n";
                                break;
                            default:
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . "' => '" . $nullable . "',\n";
                                break;
                        }
                    }

                }
                $controllerNew .= $line;

            }

            if (preg_match('/\$msgs = \[/', $line) && $times < 6) {
                $times++;
                $line = fgets($controllerOld);
                while (!preg_match('/\];/', $line)) {
                    $key = explode(".", explode('=>', preg_replace("/\s/", "", str_replace("'", "", $line)))[0])[0];
                    if (in_array($key, $camposComments))
                        $controllerNew .= $line;
                    $line = fgets($controllerOld);
                }
                for ($i = 0; $i < count($newKeys); $i++) {
                    if ($newKeys[$i] == 'Internal') continue;
                    if ($camposPhpName[$i] == 'Uuid') {
                        $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".required' => 'Validacion fallada en " . $camposComments[$i] . ".required',\n";
                        $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".uuid' => 'Validacion fallada en " . $camposComments[$i] . ".uuid',\n";
                        $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".size' => 'Validacion fallada en " . $camposComments[$i] . ".size',\n";
                        continue;
                    }
                    if ($i != $primaryPos && $i != $uuidPos) {
                        switch ($campoType[$i]) {
                            case 'SMALLINT':
                            case 'BIGINT':
                            case 'INTEGER':
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".required' => 'Validacion fallada en " . $camposComments[$i] . ".required',\n";
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".integer' => 'Validacion fallada en " . $camposComments[$i] . ".integer',\n";
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".min' => 'Validacion fallada en " . $camposComments[$i] . ".min',\n";
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".max' => 'Validacion fallada en " . $camposComments[$i] . ".max',\n";
                                break;
                            case 'DOUBLE':
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".required' => 'Validacion fallada en " . $camposComments[$i] . ".required',\n";
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".numeric' => 'Validacion fallada en " . $camposComments[$i] . ".numeric',\n";
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".min' => 'Validacion fallada en " . $camposComments[$i] . ".min',\n";
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".max' => 'Validacion fallada en " . $camposComments[$i] . ".max',\n";
                                break;
                            case 'CLOB':
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".json' => 'Validacion fallada en " . $camposComments[$i] . ".json',\n";
                                break;
                            case 'LONGVARCHAR':
                            case 'CHAR':
                            case 'VARCHAR':
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".required' => 'Validacion fallada en " . $camposComments[$i] . ".required',\n";
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".string' => 'Validacion fallada en " . $camposComments[$i] . ".string',\n";
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".max' => 'Validacion fallada en " . $camposComments[$i] . ".max',\n";
                                break;
                            case 'DATE':
                            case 'TIMESTAMP':
                            case 'TIMESTAMPTZ':
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".required' => 'Validacion fallada en " . $camposComments[$i] . ".required',\n";
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".date_format' => 'Validacion fallada en " . $camposComments[$i] . ".date_format',\n";
                                break;
                            case 'BOOLEAN':
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . "' => 'required|boolean',\n";
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".required' => 'Validacion fallada en " . $camposComments[$i] . ".required',\n";
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".boolean' => 'Validacion fallada en " . $camposComments[$i] . ".boolean',\n";
                                break;
                            default:
                                $controllerNew .= "\t\t\t'" . $camposComments[$i] . "' => 'required',\n";
                                break;
                        }
                        if ($camposNulos[$i] == 'YES')
                            $controllerNew .= "\t\t\t'" . $camposComments[$i] . ".nullable' => 'Validacion fallada en " . $camposComments[$i] . ".nullable',\n";
                    }
                }
                $controllerNew .= $line;
            }

            if (preg_match('/\$data = \[/', $line) && $times < 6) {
                $times++;
                $line = fgets($controllerOld);
                while (!preg_match('/\];/', $line)) {
                    $key = explode('=>', preg_replace("/\s/", "", str_replace("'", "", $line)))[0];
                    if (in_array($key, $camposName) || substr($key, 0, 3) == 'uui')
                        $controllerNew .= $line;
                    $line = fgets($controllerOld);
                }
                for ($i = 0; $i < count($newKeys); $i++) {
                    if ($newKeys[$i] == 'Internal') continue;
                    if ($i != $primaryPos) {
                        $controllerNew .= "\t\t\t'" . $camposName[$i] . "' => request('" . $camposComments[$i] . "'),\n";
                    }
                }
                $controllerNew .= $line;
            }

        }
        fclose($controllerOld);
        $archivo = fopen('app/Http/Controllers/' . $phpNameTbl . "Controller.php", "w") or die("Unable to open file!");
        fwrite($archivo, $controllerNew);
        fclose($archivo);
        echo "Modificaciones al Controlador para '" . $phpNameTbl . "'" . PHP_EOL;
    }
//Policies
    $policiesCode = "";
    $policiesCode .=
        "<?php

namespace App\\Policies;

use App\\User;
use " . $phpNameTbl . ";
use Illuminate\\Auth\\Access\\HandlesAuthorization;

class " . $phpNameTbl . "Policy
{
    use HandlesAuthorization;

    public function view(User \$user, " . $phpNameTbl . " \$" . $nameTbl . ")
    {
        // Update \$user authorization to view \$viehicle here.
        return true;
    }

    public function create(User \$user, " . $phpNameTbl . " \$" . $nameTbl . ")
    {
        // Update \$user authorization to view \$viehicle here.
        return true;
    }

    public function update(User \$user, " . $phpNameTbl . " \$" . $nameTbl . ")
    {
        // Update \$user authorization to view \$viehicle here.
        return true;
    }

    public function delete(User \$user, " . $phpNameTbl . " \$" . $nameTbl . ")
    {
        // Update \$user authorization to view \$viehicle here.
        return true;
    }
}";
    @mkdir('app/Policies');
    $archivo = fopen('app/Policies/' . $phpNameTbl . "Policy.php", "w") or die("Unable to open file!");
    fwrite($archivo, $policiesCode);
    fclose($archivo);

    echo "Politica generada para '" . $phpNameTbl . "'" . PHP_EOL;


//Testing
    if (!file_exists('tests/Unit/Models/' . $phpNameTbl . "Test.php")) {
        $unitTestingCode = "";
        $unitTestingCode .=
            "<?php

namespace Tests\Unit\Models;

use App\TransactionHandler;
use Faker;
use Tests\TestCase;
use Illuminate\Support\Facades\Log;
use Propel\Runtime\Exception\PropelException;

class " . $phpNameTbl . "UnitTest extends TestCase
{
    /** @test */
    public function it_can_crud_" . $phpNameTbl . "()
    {
//@CREATE
        \$faker = Faker\Factory::create();
        \$trncnn = TransactionHandler::begin();

        \$data = [\n";
        for ($i = 0; $i < count($camposName); $i++) {
            if ($i != $primaryPos) { // Evitar la llave primaria autoincrementable
                if ($campoType[$i] == 'CLOB') {
                    $unitTestingCode .= "\t\t\t'" . $camposName[$i] . "' => '{\"test\" : \"json\"}',\n";
                    continue;
                }
                if ($campoType[$i] == 'BIGINT') {
                    $unitTestingCode .= "\t\t\t'" . $camposName[$i] . "' => null,\n";
                    continue;
                }
                $unitTestingCode .= "\t\t\t'" . $camposName[$i] . "' => \$faker->";
                switch ($campoType[$i]) {
                    case 'SMALLINT':
                    case 'INTEGER':
                        $unitTestingCode .= "numberBetween(0, " . str_pad("9", $campoSize[$i], "9") . ")";
                        break;
                    case 'DOUBLE':
                        $unitTestingCode .= "randomFloat()";
                        break;
                    case 'LONGVARCHAR':
                    case 'VARCHAR':
                        $unitTestingCode .= "text(" . $campoSize[$i] . ")";
                        break;
                    case 'CHAR':
                        $unitTestingCode .= "text(" . $campoSize[$i] . ")";
                        break;
                    case 'DATE':
                    case 'TIMESTAMP':
                    case 'TIMESTAMPTZ':
                        $unitTestingCode .= "iso8601()";
                        break;
                    case 'BOOLEAN':
                        $unitTestingCode .= "boolean()";
                        break;
                }
                $unitTestingCode .= ",\n";
            }
        }
        $unitTestingCode .=
            "\t\t];
        \$result = \\" . $phpNameTbl . "::crt" . $sixletters . "(\$data, \$trncnn);
        self::assertInstanceOf(\\" . $phpNameTbl . "::class, \$result);\n";
        for ($i = 0; $i < count($camposName); $i++) {
            if ($i != $primaryPos) {
                if ($campoType[$i] == "TIMESTAMP") {
                    $unitTestingCode .=
                        "       try {\n";
                    $unitTestingCode .= "\t\t\tself::assertEquals(\$data['" . $camposName[$i] . "'], \$result->get" . $camposPhpName[$i];
                    $unitTestingCode .= "(DATE_ISO8601), 'Valor inserción no concuerda en " . $camposName[$i] . "');\n";
                    $unitTestingCode .=
                        "       } catch(PropelException \$e) {
            Log::debug(\$e);
            self::assertTrue(false, 'Formato inserción no concuerda: " . $camposName[$i] . "');
       }\n";
                } else {
                    $unitTestingCode .= "\t\tself::assertEquals(\$data['" . $camposName[$i] . "'], \$result->get" . $camposPhpName[$i];
                    $unitTestingCode .= "(), 'Valor inserción no concuerda en " . $camposName[$i] . "');\n";
                }
            }
        }
        $unitTestingCode .=
            "
//@UPDATE
        \$update = [\n";
        for ($i = 0; $i < count($camposName); $i++) {
            if ($i != $primaryPos) { // Evitar la llave primaria autoincrementable
                if ($camposNulos[$i] == 'NO') {
                    if ($campoType[$i] == 'CLOB') {
                        $unitTestingCode .= "\t\t\t'" . $camposName[$i] . "' => '{\"test\" : \"json\"}',\n";
                        continue;
                    }
                    if ($campoType[$i] == 'BIGINT') {
                        $unitTestingCode .= "\t\t\t'" . $camposName[$i] . "' => null,\n";
                        continue;
                    }
                    $unitTestingCode .= "\t\t\t'" . $camposName[$i] . "' => \$faker->";
                    switch ($campoType[$i]) {
                        case 'SMALLINT':
                        case 'INTEGER':
                            $unitTestingCode .= "numberBetween(0, " . str_pad("9", $campoSize[$i], "9") . ")";
                            break;
                        case 'DOUBLE':
                            $unitTestingCode .= "randomFloat()";
                            break;
                        case 'LONGVARCHAR':
                        case 'VARCHAR':
                            $unitTestingCode .= "text(" . $campoSize[$i] . ")";
                            break;
                        case 'CHAR':
                            $unitTestingCode .= "text(" . $campoSize[$i] . ")";
                            break;
                        case 'DATE':
                        case 'TIMESTAMP':
                        case 'TIMESTAMPTZ':
                            $unitTestingCode .= "iso8601()";
                            break;
                        case 'BOOLEAN':
                            $unitTestingCode .= "boolean()";
                            break;
                    }
                } else {
                    $unitTestingCode .= "\t\t\t'" . $camposName[$i] . "' => null";
                }
                $unitTestingCode .= ",\n";
            } else {
                $unitTestingCode .= "\t\t\t'" . $camposName[$i] . "' => \$result->get" . $camposPhpName[$i] . "(),\n";
            }
        }
        $unitTestingCode .=
            "\t\t];
        \$updated = \\" . $phpNameTbl . "::upd" . $sixletters . "(\$update, \$trncnn);
        self::assertInstanceOf(\\" . $phpNameTbl . "::class, \$updated);\n";
        for ($i = 0; $i < count($camposName); $i++) {
            $updatedVal = "\$update['" . $camposName[$i] . "']";
            if ($camposNulos[$i] == 'YES') $updatedVal = 'null';
            if ($i != $primaryPos) {
                if ($campoType[$i] == "TIMESTAMP") {
                    $unitTestingCode .=
                        "\t\ttry {\n";
                    $unitTestingCode .= "\t\t\tself::assertEquals(" . $updatedVal . ", \$updated->get" . $camposPhpName[$i];
                    $unitTestingCode .= "(DATE_ISO8601), 'Valor actualización no concuerda en " . $camposName[$i] . "');\n";
                    $unitTestingCode .=
                        "\t\t} catch(PropelException \$e) {
            Log::debug(\$e);
            self::assertTrue(false, 'Formato actualización no concuerda: " . $camposName[$i] . "');
        }\n";
                } else {
                    $unitTestingCode .= "\t\tself::assertEquals(" . $updatedVal . ", \$updated->get" . $camposPhpName[$i];
                    $unitTestingCode .= "(), 'Valor actualización no concuerda en " . $camposName[$i] . "');\n";
                }
            } else {
                $unitTestingCode .= "\t\tself::assertEquals(\$result->get" . $camposPhpName[$i] . "()," . "\$updated->get" . $camposPhpName[$i] . "(), 'Llaves actualización no concuerdan');\n";
            }
        }

        $unitTestingCode .=
            "
//@DISPLAY ONE CVE\n";

        $unitTestingCode .= "\t\t\$found = \\" . $phpNameTbl . "::fno" . $sixletters . "(\$updated->get" . $camposPhpName[$primaryPos] . "(),\$trncnn);
\t\tself::assertInstanceOf(\\" . $phpNameTbl . "::class, \$found);\n";
        for ($i = 0; $i < count($camposName); $i++) {
            if ($i != $primaryPos) {
                if ($campoType[$i] == "TIMESTAMP") {
                    $unitTestingCode .=
                        "\t\ttry {\n";
                    $unitTestingCode .= "\t\t\tself::assertEquals(\$update['" . $camposName[$i] . "'], \$found->get" . $camposPhpName[$i];
                    $unitTestingCode .= "(DATE_ISO8601), 'Valor búsqueda no concuerda en " . $camposName[$i] . "');\n";
                    $unitTestingCode .=
                        "\t\t} catch(PropelException \$e) {
            Log::debug(\$e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: " . $camposName[$i] . "');
        }\n";
                } else {
                    $unitTestingCode .= "\t\tself::assertEquals(\$update['" . $camposName[$i] . "'], \$found->get" . $camposPhpName[$i];
                    $unitTestingCode .= "(), 'Valor búsqueda no concuerda en " . $camposName[$i] . "');\n";
                }
            } else {
                $unitTestingCode .= "\t\tself::assertEquals(\$result->get" . $camposPhpName[$i] . "()," . "\$found->get" . $camposPhpName[$i] . "(), 'Llaves búsqueda no concuerdan');\n";
            }
        }
        $unitTestingCode .=
            "
//@DISPLAY ONE UUID\n";

        $unitTestingCode .= "\t\t\$foundU = \\" . $phpNameTbl . "::fnu" . $sixletters . "(\$updated->getUuid(),\$trncnn);
\t\tself::assertInstanceOf(\\" . $phpNameTbl . "::class, \$foundU);\n";
        for ($i = 0; $i < count($camposName); $i++) {
            if ($i != $primaryPos) {
                if ($campoType[$i] == "TIMESTAMP") {
                    $unitTestingCode .=
                        "\t\ttry {\n";
                    $unitTestingCode .= "\t\t\tself::assertEquals(\$update['" . $camposName[$i] . "'], \$foundU->get" . $camposPhpName[$i];
                    $unitTestingCode .= "(DATE_ISO8601), 'Valor búsqueda uuid no concuerda en " . $camposName[$i] . "');\n";
                    $unitTestingCode .=
                        "\t\t} catch(PropelException \$e) {
            Log::debug(\$e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: " . $camposName[$i] . "');
        }\n";
                } else {
                    $unitTestingCode .= "\t\tself::assertEquals(\$update['" . $camposName[$i] . "'], \$foundU->get" . $camposPhpName[$i];
                    $unitTestingCode .= "(), 'Valor búsqueda uuid no concuerda en " . $camposName[$i] . "');\n";
                }
            } else {
                $unitTestingCode .= "\t\tself::assertEquals(\$result->get" . $camposPhpName[$i] . "()," . "\$foundU->get" . $camposPhpName[$i] . "(), 'Llaves búsqueda uuid no concuerdan');\n";
            }
        }

        $unitTestingCode .=
            "
//@DISPLAY ALL\n";

        $unitTestingCode .= "\t\t\$all = \\" . $phpNameTbl . "::dsp" . $sixletters . "(";
        for ($i = 0; $i < count($foraneas); $i++) {
            $unitTestingCode .= "0, ";
        }
        $unitTestingCode .= "\$trncnn);\n" .
            "\t\tself::assertInstanceOf(\Propel\Runtime\Collection\Collection::class, \$all);\n" .
            "\t\tself::assertTrue(\$all->count() > 0);\n";

        $unitTestingCode .=
            "
//@REMOVE ONE\n" .
            "\t\t\$destroyed = \\" . $phpNameTbl . "::rmv" . $sixletters . "(\$result->get" . $camposName[$primaryPos] . "(),\$trncnn);\n" .
            "\t\tself::assertTrue(\$destroyed);\n";

        $unitTestingCode .=
            "        TransactionHandler::rollback(\$trncnn);
    }
}";

        if (!file_exists('tests/Unit/Models')) {
            mkdir('test/Unit');
            mkdir('tests/Unit/Models');
        }

        $archivo = fopen('tests/Unit/Models/' . $phpNameTbl . "Test.php", "w") or die("Unable to open file!");
        fwrite($archivo, $unitTestingCode);
        fclose($archivo);
    } else {
        $unitTestOld = fopen('tests/Unit/Models/' . $phpNameTbl . "Test.php", "r");
        $unitTestNew = "";
        $times = 0;

        while (!feof($unitTestOld)) {
            $line = fgets($unitTestOld);
            $unitTestNew .= $line;
            if (preg_match('"//@CREATE"', $line)) {
                while (!preg_match('"//@UPDATE"', $line)) {
                    if (preg_match('/\$data = \[/', $line)) {
                        $unitTestNew .= $line;
                        $line = fgets($unitTestOld);
                        $newKeys = $camposName;
                        while (!preg_match('/\];/', $line)) {
                            $key = explode('=>', preg_replace("/\s/", "", str_replace("'", "", $line)))[0];
                            for ($i = 0; $i < count($newKeys); $i++) {
                                if ($newKeys[$i] == $key) {
                                    $newKeys[$i] = 'Internal';
                                }
                            }
                            if (in_array($key, $camposName))
                                $unitTestNew .= $line;

                            $line = fgets($unitTestOld);
                        }

                        for ($i = 0; $i < count($camposName); $i++) {
                            if ($newKeys[$i] == 'Internal') continue;
                            if ($i != $primaryPos) { // Evitar la llave primaria autoincrementable
                                if ($campoType[$i] == 'CLOB') {
                                    $unitTestNew .= "\t\t\t'" . $camposName[$i] . "' => '{\"test\" : \"json\"}',\n";
                                    continue;
                                }
                                if ($campoType[$i] == 'BIGINT') {
                                    $unitTestNew .= "\t\t\t'" . $camposName[$i] . "' => null,\n";
                                    continue;
                                }
                                $unitTestNew .= "\t\t\t'" . $camposName[$i] . "' => \$faker->";
                                switch ($campoType[$i]) {
                                    case 'SMALLINT':
                                    case 'INTEGER':
                                        $unitTestNew .= "numberBetween(0, " . str_pad("9", $campoSize[$i], "9") . ")";
                                        break;
                                    case 'DOUBLE':
                                        $unitTestNew .= "randomFloat()";
                                        break;
                                    case 'LONGVARCHAR':
                                    case 'VARCHAR':
                                        $unitTestNew .= "text(" . $campoSize[$i] . ")";
                                        break;
                                    case 'CHAR':
                                        $unitTestNew .= "text(" . $campoSize[$i] . ")";
                                        break;
                                    case 'DATE':
                                    case 'TIMESTAMP':
                                    case 'TIMESTAMPTZ':
                                        $unitTestNew .= "iso8601()";
                                        break;
                                    case 'BOOLEAN':
                                        $unitTestNew .= "boolean()";
                                        break;
                                }
                                $unitTestNew .= ",\n";
                            }
                        }

                    }

                    if ((preg_match('/assertInstanceOf/', $line))) {
                        while (!preg_match('"//@UPDATE"', $line)) {
                            $block = "";
                            $assert = "";
                            if (preg_match('"try"', $line)) {
                                while (!preg_match('"\s}(\s)*$"', $line)) {
                                    if (preg_match('"assertEquals"', $line)) {
                                        $assert = $line;
                                    }
                                    $block .= $line;
                                    $line = fgets($unitTestOld);
                                }
                                $block .= $line;
                            } else {
                                $assert = $line;
                                $block = $line;
                            }

                            $key1 = explode(',', str_replace("']", "", $assert));
                            if (count($key1) > 1 && preg_match('/\'/', $key1[0])) {
                                $key = explode("'", $key1[0])[1];
                                if (in_array($key, $camposName))
                                    $unitTestNew .= $block;
                            }

                            $line = fgets($unitTestOld);
                        }
                        for ($i = 0; $i < count($newKeys); $i++) {
                            if ($newKeys[$i] == "Internal") continue;
                            if ($i != $primaryPos)
                                $unitTestNew .= '        self::assertEquals($data[' . $camposName[$i] . '], $result->getCajarcpry(), \'Valor inserción no concuerda en ' . $camposName[$i] . '\');' . PHP_EOL;
                        }
                    }
                    if (!preg_match('"//@UPDATE"', $line)) {
                        if (!preg_match('"//@CREATE"', $line))
                            $unitTestNew .= $line;
                        $line = fgets($unitTestOld);
                    }
                }
            }

            if (preg_match('"//@UPDATE"', $line)) {
                $unitTestNew .= $line;
                $line = fgets($unitTestOld);
                while (!preg_match('"//@DISPLAY"', $line)) {
                    if (preg_match('/\$update = \[/', $line)) {
                        $unitTestNew .= $line;
                        $line = fgets($unitTestOld);
                        $newKeys = $camposName;
                        while (!preg_match('/\];/', $line)) {
                            $key = explode('=>', preg_replace("/\s/", "", str_replace("'", "", $line)))[0];
                            for ($i = 0; $i < count($newKeys); $i++) {
                                if ($newKeys[$i] == $key) {
                                    $newKeys[$i] = 'Internal';
                                }
                            }
                            if (in_array($key, $camposName))
                                $unitTestNew .= $line;

                            $line = fgets($unitTestOld);
                        }

                        for ($i = 0; $i < count($newKeys); $i++) {
                            if ($i != $primaryPos) { // Evitar la llave primaria autoincrementable
                                if ($newKeys[$i] == 'Internal') continue;
                                if ($camposNulos[$i] == 'NO') {
                                    if ($campoType[$i] == 'CLOB') {
                                        $unitTestNew .= "\t\t\t'" . $camposName[$i] . "' => '{\"test\" : \"json\"}',\n";
                                        continue;
                                    }
                                    if ($campoType[$i] == 'BIGINT') {
                                        $unitTestNew .= "\t\t\t'" . $camposName[$i] . "' => null,\n";
                                        continue;
                                    }
                                    $unitTestNew .= "\t\t\t'" . $camposName[$i] . "' => \$faker->";
                                    switch ($campoType[$i]) {
                                        case 'SMALLINT':
                                        case 'INTEGER':
                                            $unitTestNew .= "numberBetween(0, " . str_pad("9", $campoSize[$i], "9") . ")";
                                            break;
                                        case 'DOUBLE':
                                            $unitTestNew .= "randomFloat()";
                                            break;
                                        case 'LONGVARCHAR':
                                        case 'VARCHAR':
                                            $unitTestNew .= "text(" . $campoSize[$i] . ")";
                                            break;
                                        case 'CHAR':
                                            $unitTestNew .= "text(" . $campoSize[$i] . ")";
                                            break;
                                        case 'DATE':
                                        case 'TIMESTAMP':
                                        case 'TIMESTAMPTZ':
                                            $unitTestNew .= "iso8601()";
                                            break;
                                        case 'BOOLEAN':
                                            $unitTestNew .= "boolean()";
                                            break;
                                    }
                                } else {
                                    $unitTestNew .= "\t\t\t'" . $camposName[$i] . "' => null";
                                }
                                $unitTestNew .= ",\n";
                            }
                        }
                    }

                    if ((preg_match('/assertInstanceOf/', $line))) {
                        while (!preg_match('"//@DISPLAY"', $line)) {
                            $block = "";
                            $assert = "";
                            if (preg_match('"try"', $line)) {
                                while (!preg_match('"\s}(\s)*$"', $line)) {
                                    if (preg_match('"assertEquals"', $line)) {
                                        $assert = $line;
                                    }
                                    $block .= $line;
                                    $line = fgets($unitTestOld);
                                }
                                $block .= $line;
                            } else {
                                $assert = $line;
                                $block = $line;
                            }

                            $key1 = preg_split('"\((DATE_ISO8601)*\)"', $assert);
                            if (count($key1) > 1 && preg_match('"get"', $key1[0])) {
                                if (in_array(strtolower($key), $camposName))
                                    $unitTestNew .= $block;
                            } else {
                                $unitTestNew .= $line;
                            }
                            $line = fgets($unitTestOld);
                        }
                        for ($i = 0; $i < count($newKeys); $i++) {
                            if ($newKeys[$i] == "Internal") continue;
                            $updatedVal = "\$update['" . $camposName[$i] . "']";
                            if ($camposNulos[$i] == 'YES') $updatedVal = 'null';
                            if ($i != $primaryPos) {
                                if ($campoType[$i] == "TIMESTAMP") {
                                    $unitTestNew .=
                                        "\t\ttry {\n";
                                    $unitTestNew .= "\t\t\tself::assertEquals(" . $updatedVal . ", \$updated->get" . $camposPhpName[$i];
                                    $unitTestNew .= "(DATE_ISO8601), 'Valor actualización no concuerda en " . $camposName[$i] . "');\n";
                                    $unitTestNew .=
                                        "\t\t} catch(PropelException \$e) {
            Log::debug(\$e);
            self::assertTrue(false, 'Formato actualización no concuerda: " . $camposName[$i] . "');
        }\n";
                                } else {
                                    $unitTestNew .= "\t\tself::assertEquals(" . $updatedVal . ", \$updated->get" . $camposPhpName[$i];
                                    $unitTestNew .= "(), 'Valor actualización no concuerda en " . $camposName[$i] . "');\n";
                                }
                            } else {
                                $unitTestNew .= "\t\tself::assertEquals(\$result->get" . $camposPhpName[$i] . "()," . "\$updated->get" . $camposPhpName[$i] . "(), 'Llaves actualización no concuerdan');\n";
                            }
                        }
                    }

                    if (!preg_match('"//@DISPLAY"', $line)) {
                        $unitTestNew .= $line;
                        $line = fgets($unitTestOld);
                    }
                }

            }

            if (preg_match('"//@DISPLAY ONE CVE"', $line)) {
                $unitTestNew .= $line;
                $line = fgets($unitTestOld);
                while (!preg_match('"//@DISPLAY ONE UUID"', $line)) {
                    if ((preg_match('/assertEquals\(\$result/', $line))) {
                        $unitTestNew .= $line;
                        $line = fgets($unitTestOld);
                        while (!preg_match('"//@DISPLAY"', $line) ) {
                            $block = "";
                            $assert = "";
                            if (preg_match('"try"', $line)) {
                                while (!preg_match('"\s}(\s)*$"', $line)) {
                                    if (preg_match('"assertEquals"', $line)) {
                                        $assert = $line;
                                    }
                                    $block .= $line;
                                    $line = fgets($unitTestOld);
                                }
                                $block .= $line;
                            } else {
                                $assert = $line;
                                $block = $line;
                            }
                            $key1 = explode(',', str_replace("']", "", $assert));
                            if (count($key1) > 1 && preg_match("/'/", $key1[0])) {
                                $key = explode("'", $key1[0])[1];
                                if (in_array(strtolower($key), $camposName))
                                    $unitTestNew .= $block;
                            } else {
                                $unitTestNew .= $line;
                            }
                            $line = fgets($unitTestOld);
                        }
                        for ($i = 0; $i < count($newKeys); $i++) {
                            if ($newKeys[$i] == "Internal") continue;
                            $updatedVal = "\$update['" . $camposName[$i] . "']";
                            if ($camposNulos[$i] == 'YES') $updatedVal = 'null';
                            if ($i != $primaryPos) {
                                if ($campoType[$i] == "TIMESTAMP") {
                                    $unitTestNew .=
                                        "\t\ttry {\n";
                                    $unitTestNew .= "\t\t\tself::assertEquals(" . $updatedVal . ", \$updated->get" . $camposPhpName[$i];
                                    $unitTestNew .= "(DATE_ISO8601), 'Valor actualización no concuerda en " . $camposName[$i] . "');\n";
                                    $unitTestNew .=
                                        "\t\t} catch(PropelException \$e) {
            Log::debug(\$e);
            self::assertTrue(false, 'Formato actualización no concuerda: " . $camposName[$i] . "');
        }\n";
                                } else {
                                    $unitTestNew .= "\t\tself::assertEquals(" . $updatedVal . ", \$updated->get" . $camposPhpName[$i];
                                    $unitTestNew .= "(), 'Valor actualización no concuerda en " . $camposName[$i] . "');\n";
                                }
                            } else {
                                $unitTestNew .= "\t\tself::assertEquals(\$result->get" . $camposPhpName[$i] . "()," . "\$updated->get" . $camposPhpName[$i] . "(), 'Llaves actualización no concuerdan');\n";
                            }
                        }
                    }

                    if (!preg_match('"//@DISPLAY ONE UUID"', $line)) {
                        $unitTestNew .= $line;
                        $line = fgets($unitTestOld);
                    }
                }
            }

            if (preg_match('"//@DISPLAY ONE UUID"', $line)) {
                $unitTestNew .= $line;
                $line = fgets($unitTestOld);
                while (!preg_match('"//@DISPLAY ALL"', $line)) {
                    $unitTestNew .= $line;
                    if ((preg_match('/assertEquals\(\$result/', $line))) {
                        $line = fgets($unitTestOld);
                        while (!preg_match('"//@DISPLAY"', $line)) {
                            $block = "";
                            $assert = "";
                            if (preg_match('"try"', $line)) {
                                while (!preg_match('"\s}(\s)*$"', $line)) {
                                    if (preg_match('"assertEquals"', $line)) {
                                        $assert = $line;
                                    }
                                    $block .= $line;
                                    $line = fgets($unitTestOld);
                                }
                                $block .= $line;
                            } else {
                                $assert = $line;
                                $block = $line;
                            }
                            $key1 = explode(',', str_replace("']", "", $assert));
                            if (count($key1) > 1 && preg_match("/'/", $key1[0])) {
                                $key = explode("'", $key1[0])[1];
                                if (in_array(strtolower($key), $camposName))
                                    $unitTestNew .= $block;
                            } else {
                                $unitTestNew .= $line;
                            }
                            $line = fgets($unitTestOld);
                        }
                        for ($i = 0; $i < count($newKeys); $i++) {
                            if ($newKeys[$i] == "Internal") continue;
                            $updatedVal = "\$update['" . $camposName[$i] . "']";
                            if ($camposNulos[$i] == 'YES') $updatedVal = 'null';
                            if ($i != $primaryPos) {
                                if ($campoType[$i] == "TIMESTAMP") {
                                    $unitTestNew .=
                                        "\t\ttry {\n";
                                    $unitTestNew .= "\t\t\tself::assertEquals(" . $updatedVal . ", \$updated->get" . $camposPhpName[$i];
                                    $unitTestNew .= "(DATE_ISO8601), 'Valor actualización no concuerda en " . $camposName[$i] . "');\n";
                                    $unitTestNew .=
                                        "\t\t} catch(PropelException \$e) {
            Log::debug(\$e);
            self::assertTrue(false, 'Formato actualización no concuerda: " . $camposName[$i] . "');
        }\n";
                                } else {
                                    $unitTestNew .= "\t\tself::assertEquals(" . $updatedVal . ", \$updated->get" . $camposPhpName[$i];
                                    $unitTestNew .= "(), 'Valor actualización no concuerda en " . $camposName[$i] . "');\n";
                                }
                            } else {
                                $unitTestNew .= "\t\tself::assertEquals(\$result->get" . $camposPhpName[$i] . "()," . "\$updated->get" . $camposPhpName[$i] . "(), 'Llaves actualización no concuerdan');\n";
                            }
                        }
                    }

                    if (!preg_match('"//@DISPLAY ALL"', $line)) {
                        $line = fgets($unitTestOld);
                    }
                }
            }

            if (preg_match('"//@DISPLAY ALL"', $line)) {
                $unitTestNew .= $line;
            }
        }
        fclose($unitTestOld);
        $archivo = fopen('tests/Unit/Models/' . $phpNameTbl . "Test.php", "w") or die("Unable to open file!");
        fwrite($archivo, $unitTestNew);
        fclose($archivo);
    }


    if (!file_exists('tests/Feature/Controllers/' . $phpNameTbl . "FeatureTest.php")) {
        $controllerTestingCode = "";
        $controllerTestingCode .=
            "<?php

namespace Tests\Feature\Controllers;

use App\TransactionHandler;
use Faker;
use Tests\TestCase;
use Illuminate\Support\Facades\Log;
use Propel\Runtime\Exception\PropelException;

class " . $phpNameTbl . "FeatureTest extends TestCase
{
    /** @test */
    public function it_can_crud_" . $phpNameTbl . "() {
        \$faker = Faker\Factory::create();
//FOREIGNERS

//VALIDATOR\n";
        for ($v = 0; $v < count($camposComments); $v++) {
            $fields = "";
            for ($x = 0; $x < count($camposComments); $x++) {
                if ($x != $primaryPos && $camposComments[$x] != $camposComments[$v]) { //Evitar llave primaria y el campo actual en v
                    if ($camposComments[$x] == 'Internal') continue;
                    if ($campoType[$x] == 'CLOB') {
                        $fields .= "\t\t\t'" . $camposComments[$x] . "' => '{\"test\" : \"json\"}',\n";
                        continue;
                    }
                    if ($campoType[$x] == 'BIGINT') {
                        $fields .= "\t\t\t'" . $camposComments[$x] . "' => null,\n";
                        continue;
                    }
                    $fields .= "\t\t\t'" . $camposComments[$x] . "' => \$faker->";
                    switch ($campoType[$x]) {
                        case 'SMALLINT':
                        case 'INTEGER':
                            $fields .= "numberBetween(0, " . str_pad("9", $campoSize[$x], "9") . ")";
                            break;
                        case 'DOUBLE':
                            $fields .= "randomFloat()";
                            break;
                        case 'LONGVARCHAR':
                        case 'VARCHAR':
                            $fields .= "text(" . $campoSize[$x] . ")";
                            break;
                        case 'CHAR':
                            $fields .= "text(" . $campoSize[$x] . ")";
                            break;
                        case 'DATE':
                        case 'TIMESTAMP':
                        case 'TIMESTAMPTZ':
                            $fields .= "iso8601()";
                            break;
                        case 'BOOLEAN':
                            $fields .= "boolean()";
                            break;
                    }
                    $fields .= ",\n";
                }
            }

            if ($camposComments[$v] == 'Uuid') {
                $controllerTestingCode .=
                    "\t\t//Uuid.required
        \$data = [\n"
                    . $fields .
                    "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

        //Uuid.uuid
        \$data = [
            'Uuid' => \"not a uuid\",\n"
                    . $fields .
                    "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

        //Uuid.size
        \$data = [
            'Uuid' => \"".str_pad("", ($campoSize[$v] + 1), "*")."\",\n"
                    . $fields .
                    "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

";
                continue;
            }


            if ($v != $primaryPos) {
                switch ($campoType[$v]) {
                    case "BIGINT":
                        $controllerTestingCode .=
                            "\t\t//" . $camposComments[$v] . ".required
        \$data = [\n"
                            . $fields .
                            "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

        //" . $camposComments[$v] . ".uuid
        \$data = [
            '" . $camposComments[$v] . "' => \"not a uuid\",\n"
                            . $fields .
                            "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

        //" . $camposComments[$v] . ".size
        \$data = [
            '" . $camposComments[$v] . "' => \"".str_pad("", 37, "*")."\",\n"
                            . $fields .
                            "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

";
                        break;
                    case 'SMALLINT':
                    case 'INTEGER':
                        $controllerTestingCode .=
                            "\t\t//" . $camposComments[$v] . ".integer
        \$data = [
            '" . $camposComments[$v] . "' => \"not integer\",\n"
                            . $fields .
                            "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

        //" . $camposComments[$v] . ".min
        \$data = [
            '" . $camposComments[$v] . "' => \"-1\",\n"
                            . $fields .
                            "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

        //" . $camposComments[$v] . ".max
        \$data = [
            '" . $camposComments[$v] . "' => \"".str_pad("", ($campoSize[$v] + 1), "9")."\",\n"
                            . $fields .
                            "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

";

                        break;
                    case 'DOUBLE':
                        $controllerTestingCode .=
                            "\t\t//" . $camposComments[$v] . ".numeric
        \$data = [
                   '" . $camposComments[$v] . "' => \"1\",\n"
                            . $fields .
                            "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

        //" . $camposComments[$v] . ".min
        \$data = [
            '" . $camposComments[$v] . "' => \"-1\",\n"
                            . $fields .
                            "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

        //" . $camposComments[$v] . ".max
        \$data = [
            '" . $camposComments[$v] . "' => \"".str_pad("", ($campoSize[$v] + 1), "9")."\",\n"
                            . $fields .
                            "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

";
                        break;
                    case 'CLOB':
                        $controllerTestingCode .=
                            "\t\t//" . $camposComments[$v] . ".json
        \$data = [
                   '" . $camposComments[$v] . "' => \"not json\",\n"
                            . $fields .
                            "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

";
                        break;
                    case 'LONGVARCHAR':
                    case 'CHAR':
                    case 'VARCHAR':
                        $controllerTestingCode .=
                            "        //" . $camposComments[$v] . ".max
        \$data = [
            '" . $camposComments[$v] . "' => \"".str_pad("", ($campoSize[$v] + 1), "*")."\",\n"
                            . $fields .
                            "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

";
                        break;
                    case 'DATE':
                    case 'TIMESTAMP':
                    case 'TIMESTAMPTZ':
                        $controllerTestingCode .=
                            "        //" . $camposComments[$v] . ".date_format ISO
        \$data = [
            '" . $camposComments[$v] . "' => date('Y-m-d'),\n"
                            . $fields .
                            "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

";
                        break;
                    case 'BOOLEAN':
                        $controllerTestingCode .=
                            "        //" . $camposComments[$v] . ".boolean
        \$data = [
            '" . $camposComments[$v] . "' => 'not boolean'\n,"
                            . $fields .
                            "        ];
        \$this
            ->post(route('" . $phpNameTbl . ".submit'), \$data)
            ->assertStatus(200)
            ->assertSee('\"success\":false');

";
                        break;
                }
            }
        }

        $controllerTestingCode .=
            "//CREATE
        \$data = [\n";
        for ($i = 0; $i < count($camposComments); $i++) {
            if ($i != $primaryPos) { // Evitar la llave primaria autoincrementable
                if ($camposComments[$i] == 'Internal') continue;
                if ($camposPhpName[$i] == 'Uuid') {
                    $controllerTestingCode .= "\t\t\t'" . $camposComments[$i] . "' => \$faker->uuid,\n";
                    continue;
                }
                if ($campoType[$i] == 'CLOB') {
                    $controllerTestingCode .= "\t\t\t'" . $camposComments[$i] . "' => '{\"test\" : \"json\"}',\n";
                    continue;
                }
                if ($campoType[$i] == 'BIGINT') {
                    $controllerTestingCode .= "\t\t\t'" . $camposComments[$i] . "' => null,\n";
                    continue;
                }
                $controllerTestingCode .= "\t\t\t'" . $camposComments[$i] . "' => \$faker->";
                switch ($campoType[$i]) {
                    case 'SMALLINT':
                    case 'INTEGER':
                        $controllerTestingCode .= "numberBetween(0, " . str_pad("9", $campoSize[$i], "9") . ")";
                        break;
                    case 'DOUBLE':
                        $controllerTestingCode .= "randomFloat()";
                        break;
                    case 'LONGVARCHAR':
                    case 'VARCHAR':
                        $controllerTestingCode .= "text(" . $campoSize[$i] . ")";
                        break;
                    case 'CHAR':
                        $controllerTestingCode .= "text(" . $campoSize[$i] . ")";
                        break;
                    case 'DATE':
                    case 'TIMESTAMP':
                    case 'TIMESTAMPTZ':
                        $controllerTestingCode .= "iso8601()";
                        break;
                    case 'BOOLEAN':
                        $controllerTestingCode .= "boolean()";
                        break;
                }
                $controllerTestingCode .= ",\n";
            }
        }
        $controllerTestingCode .= "\t\t];
\t\t\$this
\t\t\t->post(route('" . $phpNameTbl . ".submit'), \$data)\n" .
            "\t\t\t->assertStatus(200)\n" .
            "\t\t\t->assertSee('\"success\":true');\n";

        $controllerTestingCode .=
            "
// UPDATE
        \$update = [\n";
        for ($i = 0; $i < count($camposName); $i++) {
            if ($camposComments[$i] == 'Internal') continue;
            if ($camposComments[$i] == 'Uuid') {
                $controllerTestingCode .= "\t\t\t'" . $camposComments[$i] . "' => \$data['Uuid'],\n";
                continue;
            }


            if ($i != $primaryPos) { // Evitar la llave primaria autoincrementable
                if ($camposNulos[$i] == 'NO') {
                    if ($campoType[$i] == 'CLOB') {
                        $controllerTestingCode .= "\t\t\t'" . $camposComments[$i] . "' => '{\"test\" : \"json\"}',\n";
                        continue;
                    }
                    $controllerTestingCode .= "\t\t\t'" . $camposComments[$i] . "' => \$faker->";
                    switch ($campoType[$i]) {
                        case 'BIGINT':
                            $controllerTestingCode .= "numberBetween(0, " . str_pad("9", 18, "9") . ")";
                            break;
                        case 'SMALLINT':
                        case 'INTEGER':
                            $controllerTestingCode .= "numberBetween(0, " . str_pad("9", $campoSize[$i], "9") . ")";
                            break;
                        case 'DOUBLE':
                            $controllerTestingCode .= "randomFloat()";
                            break;
                        case 'LONGVARCHAR':
                        case 'VARCHAR':
                            $controllerTestingCode .= "text(" . $campoSize[$i] . ")";
                            break;
                        case 'CHAR':
                            $controllerTestingCode .= "text(" . $campoSize[$i] . ")";
                            break;
                        case 'DATE':
                        case 'TIMESTAMP':
                        case 'TIMESTAMPTZ':
                            $controllerTestingCode .= "iso8601()";
                            break;
                        case 'BOOLEAN':
                            $controllerTestingCode .= "boolean()";
                            break;
                    }
                } else {
                    $controllerTestingCode .= "\t\t\t'" . $camposComments[$i] . "' => null";
                }
                $controllerTestingCode .= ",\n";
            }
        }
        $controllerTestingCode .=
            "\t\t];
\t\t\$this
\t\t\t->post(route('" . $phpNameTbl . ".modify'), \$update)\n" .
            "\t\t\t->assertStatus(200)\n" .
            "\t\t\t->assertSee('\"success\":true');\n\n";

        $controllerTestingCode .= "\n\n//DELETE
\t\t\$this
\t\t\t->post(route('" . $phpNameTbl . ".remove'), \$update)\n" .
            "\t\t\t->assertStatus(200)\n" .
            "\t\t\t->assertSee('\"success\":true');\n";

        $controllerTestingCode .=
            "\t}
}";

        if (!file_exists('tests/Feature/Controllers'))
            mkdir('tests/Feature/Controllers');

        $archivo = fopen('tests/Feature/Controllers/' . $phpNameTbl . "FeatureTest.php", "w") or die("Unable to open file!");
        fwrite($archivo, $controllerTestingCode);
        fclose($archivo);

    } else {
        $controllerTestOld = fopen('tests/Feature/Controllers/' . $phpNameTbl . "FeatureTest.php", "r");
        $controllerTestNew = "";
        $times = 0;

        $validation = '';
        while (!feof($controllerTestOld)) {
            $line = fgets($controllerTestOld);
            if(preg_match('/\s\/\/\w+.\w+/', $line)) {
                $validation = trim(preg_replace("@\s//@", '', explode('.', $line)[0]));
            }

            if (preg_match('/\$data = \[/', $line)) {
                $controllerTestNew .= $line;
                $line = fgets($controllerTestOld);
                $newKeys = $camposComments;
                while (!preg_match('/\];/', $line)) {
                    $key = explode('=>', preg_replace("/\s/", "", str_replace("'", "", $line)))[0];
                    for ($i = 0; $i < count($newKeys); $i++) {
                        if ($newKeys[$i] == $key) {
                            $newKeys[$i] = 'Internal';
                        }
                        if ($newKeys[$i] == $validation) {
                            $newKeys[$i] = 'Internal';
                        }
                    }
                    if (in_array($key, $camposComments))
                        $controllerTestNew .= $line;

                    $line = fgets($controllerTestOld);
                }

                for ($i = 0; $i < count($camposComments); $i++) {
                    if ($newKeys[$i] == 'Internal') continue;
                    if ($i != $primaryPos) { // Evitar la llave primaria autoincrementable
                        if ($campoType[$i] == 'CLOB') {
                            $controllerTestNew .= "\t\t\t'" . $camposComments[$i] . "' => '{\"test\" : \"json\"}',\n";
                            continue;
                        }
                        if ($campoType[$i] == 'BIGINT') {
                            $controllerTestNew .= "\t\t\t'" . $camposComments[$i] . "' => null,\n";
                            continue;
                        }
                        $controllerTestNew .= "\t\t\t'" . $camposComments[$i] . "' => \$faker->";
                        switch ($campoType[$i]) {
                            case 'SMALLINT':
                            case 'INTEGER':
                                $controllerTestNew .= "numberBetween(0, " . str_pad("9", $campoSize[$i], "9") . ")";
                                break;
                            case 'DOUBLE':
                                $controllerTestNew .= "randomFloat()";
                                break;
                            case 'LONGVARCHAR':
                            case 'VARCHAR':
                                $controllerTestNew .= "text(" . $campoSize[$i] . ")";
                                break;
                            case 'CHAR':
                                $controllerTestNew .= "text(" . $campoSize[$i] . ")";
                                break;
                            case 'DATE':
                            case 'TIMESTAMP':
                            case 'TIMESTAMPTZ':
                                $controllerTestNew .= "iso8601()";
                                break;
                            case 'BOOLEAN':
                                $controllerTestNew .= "boolean()";
                                break;
                        }
                        $controllerTestNew .= ",\n";
                    }
                }
            }

            if (preg_match('/\$update = \[/', $line)) {
                $controllerTestNew .= $line;
                $line = fgets($controllerTestOld);
                $newKeys = $camposComments;
                while (!preg_match('/\];/', $line)) {
                    $key = explode('=>', preg_replace("/\s/", "", str_replace("'", "", $line)))[0];
                    for ($i = 0; $i < count($newKeys); $i++) {
                        if ($newKeys[$i] == $key) {
                            $newKeys[$i] = 'Internal';
                        }
                    }
                    if (in_array($key, $camposComments))
                        $controllerTestNew .= $line;

                    $line = fgets($controllerTestOld);
                }

                for ($i = 0; $i < count($newKeys); $i++) {
                    if ($i != $primaryPos) { // Evitar la llave primaria autoincrementable
                        if ($newKeys[$i] == 'Internal') continue;
                        if ($camposNulos[$i] == 'NO') {
                            if ($campoType[$i] == 'CLOB') {
                                $controllerTestNew .= "\t\t\t'" . $camposComments[$i] . "' => '{\"test\" : \"json\"}',\n";
                                continue;
                            }
                            if ($campoType[$i] == 'BIGINT') {
                                $controllerTestNew .= "\t\t\t'" . $camposComments[$i] . "' => null,\n";
                                continue;
                            }
                            $controllerTestNew .= "\t\t\t'" . $camposComments[$i] . "' => \$faker->";
                            switch ($campoType[$i]) {
                                case 'SMALLINT':
                                case 'INTEGER':
                                    $controllerTestNew .= "numberBetween(0, " . str_pad("9", $campoSize[$i], "9") . ")";
                                    break;
                                case 'DOUBLE':
                                    $controllerTestNew .= "randomFloat()";
                                    break;
                                case 'LONGVARCHAR':
                                case 'VARCHAR':
                                    $controllerTestNew .= "text(" . $campoSize[$i] . ")";
                                    break;
                                case 'CHAR':
                                    $controllerTestNew .= "text(" . $campoSize[$i] . ")";
                                    break;
                                case 'DATE':
                                case 'TIMESTAMP':
                                case 'TIMESTAMPTZ':
                                    $controllerTestNew .= "iso8601()";
                                    break;
                                case 'BOOLEAN':
                                    $controllerTestNew .= "boolean()";
                                    break;
                            }
                        } else {
                            $controllerTestNew .= "\t\t\t'" . $camposComments[$i] . "' => null";
                        }
                        $controllerTestNew .= ",\n";
                    }
                }
            }

            $controllerTestNew .= $line;
        }

        fclose($controllerTestOld);
        $archivo = fopen('tests/Feature/Controllers/' . $phpNameTbl . "FeatureTest.php", "w") or die("Unable to open file!");
        fwrite($archivo, $controllerTestNew);
        fclose($archivo);

    }

    echo "Feature Test generado para '" . $phpNameTbl . "'" . PHP_EOL;


// Vistas
// Vistas 1 ------------------------------------------------------------------
    $codeViewCreateEdit =
        "<template>
    <div>
    <hero-bar>
      {{ heroTitle }}
      <router-link slot=\"right\" to=\"/" . $phpNameTbl . "/index\" class=\"button\">
        " . $tableComment . "
      </router-link>
    </hero-bar>
    <section class=\"section is-main-section\">
      <tiles>
        <card-component :title=\"formCardTitle\" icon=\"account-edit\" class=\"tile is-child\">
          <form @submit.prevent=\"submit\">
            <template v-if=\"id\">
              <b-field label=\"" . $camposPhpName[$primaryPos] . "\" horizontal>
                <b-input :value=\"" . $camposName[$primaryPos] . "\" custom-class=\"is-static\" readonly />
              </b-field>
              <b-field label=\"" . $camposPhpName[$uuidPos] . "\" horizontal>
                <b-input :value=\"" . $camposName[$uuidPos] . "\" custom-class=\"is-static\" readonly />
              </b-field>
              <hr>
            </template>
";
    for ($i = 0; $i < count($campoType); $i++) {
        if ($i != $primaryPos && $i != $uuidPos) {
            $codeViewCreateEdit .=
                "            <b-field label=\"" . $camposPhpName[$i] . "\"  horizontal>
              ";
            if ($camposNulos[$i] == "YES") {
                $codeViewCreateEdit .=
                    "<b-input  v-model=\"form." . $camposPhpName[$i] . "\" required />\n";
            } else {
                $codeViewCreateEdit .=
                    "<b-input  v-model=\"form." . $camposPhpName[$i] . "\" />\n";
            }
            "             </b-field>\n";
        }
    }
    $codeViewCreateEdit .=
        "            <b-field horizontal>
               <b-button type=\"is-primary\" :loading=\"isLoading\" native-type=\"submit\">Submit</b-button>
             </b-field>
          </form>
        </card-component>
       </tiles>
     </section>
   </div>
</template>

<script>
import clone from 'lodash/clone'
import TitleBar from '@/components/TitleBar'
import HeroBar from '@/components/HeroBar'
import Tiles from '@/components/Tiles'
import CardComponent from '@/components/CardComponent'
import FilePicker from '@/components/FilePicker'
import UserAvatar from '@/components/UserAvatar'
import Notification from '@/components/Notification'

export default{
  name: '" . $phpNameTbl . "Form',
  components: { UserAvatar, FilePicker, CardComponent, Tiles, HeroBar, TitleBar, Notification },
  props: {
    id: {
      default: null
    }
  },
  data () {
    return {
      isLoading: false,
      item: null,
      form: this.getClearFormObject(),
      createdReadable: null,
    }
  },
  computed: {
    isProfileExists () {
      return !!this.item
    }
  },
  created () {
    this.getData()
  },
  methods: {
    getClearFormObject () {
      return {
";
    for ($i = 0; $i < count($camposName); $i++) {
        switch ($campoType[$i]) {
            case 'TIMESTAMP':
            case 'TIMESTAMPTZ':
                $codeViewCreateEdit .=
                    "        " . $camposName[$i] . ": new Date(),";
                break;
            default:
                $codeViewCreateEdit .=
                    "        " . $camposName[$i] . ": null,";
                break;
        }
    }
    $codeViewCreateEdit .=
        "      }
    },
    getData () {
      if (this.id) {
        axios
          .get(`/" . $phpNameTbl . "/\${this.id}`)
          .then(r => {
            this.form = r.data.data
            this.item = clone(r.data.data)

            // TODO this.form.created_date = new Date(r.data.data.created_mm_dd_yyyy)
          })
          .catch(e => {
            this.item = null

            this.\$buefy.toast.open({
              message: `Error: \${e.message}`,
              type: 'is-danger',
              queue: false
            })
          })
      }
    },
    fileIdUpdated (fileId) {
      this.form.file_id = fileId
      //this.form.avatar_filename = null
    },
    input (v) {
      //this.createdReadable = moment(v).format('MMM D, Y').toString()
    },
    submit () {
      this.isLoading = true
      let method = 'post'
      let url = '/" . $phpNameTbl . "/store'

      if (this.id) {
        method = 'patch'
        url = `/" . $phpNameTbl . "/\${this.id}`
      }

      axios({
        method,
        url,
        data: this.form
      }).then(r => {
        this.isLoading = false

        if (!this.id && r.data.data.id) {
          this.\$router.push({name: '" . $phpNameTbl . ".edit', params: {id: r.data.data.id}})

          this.\$buefy.snackbar.open({
            message: 'Created',
            queue: false
          })
        } else {
          this.item = r.data.data

          this.\$buefy.snackbar.open({
            message: 'Updated',
            queue: false
          })
        }
      }).catch(e => {
        this.isLoading = false

        this.\$buefy.toast.open({
          message: `Error: \${e.message}`,
          type: 'is-danger',
          queue: false
        })
      })
    }
  },
  watch: {
    id (newValue) {
      this.form = this.getClearFormObject()
      this.item = null

      if (newValue) {
        this.getData()
      }
    }
  }
}
</script>";

// Vistas 2 ------------------------------------------------------------------

    $codeTblWTrash =
        "<template>
   <div>
      <modal-trash-box :is-active=\"isModalActive\" :trash-subject=\"trashObjectName\" @confirm=\"trashConfirm\" @cancel=\"trashCancel\"/>
         <b-table
         :loading=\"isLoading\"
         :paginated=\"paginated\"
         :per-page=\"perPage\"
         :striped=\"true\"
         :hoverable=\"true\"
         :data=\"" . $nameTbl . "\">
         <template slot-scope=\"props\">
";
    for ($i = 0; $i < count($camposPhpName); $i++) {
        if ($i != $primaryPos) {
            $codeTblWTrash .=
                "\t\t\t<b-table-column label=\"" . $camposPhpName[$i] . "\" field=\"" . $camposName[$i] . "\" >
                        {{ props.row." . $camposPhpName[$i] . " }}
                    </b-table-column>\n";
        }
    }
    $codeTblWTrash .=
        "
            <b-table-column custom-key=\"actions\" class=\"is-actions-cell\">
                <div class=\"buttons is-right\">
                    <router-link :to=\"{name:'" . $nameTbl . ".edit', params: {id: props.row." . $camposName[$primaryPos] . "}}\" class=\"button is-small is-primary\">
                       <b-icon icon=\"account-edit\" size=\"is-small\"/>
                    </router-link>
                    <button class=\"button is-small is-danger\" type=\"button\" @click.prevent=\"trashModal(props.row)\">
                      <b-icon icon=\"trash-can\" size=\"is-small\"/>
                    </button>
                </div>
             </b-table-column>
         </template>
     <section class=\"section\" slot=\"empty\">
        <div class=\"content has-text-grey has-text-centered\">
          <template v-if=\"isLoading\">
            <p>
              <b-icon icon=\"dots-horizontal\" size=\"is-large\"/>
            </p>
            <p>Fetching data...</p>
          </template>
          <template v-else>
            <p>
              <b-icon icon=\"emoticon-sad\" size=\"is-large\"/>
            </p>
            <p>Nothing's here&hellip;</p>
          </template>
        </div>
      </section>
    </b-table>
  </div>
</template>

<script>
import ModalTrashBox from '@/components/ModalTrashBox'
export default {
  name: '" . $phpNameTbl . "Table',
  components: { ModalTrashBox },
  props: {
    dataUrl: {
      type: String,
      default: null
    },
    checkable: {
      type: Boolean,
      default: false
    }
  },
  data () {
    return {
      isModalActive: false,
      trashObject: null,
      " . strtolower($phpNameTbl) . ": [],
      isLoading: false,
      paginated: false,
      perPage: 10,
      checkedRows: []
    }
  },
  computed: {
    trashObjectName () {
      if (this.trashObject) {
        return this.trashObject.name
      }

      return null
    }
  },
  created () {
    this.getData()
  },
  methods: {
    getData () {
      if (this.dataUrl) {
        this.isLoading = true
        axios
          .get(this.dataUrl)
          .then(r => {
            this.isLoading = false
            if (r.data && r.data.data) {
              if (r.data.data.length > this.perPage) {
                this.paginated = true
              }
              this." . $nameTbl . " = r.data.data
            }
          })
          .catch( err => {
            this.isLoading = false
            this.\$buefy.toast.open({
              message: `Error: \${err.message}`,
              type: 'is-danger',
              queue: false
            })
          })
      }
    },
    trashModal (trashObject) {
      this.trashObject = trashObject
      this.isModalActive = true
    },
    trashConfirm () {
      let url
      let method
      let data = this.trashObject

      this.isModalActive = false
      method = 'post'
      url = '/" . $phpNameTbl . "/remove'
      axios({
        method,
        url,
        data
      })
        .then( r => {
          this.getData()

          this.\$buefy.snackbar.open({
            message: `Deleted \${this.trashObject.name}`,
            queue: false
          });

        })
        .catch( err => {
          this.\$buefy.toast.open({
            message: `Error: \${err.message}`,
            type: 'is-danger',
            queue: false
          })
        })
    },
    trashCancel () {
      this.trashObject = null;
      this.isModalActive = false;
    }
  }
}
</script>";

// Vistas 3 ---------------------------------------------------------------
    $codeViewMain =
        "<template>
    <section class=\"section is-main-section\">
      <div class=\"tile is-ancestor\">
        <div class=\"tile is-3 is-vertical is-parent\">
          <div class=\"tile is-child box\">
            <p class=\"title\">Filtros</p>
            <create></create>
          </div>
        </div>
        <div class=\"tile is-parent\">
          <div class=\"tile is-child box\">
            <p class=\"title\">" . $tableComment . "</p>
            <TableWithTrash data-url=\"/" . $phpNameTbl . "\" />
          </div>
        </div>
      </div>

    </section>
  </div>
</template>
<script>
  import TableWithTrash from './TableWithTrash'
  import CardComponent from '@/components/CardComponent'
  import Notification from '@/components/Notification'
  import Create from '@/views/" . $phpNameTbl . "/" . $phpNameTbl . "CreateEdit'

  export default {
        name: \"Main\",
        components: {
          TableWithTrash,
          CardComponent,
          Notification,
          Create
        }
    }
</script>

<style scoped>

</style>
";

//Vistas 4 ----------------------------------------------------------------
    $codeViewFilter =
        "<template>
   <div class='container'>
     <card-component class=\"has-table has-mobile-sort-spaced\" title=\"MASTER INPUTS\" icon=\"account-multiple\">
       <form @submit=\"\">
         ";
    for ($i = 0; $i < count($campoType); $i++) {
        if ($i != $primaryPos && $i != $uuidPos) {
            switch ($campoType[$i]) {
                case 'SMALLINT':
                case 'BIGINT':
                case 'INTEGER':
                    $codeViewFilter .=
                        "<integer title=\"" . $camposComments[$i] . "\"
                :inputData.sync=\"" . $camposPhpName[$i] . "\"></integer>";
                    break;
                case 'DOUBLE':
                    $codeViewFilter .=
                        "<Float title=\"" . $camposComments[$i] . "\"
                :inputData.sync=\"" . $camposPhpName[$i] . "\"></Float>";
                    break;
                case 'LONGVARCHAR':
                case 'CLOB':
                case 'VARCHAR':
                    $codeViewFilter .=
                        "<String title=\"" . $camposComments[$i] . "\"
                v-bind:lenght=\"30\"
                :inputData.sync=\"" . $camposPhpName[$i] . "\"></String>";
                    break;
                case 'CHAR':
                    break;
                case 'DATE':
                case 'TIMESTAMP':
                case 'TIMESTAMPTZ':
                    $codeViewFilter .=
                        "<DateTime title=\"" . $camposComments[$i] . "\"
                  :inputData.sync=\"" . $camposPhpName[$i] . "\"></DateTime>";
                    break;
                case 'BOOLEAN':
                    break;
            }
            $codeViewFilter .= "\n         ";
        }
    }
    $codeViewFilter .=
        " </form>
    </card-component>
  </div>
</template>

<script>
  import CardComponent from '@/components/CardComponent'
  import Integer from '@/components/Inputs/Integer'
  import File from '@/components/Inputs/File'
  import TextArea from '@/components/Inputs/TextArea'
  import Date from '@/components/Inputs/Date'
  import Float from '@/components/Inputs/Float'
  import Password from '@/components/Inputs/Password'
  import String from '@/components/Inputs/String'
  import Email from '@/components/Inputs/Email'
  import Time from '@/components/Inputs/Time'
  import DateTime from '@/components/Inputs/DateTime'
  import Select from '@/components/Inputs/Select'
  import AlphanumericString from '@/components/Inputs/AlphanumericString'
  import AlphabeticString from '@/components/Inputs/AlphabeticString'
  import NumericString from '@/components/Inputs/AlphabeticString'

  export default {
      name: \"Create\",
      data:{
        dataForm: [],
      },
      props:{
";
    for ($i = 0; $i < count($campoType); $i++) {
        if ($i != $primaryPos) {
            switch ($campoType[$i]) {
                case 'SMALLINT':
                case 'BIGINT':
                case 'INTEGER':
                    $codeViewFilter .=
                        "\t\t" . $camposPhpName[$i] . ": {
          type: String,
          default: null
          },";
                    break;
                case 'DOUBLE':
                    $codeViewFilter .=
                        "\t\t" . $camposPhpName[$i] . ": {
          type: Number,
          default: 0
          },";
                    break;
                case 'CLOB':
                case 'LONGVARCHAR':
                case 'VARCHAR':
                    $codeViewFilter .=
                        "\t\t" . $camposPhpName[$i] . ": {
          type: String,
          default: null
          },";
                    break;
                case 'CHAR':
                    break;
                case 'DATE':
                case 'TIMESTAMP':
                case 'TIMESTAMPTZ':
                    $codeViewFilter .=
                        "\t\t" . $camposPhpName[$i] . ": {
          type: String,
          default: null
          },";
                    break;
                case 'BOOLEAN':
                    break;
            }
            $codeViewFilter .= "\n";
        }
    }
    $codeViewFilter .=
        "   },
    components: {
        CardComponent,
        Integer,
        File,
        TextArea,
        Date,
        Float,
        Password,
        String,
        Email,
        Time,
        DateTime,
        Select,
        AlphanumericString,
        AlphabeticString,
        NumericString
      },
      methods:{
          formSubmit(e){
            e.preventDefault();
            let currentObj = this;
            this.axios.post('/submit',{
              dataForm: this.dataForm
            })
            .then(function (response){
              console.log(response.data);
            })
            .catch(function (response){
              console.log(response.data);
            });
          }
      }
    }
</script>

<style scoped>

</style>
";

//    @mkdir("resources/js/views");
//    @mkdir("resources/js/views/".$phpNameTbl);
//    $archivo = fopen("resources/js/views/".$phpNameTbl."/".$phpNameTbl."CreateEdit.vue", "w") or die("Unable to open file!");
//    fwrite($archivo, $codeViewCreateEdit);
//    fclose($archivo);
//    $archivo = fopen("resources/js/views/".$phpNameTbl."/".$phpNameTbl."Main.vue", "w") or die("Unable to open file!");
//    fwrite($archivo, $codeViewMain);
//    fclose($archivo);
//    $archivo = fopen("resources/js/views/".$phpNameTbl."/".$phpNameTbl."Filter.vue", "w") or die("Unable to open file!");
//    fwrite($archivo, $codeViewFilter);
//    fclose($archivo);
//    $archivo = fopen("resources/js/views/".$phpNameTbl."/TableWithTrash.vue", "w") or die("Unable to open file!");
//    fwrite($archivo, $codeTblWTrash);
//    fclose($archivo);

//    echo "Vistas generadas para '" . $phpNameTbl . "'" . PHP_EOL;

    // API ROUTES


    //JS Router
    $codeJSRouter =
        "import ModLayout from \"../components/Layout/WelcomeLayout.vue\";
import WelcomeLayout from \"../components/Layouts/WelcomeLayout.vue\";
import Welcome from \"../views/welcome\"";
    foreach ($camposPhpName as $tbl) {
        $codeJSRouter .= "import " . $tbl . "Main from \"../views/" . $tbl . "/Main\"\n";
    }
    $codeJSRouter .=
        "let WelcomePage = {
    path: \"/\",
    component: WelcomeLayout,
    children:[
      {
        path: '/',
        name: 'Welcome',
        component: Welcome
      }
    ]
};

";

    foreach ($camposPhpName as $tbl) {
        $codeJSRouter .= "let " . $tbl . "Pages = {
    path: \"/" . strtolower($tbl) . "\",
    component: ModLayout,
    children: [
        {
            path: \"/" . strtolower($tbl) . "\",
            name: \"" . $tbl . "\",
            component: " . $tbl . "Main,
        },
    ]
};
";
    }
    $codeJSRouter .=
        "const routes = [";
    foreach ($camposPhpName as $tbl) {
        $codeJSRouter .=
            "   " . $tbl . "Pages,\n";
    }
    $codeJSRouter .=
        "export default routes;";
    // var_dump($codeJSRouter);
// END MAIN
}

//Obtener nombredb
$dbName = $schemaTmp->xpath("/database")[0]->attributes()['name']->__toString();

// Param check
if(count($argv)==1) {
    //echo "no params\n";
    foreach ($schemaTmp as $table) {
        makeTblCode($table, $dbName, $schemaTmp, $tblComments);
    }
}else {
    //echo "params\n";
    for($i=1;$i<count($argv);$i++){
        $table = $schemaTmp->xpath("//table[@name='".$argv[$i]."']");
        if(count($table)==0) {
            echo "No existe la tabla ".$argv[$i]."\n";
        }else {
            $table = $table[0];
            makeTblCode($table, $dbName, $schemaTmp, $tblComments);
        }
    }
}

//Rutas TODO CASI OBSOLETO Reemplazado por API y Rutas de JS
$codeRoutes =
    "<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('/', function () {
    return view('welcome');
});

";

$tbls = $schemaTmp->xpath("//table");
foreach ($tbls as $key => $table) {
    $nameTbl = $table->attributes()['name']->__toString();
    if ($nameTbl == 'migrations' || $nameTbl == 'password_resets' || $nameTbl == 'failed_jobs' || $nameTbl == 'oauth_providers') continue;
    $natNameTbl = $tblComments[$nameTbl];
    $codeRoutes .=
        "\n//" . $natNameTbl . " Route
    Route::get('/" . $natNameTbl . "/fetch', '" . $natNameTbl . "Controller@loadtable')->name('" . $natNameTbl . ".fetch');
    Route::post('/" . $natNameTbl . "/submit', '" . $natNameTbl . "Controller@create')->name('" . $natNameTbl . ".submit');
    Route::post('/" . $natNameTbl . "/modify', '" . $natNameTbl . "Controller@update')->name('" . $natNameTbl . ".modify');
    Route::post('/" . $natNameTbl . "/remove', '" . $natNameTbl . "Controller@destroy')->name('" . $natNameTbl . ".remove');
";
}
$codeRoutes .=
    "\n//TODO *CRUD Generator control separator line* (Don't remove this line!)
";

if (file_exists('routes/api.php')) {
    $archivo = fopen('routes/api.php', "r") or die("Unable to open file!");
    $oldcode = "";
    while (($buffer = fgets($archivo)) !== false) {
        $oldcode .= $buffer;
    }
    $oldcode = preg_split("/\/\/TODO \*CRUD Generator control separator line\*\s\(Don't remove this line!\)/", $oldcode);
    $codeRoutes = $codeRoutes . $oldcode[1];

} else {
    $codeRoutes .= "\n}";
}


$archivo = fopen('routes/api.php', "w") or die("Unable to open file!");
fwrite($archivo, $codeRoutes);
fclose($archivo);
