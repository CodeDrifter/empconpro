<?php

namespace App\Policies;

use App\User;
use Crsenttbl;
use Illuminate\Auth\Access\HandlesAuthorization;

class CrsenttblPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Crsenttbl $crsenttbl)
    {
        // Update $user authorization to view $viehicle here.
        return true;
    }

    public function create(User $user, Crsenttbl $crsenttbl)
    {
        // Update $user authorization to view $viehicle here.
        return true;
    }

    public function update(User $user, Crsenttbl $crsenttbl)
    {
        // Update $user authorization to view $viehicle here.
        return true;
    }

    public function delete(User $user, Crsenttbl $crsenttbl)
    {
        // Update $user authorization to view $viehicle here.
        return true;
    }
}