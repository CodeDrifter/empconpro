<?php

namespace App\Policies;

use App\User;
use Chcenttbl;
use Illuminate\Auth\Access\HandlesAuthorization;

class ChcenttblPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Chcenttbl $chcenttbl)
    {
        // Update $user authorization to view $viehicle here.
        return true;
    }

    public function create(User $user, Chcenttbl $chcenttbl)
    {
        // Update $user authorization to view $viehicle here.
        return true;
    }

    public function update(User $user, Chcenttbl $chcenttbl)
    {
        // Update $user authorization to view $viehicle here.
        return true;
    }

    public function delete(User $user, Chcenttbl $chcenttbl)
    {
        // Update $user authorization to view $viehicle here.
        return true;
    }
}