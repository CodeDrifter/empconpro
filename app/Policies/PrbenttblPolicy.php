<?php

namespace App\Policies;

use App\User;
use Prbenttbl;
use Illuminate\Auth\Access\HandlesAuthorization;

class PrbenttblPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Prbenttbl $prbenttbl)
    {
        // Update $user authorization to view $viehicle here.
        return true;
    }

    public function create(User $user, Prbenttbl $prbenttbl)
    {
        // Update $user authorization to view $viehicle here.
        return true;
    }

    public function update(User $user, Prbenttbl $prbenttbl)
    {
        // Update $user authorization to view $viehicle here.
        return true;
    }

    public function delete(User $user, Prbenttbl $prbenttbl)
    {
        // Update $user authorization to view $viehicle here.
        return true;
    }
}