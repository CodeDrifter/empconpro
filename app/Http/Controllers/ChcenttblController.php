<?php

namespace App\Http\Controllers;

use App\ReturnHandler;
use App\TransactionHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Chcenttbl;
use Ramsey\Uuid\Uuid;
use MercadoPago;
use function Psy\debug;


class ChcenttblController extends Controller
{
    public function __construct()
    {

    }

    // store (C)
    public function create($cursouuid, $useruuid, $admin = false)
    {
        $trncnn = TransactionHandler::begin();

        $uuid4 = Uuid::uuid4();

        $entusr = \Users::fnuusers($useruuid, $trncnn);
        if (!$entusr) {
            TransactionHandler::rollback($trncnn);
            if ($admin) {
                return ReturnHandler::rtrerrjsn("No existe el usuario");
            } else {
                return redirect("https://emprendedoresconproposito.online/mensaje/error/Su pago fue exitoso, sin embargo hubo un problema al registrar la compra, le suplicamos que contacte por correo a la administración con brevedad");
            }
        }

        $entcrs = \Crsenttbl::fnucrsenttbl($cursouuid, $trncnn);
        if (!$entcrs) {
            TransactionHandler::rollback($trncnn);
            if ($admin) {
                return ReturnHandler::rtrerrjsn("No existe el curso");
            } else {
                return redirect("https://emprendedoresconproposito.online/mensaje/error/Su pago fue exitoso, sin embargo hubo un problema al registrar la compra, le suplicamos que contacte por correo a la administración con brevedad");
            }
        }


        $checkout = \Chcenttbl::fnfchcenttbl($useruuid, $cursouuid);

        if ($checkout) {
            return redirect("https://emprendedoresconproposito.online/mensaje/info/Parece que ya estas incrito a este curso, revisa en \"Mis Cursos\"");
        }

        $entusr = $entusr->toArray();
        $entcrs = $entcrs->toArray();

        // 2.- Peticion a variables
        $data = [
            'uuid' => $uuid4,
            'idnentusr' => $entusr["Id"],
            'uidentusr' => $entusr["Uuid"],
            'idnentcrs' => $entcrs["Idnentcrs"],
            'uidentcrs' => $entcrs["Uuid"],
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ];

        $entchc = \Chcenttbl::crtchcenttbl($data, $trncnn);

        if (!$entchc) {
            TransactionHandler::rollback($trncnn);
            if ($admin) {
                return ReturnHandler::rtrerrjsn("Error al registrar");
            } else {
                return redirect("https://emprendedoresconproposito.online/mensaje/error/Su pago fue exitoso, sin embargo hubo un problema al registrar la compra, le suplicamos que contacte por correo a la administración con brevedad");
            }
        }

        try {
            $data = array(
                'name' => $entusr["Name"],
                'messages' => "¡Felicidades! A partir de hoy, formas parte de Emprendedores con propósito. No sabes el gusto que me da que hayas tomado la decisión de transformar tu vida HOY y formar tu Modelo de negocios. Archiva este correo en algún lugar que recuerdes, contiene tu información de acceso al curso. Haz click aqui para acceder al curso: ",
                'link' => "https://emprendedoresconproposito.online/curso/videos/" . $entcrs["Uuid"],
                'linkstring' => "Ingresar Curso",
                'goodbye' => "¡Te doy la mas cordial bienvenida a creando tu modelo de negocios Paso-a-Paso!",
                'autor' => "- Adilene Ramos",
            );

            Mail::send('email.regular', $data, function ($message) use ($entusr) {
                $message->from("emprendedoresconproposito01@gmail.com", "Emprendedores con proposito");
                $message->subject("Recibo de webinar/curso");
                $message->to($entusr["Email"]);
            });
        } catch (Exception $e) {
            Log::debug($e);
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('Ocurrio un problema para enviarte un correo');
        }

        TransactionHandler::commit($trncnn);

        if ($admin) {
            return ReturnHandler::rtrsccjsn("Registrado y correo enviado correctamente");
        } else {
            return redirect("https://emprendedoresconproposito.online/mensaje/success/¡Gracias por su compra!, le hemos enviado un correo con información y como recibo");
        }
    }

    public function createHandler(Request $request)
    {
        $rules = [
            'UuidUsuario' => 'required|uuid|size:36',
            'UuidCurso' => 'required|uuid|size:36',
        ];

        $msgs = [
            'UuidUsuario.required' => 'Objeto no válido',
            'UuidUsuario.uuid' => 'Objeto no válido',
            'UuidUsuario.size' => 'Objeto no válido',
            'UuidCurso.required' => 'Objeto no válido',
            'UuidCurso.uuid' => 'Objeto no válido',
            'UuidCurso.size' => 'Objeto no válido',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        $UuidUsuario = request('UuidUsuario');
        $UuidCurso = request('UuidCurso');

        return $this->create($UuidCurso, $UuidUsuario, true);
    }

    // destroy (R)
    public function destroy(Request $request)
    {
        // 1.- Validacion del request
        $rules = [
            'Uuid' => 'required|uuid|size:36',
            'Creado' => 'nullable|date_format:"Y-m-d\TH:i:sO"',
            'Actualizado' => 'nullable|date_format:"Y-m-d\TH:i:sO"',
        ];

        $msgs = [ // TODO *Customizable*
            'Uuid.required' => 'Objeto no válido',
            'Uuid.uuid' => 'Objeto no válido',
            'Uuid.size' => 'Objeto no válido',
            'Creado.required' => 'Validacion fallada en Creado.required',
            'Creado.date_format' => 'Validacion fallada en Creado.date_format',
            'Creado.nullable' => 'Validacion fallada en Creado.nullable',
            'Actualizado.required' => 'Validacion fallada en Actualizado.required',
            'Actualizado.date_format' => 'Validacion fallada en Actualizado.date_format',
            'Actualizado.nullable' => 'Validacion fallada en Actualizado.nullable',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        // 2.- Request a variables
        $uuid = request('Uuid');

        // 3.- Iniciar Transaccion
        $trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        $chcenttbl = \Chcenttbl::fnuchcenttbl($uuid, $trncnn);
        if (!$chcenttbl instanceof \Chcenttbl) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('$chcenttbl false');
        }

        $result = \Chcenttbl::rmvchcenttbl($chcenttbl->getIdnentchc(), $trncnn);

        // 6.- Commit & return
        if (!$result) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('Ocurrió un inesperado');
        }

        TransactionHandler::commit($trncnn);
        return ReturnHandler::rtrsccjsn('Eliminado correctamente');

    }

    // update (U)
    public function update(Request $request)
    {
        // 1.- Validacion del request TODO *Modificar*
        $rules = [
            'Uuid' => 'required|uuid|size:36',
            'UuidUsuario' => 'required|uuid|size:36',
            'UuidCurso' => 'required|uuid|size:36'
        ];

        $msgs = [
            'Uuid.required' => 'Validacion fallada en Uuid.required',
            'Uuid.uuid' => 'Validacion fallada en Uuid.uuid',
            'Uuid.size' => 'Validacion fallada en Uuid.size',
            'UuidUsuario.required' => 'Validacion fallada en Uuid.required',
            'UuidUsuario.uuid' => 'Validacion fallada en Uuid.uuid',
            'UuidUsuario.size' => 'Validacion fallada en Uuid.size',
            'UuidCurso.required' => 'Validacion fallada en Uuid.required',
            'UuidCurso.uuid' => 'Validacion fallada en Uuid.uuid',
            'UuidCurso.size' => 'Validacion fallada en Uuid.size',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        // 2.- Peticion a variables TODO *Modificar*
        $udxchcenttbl = request('Uuid');
        $timestamp = date(DATE_ISO8601);

        // 3.- Iniciar Transaccion
        $trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        $chcenttbl = \Chcenttbl::fnuchcenttbl($udxchcenttbl, $trncnn);
        if (!$chcenttbl instanceof \Chcenttbl) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('$chcenttbl false');
        }

        $data = [
            'idnentchc' => $chcenttbl->getIdnentchc(),
            'uuid' => $chcenttbl->getUuid(),
            'uidentusr' => request('UuidUsuario'),
            'uidentcrs' => request('UuidCurso'),
            'updated_at' => $timestamp
        ];

        $result = \Chcenttbl::updchcenttbl($data, $trncnn);

        // 6.- Commit & return
        if (!$result) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('Ocurrió un error inesperado');
        }

        TransactionHandler::commit($trncnn);
        return ReturnHandler::rtrsccjsn('Actualizado correctamente');
    }

    public function loadTable()
    {
        $recibos = \Chcenttbl::dspchcenttbl(0, 0, 0, 0);

        if (!$recibos) {
            return ReturnHandler::rtrerrjsn('No existe ningun recibo');
        }

        $recibos = $recibos->toArray();

        return json_encode([
            'success' => true,
            'data' => $recibos
        ]);
    }

    public function findOne(Request $request)
    {
        $rules = [
            'Uuid' => 'required|uuid|size:36',
        ];

        $msgs = [
            'Uuid.required' => 'Objeto no válido',
            'Uuid.uuid' => 'Objeto no válido uuid',
            'Uuid.size' => 'Objeto no válido tamaño',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        $uuid = request('Uuid');

        $checkout = \Chcenttbl::fnuchcenttbl($uuid);

        $checkout = $checkout->toArray();

        if (!$checkout) {
            return ReturnHandler::rtrerrjsn("No existen cursos");
        }

        return json_encode([
            'success' => true,
            'data' => $checkout
        ]);
    }

    public function findOneByBoth(Request $request)
    {
        $rules = [
            'UuidUsuario' => 'required|uuid|size:36',
            'UuidCurso' => 'required|uuid|size:36',
        ];

        $msgs = [
            'UuidUsuario.required' => 'Objeto no válido',
            'UuidUsuario.uuid' => 'Objeto no válido uuid',
            'UuidUsuario.size' => 'Objeto no válido tamaño',
            'UuidCurso.required' => 'Objeto no válido',
            'UuidCurso.uuid' => 'Objeto no válido uuid',
            'UuidCurso.size' => 'Objeto no válido tamaño',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        $uuidusuario = request('UuidUsuario');
        $uuidcurso = request('UuidCurso');

        $checkout = \Chcenttbl::fnfchcenttbl($uuidusuario, $uuidcurso);

        if (!$checkout) {
            return ReturnHandler::rtrerrjsn("No existen cursos");
        }

        $checkout = $checkout->toArray();

        return json_encode([
            'success' => true,
            'data' => $checkout
        ]);
    }

    public function loadList()
    {
        $checkouts = \Chcenttbl::dsplstchcenttbl();

        if (!$checkouts) {
            return ReturnHandler::rtrerrjsn('No existe ningun recibo');
        }

        $checkouts = $checkouts->toArray();

        return json_encode([
            'success' => true,
            'data' => $checkouts
        ]);
    }

    public function verifyCurso(Request $request)
    {
        // 1.- Validacion del request
        $rules = [
            'Idnentusr' => 'required|max:255',
            'Uidentcrs' => 'required|max:255',
        ];

        $msgs = [
            'Idnentusr.required' => 'Ocurrio un error inesperado',
            'Uidentcrs.required' => 'Ocurrio un error inesperado',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        // 2.- Peticion a variables
        $data = [
            'idnentusr' => request('Idnentusr'),
            'uidentcrs' => request('Uidentcrs'),
        ];

        $trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        $entcrs = \Crsenttbl::fndcrsenttbl($trncnn);

        // 6.- Commit y return
        if (!$entcrs) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('Por el momento no existen cursos');
        }

        $entchc = \Chcenttbl::fndusrcrs($data['id'], $trncnn);

        $entcrs = $entcrs->toArray();

        if ($entchc) {
            $entchc = $entchc->toArray();
        }


        foreach ($entcrs as $key => $rowcrs) {
            if ($entchc) {
                foreach ($entchc as $rowchc) {
                    if ($rowcrs['Idnentcrs'] == $rowchc['Idnentcrs']) {
                        $entcrs[$key]["Payed"] = true;
                        break;
                    } else {
                        $entcrs[$key]["Payed"] = false;
                    }
                }
            } else {
                $entcrs[$key]["Payed"] = false;
            }
        }

        TransactionHandler::commit($trncnn);

        return json_encode([
            'success' => true,
            'cursos' => $entcrs
        ]);
    }

    public function viewMercadoPago($cursouuid, $useruuid)
    {

        $curso = \Crsenttbl::fnucrsenttbl($cursouuid);

        if (!$curso) {
            return redirect("https://emprendedoresconproposito.online/mensaje/error/Ocurrio un error inesperado, este curso no existe, contacte al administrador para cualquier duda");
        }

        $curso = $curso->toArray();

        if ($curso['Prcentcrs'] <= 0) {
            return $this->create($cursouuid, $useruuid, false);
        }

        MercadoPago\SDK::setAccessToken('APP_USR-1739600810855248-032622-6b7137401f00f1ca2418a55c0c32f8be-539548469');
        MercadoPago\SDK::setClientId("1739600810855248");
        MercadoPago\SDK::setClientSecret("rMSTbM7mLJku8rTQnR2eQ4XrdBSVGNOV");

//        MercadoPago\SDK::setAccessToken('TEST-5557734786547208-021123-ac4c4f5bf1791060963265eaeede0bde-411103711');
//        MercadoPago\SDK::setClientId("5557734786547208");
//        MercadoPago\SDK::setClientSecret("yLm9UHOyFlJ4MhcCNFjL77TMmxPiKmpc");

        $preference = new MercadoPago\Preference();

        $item = new MercadoPago\Item();
        $item->id = $curso['Idnentcrs'];
        $item->title = $curso['Nmbentcrs'];
        $item->quantity = 1;
        $item->unit_price = $curso['Prcentcrs'];
        $item->currency_id = "MXN";
        $item->binary_mode = true;
        $preference->items = array($item);
        $preference->tracks = array(
            array(
                'type' => 'facebook_ad',
                'values' => array(
                    'pixel_id' => '606690429889135'
                )
            )
        );
        $preference->payment_methods = array(
            "excluded_payment_methods" => array(
                array("id" => "oxxo")
            ),
            "excluded_payment_types" => array(
                array("id" => "atm")
            ),
            "installments" => 12
        );
        $preference->back_urls = array(
            "success" => "https://emprendedoresconproposito.online/mensaje/success/¡Gracias por su compra!, le hemos enviado un correo con información y como recibo",
            "failure" => "https://emprendedoresconproposito.online/mensaje/error/Parece que en tu medio pago hubo un problema, contactalos para resolver la situación",
            "pending" => "https://emprendedoresconproposito.online/mensaje/warning/Parece que se esta procesando tu pago, ¡Bien!, revisa tu correo para alguna actualización"
        );
        $preference->auto_return = "approved";
        $preference->save();

        return view("pago")->with([
            "preference" => $preference,
            "curso" => $cursouuid,
            "user" => $useruuid,
        ]);
    }

    //TODO *CRUD Generator control separator line* (Don't remove this line!)
}
