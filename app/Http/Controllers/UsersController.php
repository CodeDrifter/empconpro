<?php

namespace App\Http\Controllers;

use App\Mail\MailContacto;
use App\ReturnHandler;
use App\TransactionHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Ramsey\Uuid\Uuid;
use Users;


class UsersController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        // return view('app.Users.main');
        $entcls = Users::dspusers();
        $entcls = array("data" => $entcls->toArray());

        return json_encode($entcls);
    }

    // store (C)
    public function create(Request $request)
    {
        // 1.- Validacion del request 
        $rules = [
            'Nombre' => 'required|max:255',
            'Correo' => 'required|email|max:255',
            'Contrasena' => 'required|max:255'
        ];

        $msgs = [
            'Nombre.required' => 'Por favor, escriba el nombre',
            'Nombre.string' => 'Por favor, escriba el nombre correctamente',
            'Nombre.max' => 'Por favor, escriba el nombre correctamente',
            'Correo.required' => 'Por favor, escriba el correo',
            'Correo.email' => 'Por favor, escriba el correo correctamente',
            'Correo.max' => 'Por favor, escriba el correo correctamente',
            'Contrasena.required' => 'Por favor, escriba la contraseña',
            'Contrasena.string' => 'Por favor, escriba la contraseña correctamente',
            'Contrasena.max' => 'Por favor, escriba la contraseña correctamente'
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        $uuid4 = Uuid::uuid4();
        $timestamp = date(DATE_ISO8601);

        // 2.- Peticion a variables 
        $data = [
            'uuid' => $uuid4,
            'name' => request('Nombre'),
            'email' => request('Correo'),
            'password' => bcrypt(request('Contrasena')),
			'rol' => 'user',
            'created_at' => $timestamp,
            'updated_at' => $timestamp
        ];

        // 3.- Iniciar transaccion
        $trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        $result = \Users::crtusers($data, $trncnn);

        // 6.- Commit y return
        if (!$result) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('No se pudo registrar el usuario');
        }

        TransactionHandler::commit($trncnn);
        return ReturnHandler::rtrsccjsn('Guardado correctamente');
    }

    // destroy (R)
    public function destroy(Request $request)
    {
        // 1.- Validacion del request
        $rules = [
            'Uuid' => 'required|uuid|size:36'
        ];

        $msgs = [ // TODO *Customizable*
            'Uuid.required' => 'Objeto no válido',
            'Uuid.uuid' => 'Objeto no válido',
            'Uuid.size' => 'Objeto no válido'
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        // 2.- Request a variables
        $uuid = request('Uuid');

        // 3.- Iniciar Transaccion
        $trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        $users = Users::fnuusers($uuid, $trncnn);
        if (!$users instanceof Users) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('$users false');
        }

        $result = Users::rmvusers($users->getId(), $trncnn);

        // 6.- Commit & return
        if (!$result) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('Ocurrió un inesperado');
        }

        TransactionHandler::commit($trncnn);
        return ReturnHandler::rtrsccjsn('Eliminado correctamente');

    }

    // update (U)
    public function update(Request $request)
    {
        // 1.- Validacion del request TODO *Modificar*
        $rules = [
            'Uuid' => 'required|uuid|size:36',
            'Correo' => 'required|max:255',
            'Nombre' => 'required|max:255',
            'Contrasena' => 'required|max:255'
        ];

        $msgs = [ // TODO *Customizable*
            'Uuid.required' => 'Este objeto no es valido',
            'Uuid.uuid' => 'Este objeto no es valido',
            'Uuid.size' => 'Este objeto no es valido',
            'Correo.required' => 'Por favor, escriba el correo',
            'Correo.string' => 'Por favor, escriba el correo correctamente',
            'Correo.max' => 'Por favor, escriba el correo correctamente',
            'Nombre.required' => 'Por favor, escriba el nombre',
            'Nombre.string' => 'Por favor, escriba el nombre correctamente',
            'Nombre.max' => 'Por favor, escriba el nombre correctamente',
            'Contrasena.required' => 'Por favor, escriba la nueva contraseña',
            'Contrasena.string' => 'Por favor, escriba la nueva contraseña correctamente',
            'Contrasena.max' => 'Por favor, escriba la nueva contraseña correctamente',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        // 2.- Peticion a variables TODO *Modificar*
        $udxusers = request('Uuid');
        $timestamp = date(DATE_ISO8601);

        // 3.- Iniciar Transaccion
        $trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        $users = Users::fnuusers($udxusers, $trncnn);
        if (!$users instanceof Users) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('$users false');
        }

        $data = [
            'id' => $users->getId(),
            'uuid' => $users->getUuid(),
            'email' => request('Correo'),
            'name' => request('Nombre'),
            'password' => bcrypt(request('Contrasena')),
            'rol' => 'user',
            'updated_at' => $timestamp,
        ];

        $result = Users::updusers($data, $trncnn);

        // 6.- Commit & return
        if (!$result) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('Ocurrió un error inesperado');
        }

        TransactionHandler::commit($trncnn);
        return ReturnHandler::rtrsccjsn('Actualizado correctamente');
    }

    public function loadTable() {
        $cursos = \Users::dspusers();

        if (!$cursos) {
            return ReturnHandler::rtrerrjsn('No existe ningun usuario');
        }

        $cursos = $cursos->toArray();

        return json_encode([
            'success' => true,
            'data' => $cursos
        ]);
    }

    public function loadList() {
        $cursos = \Users::dsplstusers();

        if (!$cursos) {
            return ReturnHandler::rtrerrjsn('No existe ningun usuario');
        }

        $cursos = $cursos->toArray();

        return json_encode([
            'success' => true,
            'data' => $cursos
        ]);
    }

    public function findOne(Request $request) {
        $rules = [
            'Uuid' => 'required|uuid|size:36',
        ];

        $msgs = [
            'Uuid.required' => 'Objeto no válido',
            'Uuid.uuid' => 'Objeto no válido',
            'Uuid.size' => 'Objeto no válido',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        $uuid = request('Uuid');

        $user = \Users::fnuusers($uuid);

        $user = $user->toArray();

        return json_encode([
            'success' => true,
            'data' => $user
        ]);
    }


    public function sendEmail(Request $request) {
        $rules = [
            'Name' => 'required|max:255',
            'Email' => 'required|email|max:255',
            'Content' => 'required',
        ];

        $msgs = [
            'Name.required' => 'Porfavor, escriba su nombre',
            'Name.max' => 'Porfavor, escriba correctamente su nombre',
            'Email.required' => 'Porfavor, escriba su correo',
            'Email.email' => 'Porfavor, escriba correctamente su correo',
            'Email.max' => 'Porfavor, escriba correctamente su correo',
            'Content.required' => 'Porfavor, escriba su mensaje',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        $data = [
            'name' => (String) request('Name'),
            'email' => (String) request('Email'),
            'content' => (String) request('Content')
        ];

        Mail::raw("Email enviado: " . $data["email"] .  ". Mensaje: " . $data["content"], function($message) use ($data) {
            $message->from($data["email"], $data["name"]);
            $message->subject($data["name"]);
            $message->to("emprendedoresconproposito01@gmail.com");
        });

        return ReturnHandler::rtrsccjsn("Se ha enviado el correo correctamente");
    }


    //TODO *CRUD Generator control separator line* (Don't remove this line!)
}
