<?php

namespace App\Http\Controllers;

use App\ReturnHandler;
use App\TransactionHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Crsenttbl;
use Exception;
use Ramsey\Uuid\Uuid;

class CrsenttblController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        // return view('app.Crsenttbl.main');
        $entcls = Crsenttbl::dspcrsenttbl();
        $entcls = array("data" => $entcls->toArray());

        return json_encode($entcls);
    }

    // store (C)
    public function create(Request $request)
    {
        // 1.- Validacion del request
        $rules = [
            'Nombre' => 'required|max:255',
            'Descripcion' => 'required',
            'Precio' => 'required|integer|max:99999999999',
            'Link' => 'required|max:255',
            'VideoPresentacion' => 'file|mimetypes:video/mp4',
            'DescripcionPresentacion' => 'required',
            'CantidadVideos' => 'required',
        ];

        $msgs = [
            'Nombre.required' => 'Por favor, introduzca el nombre del curso',
            'Nombre.string' => 'Por favor, introduzca el nombre del curso correctamente',
            'Nombre.max' => 'Por favor, introduzca el nombre del curso correctamente',
            'Descripcion.required' => 'Por favor, introduzca la descripción del curso',
            'Descripcion.string' => 'Por favor, introduzca la descripción del curso correctamente',
            'Descripcion.max' => 'Por favor, introduzca la descripción del curso correctamente',
            'Precio.required' => 'Por favor, introduzca el precio del curso',
            'Precio.integer' => 'Por favor, introduzca el precio del curso correctamente',
            'Precio.min' => 'Por favor, introduzca el precio del curso correctamente',
            'Precio.max' => 'Por favor, introduzca el precio del curso correctamente',
            'Link.required' => 'Por favor, introduzca el link del webinar del curso',
            'Link.string' => 'Por favor, introduzca el link del webinar del curso correctamente',
            'Link.max' => 'Por favor, introduzca el link del webinar del curso correctamente',
            'VideoPresentacion.file' => 'Por favor, suba el video de presentación',
            'VideoPresentacion.mimetypes' => 'Por favor, suba el video en formato mp4',
            'DescripcionPresentacion.required' => 'Por favor, escriba el texto de presentación',
            'CantidadVideos.required' => 'Por favor, sube videos',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        $VideoPresentacion = $request->file('VideoPresentacion');
        $DescripcionPresentacion = request('DescripcionPresentacion');
        $CantidadVideos = request('CantidadVideos');

        for ($i = 0; $i < $CantidadVideos; $i++) {
            if (!$request->hasFile('video' . $i)) {
                return ReturnHandler::rtrerrjsn("Por favor, revisa los archivos, quizas este corrupto o no es archivo");
            }

            if (request('descripcion' . $i) == '') {
                return ReturnHandler::rtrerrjsn("Por favor, revisa las descripciones, una esta incompleta");
            }
        }
        // 2.- Peticion a variables

        $uuid4 = Uuid::uuid4();

        $data = [
            'uuid' => $uuid4,
            'nmbentcrs' => request('Nombre'),
            'dscentcrs' => request('Descripcion'),
            'prcentcrs' => request('Precio'),
            'lnkentcrs' => request('Link'),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];

        // 3.- Iniciar transaccion
        $trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        $curso = \Crsenttbl::crtcrsenttbl($data, $trncnn);

        // 6.- Commit y return
        if (!$curso) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('Ocurrio un error inesperado');
        }

        if (!Storage::exists('public')) {
            Storage::makeDirectory('public');
        } if (!Storage::exists('public/cursos')) {
            Storage::makeDirectory('public/cursos');
        }

        Storage::makeDirectory('public/cursos/' . $data["uuid"]);
        Storage::makeDirectory('public/cursos/' . $data["uuid"] . "/videos");
        Storage::makeDirectory('public/cursos/' . $data["uuid"] . "/desc");

        try {
            Storage::putFileAs('public/cursos/' . $data["uuid"] . '/videos', $VideoPresentacion, '00.-_presentacion.mp4');
            Storage::put('public/cursos/' . $data["uuid"] . '/desc/00.-_presentacion.txt', $DescripcionPresentacion);

            for ($i = 0; $i < $CantidadVideos; $i++) {
                $video = request('video' . $i);
                $desc = request('descripcion' . $i);
                $videonombre = $video->getClientOriginalName();
                $videonombre = str_replace('.mp4', '', $videonombre);

                Storage::put('public/cursos/' . $data["uuid"] . '/desc/'. $videonombre . '.txt', $desc);
                Storage::putFileAs('public/cursos/' . $data["uuid"] . '/videos', $video, $videonombre . '.mp4');
            }
        } catch (Exception $e) {
            TransactionHandler::rollback($trncnn);
            Log::debug($e);
            return ReturnHandler::rtrerrjsn('Los videos no pudieron ser almacenados');
        }

        TransactionHandler::commit($trncnn);
        return ReturnHandler::rtrsccjsn('Guardado correctamente');
    }

    // destroy (R)
    public function destroy(Request $request)
    {
        // 1.- Validacion del request
        $rules = [
            'Uuid' => 'required|uuid|size:36'
        ];

        $msgs = [ // TODO *Customizable*
            'Uuid.required' => 'Objeto no válido',
            'Uuid.uuid' => 'Objeto no válido',
            'Uuid.size' => 'Objeto no válido'
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        // 2.- Request a variables
        $uuid = request('Uuid');

        // 3.- Iniciar Transaccion
        $trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        $crsenttbl = \Crsenttbl::fnucrsenttbl($uuid, $trncnn);
        if (!$crsenttbl instanceof \Crsenttbl) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('$crsenttbl false');
        }

        $result = \Crsenttbl::rmvcrsenttbl($crsenttbl->getIdnentcrs(), $trncnn);

        // 6.- Commit & return
        if (!$result) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('Ocurrió un inesperado');
        }

        TransactionHandler::commit($trncnn);
        return ReturnHandler::rtrsccjsn('Eliminado correctamente');

    }

    // update (U)
    public function update(Request $request)
    {
        // 1.- Validacion del request
        $rules = [
            'Uuid' => 'required|uuid|size:36',
            'Nombre' => 'required|max:255',
            'Descripcion' => 'required|max:255',
            'Precio' => 'required|integer|min:0|max:99999999999',
			'Link' => 'required|max:255',
        ];

        $msgs = [ // TODO *Customizable*
            'Uuid.required' => 'Objeto no válido',
            'Uuid.uuid' => 'Objeto no válido',
            'Uuid.size' => 'Objeto no válido',
            'Nmbentcrs.required' => 'Validacion fallada en Nombre.required',
            'Nmbentcrs.string' => 'Validacion fallada en Nombre.string',
            'Nmbentcrs.max' => 'Validacion fallada en Nombre.max',
            'Dscentcrs.required' => 'Validacion fallada en Descripcion.required',
            'Dscentcrs.string' => 'Validacion fallada en Descripcion.string',
            'Dscentcrs.max' => 'Validacion fallada en Descripcion.max',
            'Prcentcrs.required' => 'Validacion fallada en Precio.required',
            'Prcentcrs.integer' => 'Validacion fallada en Precio.integer',
            'Prcentcrs.min' => 'Validacion fallada en Precio.min',
            'Prcentcrs.max' => 'Validacion fallada en Precio.max',
            'Link.required' => 'Validacion fallada en Nombre.required',
            'Link.string' => 'Validacion fallada en Nombre.string',
            'Link.max' => 'Validacion fallada en Nombre.max',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        // 2.- Peticion a variables TODO *Modificar*
        $udxcrsenttbl = request('Uuid');
        $timestamp = date(DATE_ISO8601);

        // 3.- Iniciar Transaccion
        $trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        $crsenttbl = \Crsenttbl::fnucrsenttbl($udxcrsenttbl, $trncnn);
        if (!$crsenttbl instanceof \Crsenttbl) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('$crsenttbl false');
        }

        $data = [
            'idnentcrs' => $crsenttbl->getIdnentcrs(),
            'uuid' => $crsenttbl->getUuid(),
            'nmbentcrs' => request('Nombre'),
            'dscentcrs' => request('Descripcion'),
            'prcentcrs' => request('Precio'),
            'lnkentcrs' => request('Link'),
            'updated_at' => $timestamp,
        ];

        $result = \Crsenttbl::updcrsenttbl($data, $trncnn);

        // 6.- Commit & return
        if (!$result) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('Ocurrió un error inesperado');
        }

        TransactionHandler::commit($trncnn);
        return ReturnHandler::rtrsccjsn('Actualizado correctamente');
    }

    public function loadTable() {
        $cursos = \Crsenttbl::dspcrsenttbl();

        if (!$cursos) {
            return ReturnHandler::rtrerrjsn('No existe ningun curso por el momento');
        }

        $cursos = $cursos->toArray();

        if (empty($cursos)) {
            return ReturnHandler::rtrerrjsn('No existe ningun curso por el momento');
        }

        return json_encode([
            'success' => true,
            'data' => $cursos
        ]);
    }

    public function loadList() {
        $cursos = \Crsenttbl::dsplstcrsenttbl();

        if (!$cursos) {
            return ReturnHandler::rtrerrjsn('No existe ningun curso');
        }

        $cursos = $cursos->toArray();

        return json_encode([
            'success' => true,
            'data' => $cursos
        ]);
    }

    public function findOne(Request $request) {
        $rules = [
            'Uuid' => 'required|uuid|size:36',
        ];

        $msgs = [
            'Uuid.required' => 'Objeto no válido',
            'Uuid.uuid' => 'Objeto no válido uuid',
            'Uuid.size' => 'Objeto no válido tamaño',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        $uuid = request('Uuid');

        $curso = \Crsenttbl::fnucrsenttbl($uuid);

        if (!$curso) {
            return ReturnHandler::rtrerrjsn('No existe el curso');
        }

        $curso = $curso->toArray();

        try {
            $descripcion = Storage::get('public/cursos/' . $uuid . '/desc/00.-_presentacion.txt');
            $curso['Descripcion'] = $descripcion;
        } catch(Exception $e) {
            Log::debug($e);
            return ReturnHandler::rtrerrjsn('Ocurrio un error al recuperar información del curso');
        }


        return json_encode([
            'success' => true,
            'data' => $curso
        ]);
    }

    public function findUsersByCurso(Request $request) {
        $rules = [
            'Uuid' => 'required|uuid|size:36',
        ];

        $msgs = [
            'Uuid.required' => 'Objeto no válido',
            'Uuid.uuid' => 'Objeto no válido uuid',
            'Uuid.size' => 'Objeto no válido tamaño',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        $uuid = request('Uuid');

        $users = \Crsenttbl::fndusrenttbl($uuid);

        if (!$users) {
                return ReturnHandler::rtrerrjsn('No existen usuarios en el curso');
        }

        $users = $users->toArray();

        return json_encode([
            'success' => true,
            'data' => $users
        ]);
    }

    public function fetchCurso(Request $request)
    {
        // 1.- Validacion del request
        $rules = [
            'Id' => 'required|max:255',
            'Uuid' => 'required|max:255',
            'Select' => 'required|max:255',
        ];

        $msgs = [
            'Id.required' => 'Ocurrio un error inesperado 1',
            'Uuid.required' => 'Ocurrio un error inesperado 2',
            'Select.required' => 'Ocurrio un error inesperado 3',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        // 2.- Peticion a variables
        $data = [
            'id' => request('Id'),
            'uuid' => request('Uuid'),
            'select' => request('Select'),
        ];

        // 4 & 5 .- Variables a objeto & Regla de negocio
        $entcrs = \Crsenttbl::fnucrsenttbl($data['uuid']);

        // 6.- Commit y return
        if (!$entcrs) {
            return ReturnHandler::rtrerrjsn('Ocurrio un error al recuperar el video, intente mas tarde');
        }

        $entcrs = $entcrs->toArray();

        $urls = Storage::disk('local')->files('public/cursos/' . $data["uuid"] . '/videos');
        $descripciones = [];
        $subtitulos = [];

        foreach ($urls as $fileurl) {
            $filename = substr($fileurl, strrpos($fileurl, '/') + 1);

            $filename = str_replace('.mp4', '', $filename);
            array_push($descripciones, $filename);

            $filename = str_replace('_', ' ', $filename);
            array_push($subtitulos, $filename);
        }

        sort($subtitulos);
        sort($descripciones);
        sort($urls);

        try {

            $url = Storage::url($urls[$data["select"]]);
            $desc = Storage::url($descripciones[$data["select"]]);

            $url = "https://emprendedoresconproposito.online" . $url;
            $filename = substr($url, strrpos($url, '/') + 1);
            $filename = str_replace('.mp4', '', $filename);

            $desc = File::get(storage_path('app/public/cursos//' . $data["uuid"] . '/desc//' .  $filename . '.txt'));
        } catch (Exception $e) {
            Log::debug($e);
            return ReturnHandler::rtrerrjsn('Este video no esta disponible, intente más tarde');
        }


        return json_encode([
            'success' => true,
            'url' => $url,
            'lista' => $subtitulos,
            'titulo' => $subtitulos[$data["select"]],
            'descripcion' => $desc
        ]);
    }

    public function fetchCursosUser(Request $request)
    {
        // 1.- Validacion del request
        $rules = [
            'Uuid' => 'required|max:255',
        ];

        $msgs = [
            'Uuid.required' => 'Ocurrio un error inesperado',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        // 2.- Peticion a variables
        $data = [
            'uuid' => request('Uuid'),
        ];

        $cursos = \Crsenttbl::fndbyuenttbl($data['uuid']);

        if (!$cursos) {
            return ReturnHandler::rtrerrjsn('No existe ningun curso');
        }

        $cursos = $cursos->toArray();

        if (empty($cursos)) {
            return ReturnHandler::rtrerrjsn('No estas inscrito a ningun curso');
        }

        return json_encode([
            'success' => true,
            'data' => $cursos
        ]);
    }

    public function fetchListaUser(Request $request)
    {
        // 1.- Validacion del request
        $rules = [
            'Id' => 'required|max:255',
        ];

        $msgs = [
            'Id.required' => 'Ocurrio un error inesperado 1',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        // 2.- Peticion a variables
        $data = [
            'id' => request('Id'),
        ];

        $trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        $entcrs = \Chcenttbl::fnousrenttbl($data['id'], $trncnn);

        // 6.- Commit y return
        if (!$entcrs) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('Por el momento no existen cursos');
        }

        $entchc = \Chcenttbl::fndusrcrs($data['id'], $trncnn);

        $entcrs = $entcrs->toArray();

        if ($entchc) {
            $entchc = $entchc->toArray();
        }


        foreach ($entcrs as $key => $rowcrs) {
            if ($entchc) {
                foreach ($entchc as $rowchc) {
                    if ($rowcrs['Idnentcrs'] == $rowchc['Idnentcrs']) {
                        continue;
                    } else {
                        unset($entcrs[$key]);
                    }
                }
            } else {
                unset($entcrs[$key]);
            }
        }

        TransactionHandler::commit($trncnn);

        return json_encode([
            'success' => true,
            'cursos' => $entcrs
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }


    //TODO *CRUD Generator control separator line* (Don't remove this line!)

}
