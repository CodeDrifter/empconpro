<?php

namespace App\Http\Controllers;

use App\ReturnHandler;
use App\TransactionHandler;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Prbenttbl;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints\DateTime;

class PrbenttblController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        // return view('app.Prbenttbl.main');
        $entcls = Prbenttbl::dspprbenttbl(0);
        $entcls = array("data" => $entcls->toArray());

        return json_encode($entcls);
    }

    // store (C)
    public function create(Request $request)
    {
        // 1.- Validacion del request
        $rules = [
            'UuidUsuario' => 'nullable|uuid|size:36',
            'UuidCurso' => 'required|uuid|size:36',
            'Email' => 'required|email',
            'Name' => 'required',
            'Video' => 'required|integer|min:0|max:99999999999',
            'Horario' => 'required|date_format:"Y-m-d H:i:s"'
        ];

        $msgs = [
            'UuidUsuario.uuid' => 'Este Usuario no existe',
            'UuidUsuario.size' => 'Este Usuario no existe',
            'UuidCurso.required' => 'Este curso no existe',
            'UuidCurso.uuid' => 'Este curso no existe',
            'UuidCurso.size' => 'Este curso no existe',
            'Email.required' => 'Introdusca un correo electronico',
            'Email.email' => 'Introdusca un correo electronico valido',
            'Name.required' => 'Introdusca un nombre',
            'Video.required' => 'Ocurrio un error inesperado',
            'Video.integer' => 'Ocurrio un error inesperado',
            'Video.min' => 'Ocurrio un error inesperado',
            'Video.max' => 'Ocurrio un error inesperado',
            'Horario.required' => 'Escoja un horario',
            'Horario.date_format' => 'Escoja un horario de nuevo',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        $trncnn = TransactionHandler::begin();
        $useruuid = request('UuidUsuario');
        $cursouuid = request('UuidCurso');

        $entusr = \Users::fnuusers($useruuid, $trncnn);
        if (!$entusr) {
            $entusr["Id"] = null;
            $entusr["Uuid"] = null;
        } else {
            $entusr = $entusr->toArray();
        }

        $entcrs = \Crsenttbl::fnucrsenttbl($cursouuid, $trncnn);
        if (!$entcrs) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('No existe el curso, intente mas tarde');
        }

        $entcrs = $entcrs->toArray();

        // 2.- Peticion a variables
        $uuid = Uuid::uuid4();
        $timestamp = date(DATE_ISO8601);
        $email = request('Email');
        $name = request('Name');

        $data = [
            'uuid' => $uuid,
            'idnentusr' => $entusr["Id"],
            'uidentusr' => $entusr["Uuid"],
            'idnentcrs' => $entcrs["Idnentcrs"],
            'uidentcrs' => $entcrs["Uuid"],
            'videntprb' => request('Video'),
            'hrrentprb' => request('Horario'),
            'emlentprb' => $email,
            'stdentprb' => 1,
            'created_at' => $timestamp,
            'updated_at' => $timestamp
        ];

        // 4 & 5 .- Variables a objeto & Regla de negocio TODO *Modificar*

        $result = \Prbenttbl::crtprbenttbl($data, $trncnn);

        // 6.- Commit y return
        if (!$result) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('No se pudo crear el evento');
        }

        $result = $result->toArray();

        try {
            $data = array(
                'name' => $name,
                'messages' => "Vimos que registraste tu horario para el webinar gratuito, aqui esta el link para acceder: ",
                'link' => "https://emprendedoresconproposito.online/curso/vivo/" . $result["Uuid"],
                'linkstring' => "Ver Webinar gratuito",
                'goodbye' => "Las 3 técnicas fundamentales para crear una empresa exitosa en el mercado sin perder tiempo ni dinero en el intento.",
                'autor' => "- Adilene Ramos",
            );

            Mail::send('email.regular', $data, function ($message) use ($email) {
                $message->from("emprendedoresconproposito01@gmail.com", "Emprendedores con proposito");
                $message->subject("Invitación a curso");
                $message->to($email);
            });
        } catch (Exception $e) {
            Log::debug($e);
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('Ocurrio un problema para enviar tu correo, intente mas tarde');
        }


        TransactionHandler::commit($trncnn);
        return ReturnHandler::rtrsccjsn('Evento creado, enviamos un correo con informacion al respecto del video en vivo');
    }

    // update (U)
    public function check(Request $request)
    {
        $uuiprbenttbl = request('Uuid');
        $prbenttbl = \Prbenttbl::fnuprbenttbl($uuiprbenttbl);

        Log::debug(request('Bandera'));
        $data = [
            'idnentprb' => $prbenttbl->getIdnentprb(),
            'uuid' => $prbenttbl->getUuid(),
            'stdentprb' => request('Estado'),
            'bndentprb' => request('Bandera'),
            'hrrentprb' => $prbenttbl->getHrrentprb(),
            'vstentprb' => request('Tiempo'),
        ];

        $result = \Prbenttbl::updprbenttbl($data);
    }

    // update (U)
    public function update(Request $request)
    {
        // 1.- Validacion del request TODO *Modificar*
        $rules = [
            'Uuid' => 'required|uuid|size:36',
            'Video' => 'required|integer|min:0|max:99999999999',
            'Horario' => 'required|date_format:"Y-m-d\TH:i:sO"',
            'Estado' => 'required|integer|min:0|max:99999999999',
            'Borrado' => 'required|boolean',
            'Creado' => 'nullable|date_format:"Y-m-d\TH:i:sO"',
            'Actualizado' => 'nullable|date_format:"Y-m-d\TH:i:sO"',
        ];

        $msgs = [ // TODO *Customizable*
            'Uuid.required' => 'Validacion fallada en Uuid.required',
            'Uuid.uuid' => 'Validacion fallada en Uuid.uuid',
            'Uuid.size' => 'Validacion fallada en Uuid.size',
            'Video.required' => 'Validacion fallada en Video.required',
            'Video.integer' => 'Validacion fallada en Video.integer',
            'Video.min' => 'Validacion fallada en Video.min',
            'Video.max' => 'Validacion fallada en Video.max',
            'Horario.required' => 'Validacion fallada en Horario.required',
            'Horario.date_format' => 'Validacion fallada en Horario.date_format',
            'Estado.required' => 'Validacion fallada en Estado.required',
            'Estado.integer' => 'Validacion fallada en Estado.integer',
            'Estado.min' => 'Validacion fallada en Estado.min',
            'Estado.max' => 'Validacion fallada en Estado.max',
            'Borrado' => 'required|boolean',
            'Borrado.required' => 'Validacion fallada en Borrado.required',
            'Borrado.boolean' => 'Validacion fallada en Borrado.boolean',
            'Creado.required' => 'Validacion fallada en Creado.required',
            'Creado.date_format' => 'Validacion fallada en Creado.date_format',
            'Actualizado.required' => 'Validacion fallada en Actualizado.required',
            'Actualizado.date_format' => 'Validacion fallada en Actualizado.date_format',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        // 2.- Peticion a variables TODO *Modificar*
        $uuiprbenttbl = request('Uuid');
        $timestamp = date(DATE_ISO8601);

        // 3.- Iniciar Transaccion
        $trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        $prbenttbl = \Prbenttbl::fnuprbenttbl($uuiprbenttbl, $trncnn);
        if (!$prbenttbl instanceof \Prbenttbl) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('$prbenttbl false');
        }


        $data = [
            'idnentprb' => $prbenttbl->getIdnentprb(),
            'uuid' => $prbenttbl->getUuid(),
            'videntprb' => request('Video'),
            'hrrentprb' => request('Horario'),
            'stdentprb' => request('Estado'),
            'rmventprb' => request('Borrado'),
            'created_at' => request('Creado'),
            'updated_at' => request('Actualizado'),
            'updated_at' => $timestamp
        ];

        $result = \Prbenttbl::updprbenttbl($data, $trncnn);

        // 6.- Commit & return
        if (!$result) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('Ocurrió un error inesperado');
        }

        TransactionHandler::commit($trncnn);
        return ReturnHandler::rtrsccjsn('Actualizado correctamente');
    }

    // destroy (R)
    public function destroy(Request $request)
    {
        // 1.- Validacion del request
        $rules = [
            'Uuid' => 'required|uuid|size:36',
        ];

        $msgs = [ // TODO *Customizable*
            'Uuid.required' => 'Objeto no válido',
            'Uuid.uuid' => 'Objeto no válido',
            'Uuid.size' => 'Objeto no válido',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        // 2.- Request a variables
        $uuid = request('Uuid');

        // 3.- Iniciar Transaccion
        $trncnn = TransactionHandler::begin();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        $prbenttbl = \Prbenttbl::fnuprbenttbl($uuid, $trncnn);
        if (!$prbenttbl instanceof \Prbenttbl) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('$prbenttbl false');
        }

        $result = \Prbenttbl::rmvprbenttbl($prbenttbl->getIdnentprb(), $trncnn);

        // 6.- Commit & return
        if (!$result) {
            TransactionHandler::rollback($trncnn);
            return ReturnHandler::rtrerrjsn('Ocurrió un inesperado');
        }

        TransactionHandler::commit($trncnn);
        return ReturnHandler::rtrsccjsn('Eliminado correctamente');

    }


// Views

    // Show table(D)
    public function table(Request $request)
    {

    }

    // Display one(D)
    public function show(Request $request)
    {
        $id = request('id');
        $prbenttbl = Prbenttbl::fnoprbenttbl($id);
        if (!$prbenttbl)
            return ReturnHandler::rtrerrjsn("");
        $prbenttbl = array("data" => $prbenttbl->toArray());

        return json_encode($prbenttbl);
    }

    public function findOne(Request $request)
    {
        $rules = [
            'Uuid' => 'required|uuid|size:36',
        ];

        $msgs = [
            'Uuid.required' => 'Objeto no válido',
            'Uuid.uuid' => 'Objeto no válido',
            'Uuid.size' => 'Objeto no válido',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        $uuid = request('Uuid');

        $prueba = \Prbenttbl::fnuprbenttbl($uuid);

        $prueba = $prueba->toArray();

        if (!$prueba) {
            return ReturnHandler::rtrerrjsn("No existe el evento");
        }

        return json_encode([
            'success' => true,
            'data' => $prueba
        ]);
    }

    public function fetchVideo(Request $request)
    {
        // 1.- Validacion del request
        $rules = [
            'Uuid' => 'required|max:255',
        ];

        $msgs = [
            'Uuid.required' => 'Ocurrio un error inesperado',
        ];

        $validator = Validator::make($request->toArray(), $rules, $msgs)->errors()->all();

        if (!empty($validator)) {
            return ReturnHandler::rtrerrjsn($validator[0]);
        }

        // 2.- Peticion a variables
        $data = [
            'uuid' => request('Uuid'),
        ];

        $entprb = \Prbenttbl::fnuprbenttbl($data['uuid']);

        if (!$entprb) {
            return ReturnHandler::rtrerrjsn('Ocurrio un error al recuperar el video, intente mas tarde');
        }

        $entprb = $entprb->toArray();

        // 4 & 5 .- Variables a objeto & Regla de negocio
        $entcrs = \Crsenttbl::fnucrsenttbl($entprb['Uidentcrs']);

        // 6.- Commit y return
        if (!$entcrs) {
            return ReturnHandler::rtrerrjsn('Ocurrio un error al recuperar el video, intente mas tarde');
        }

        $entcrs = $entcrs->toArray();

        $urls = Storage::disk('local')->files('public/cursos/' . $entprb['Uidentcrs'] . '/videos');
        $descripciones = [];
        $subtitulos = [];

        foreach ($urls as $fileurl) {
            $filename = substr($fileurl, strrpos($fileurl, '/') + 1);

            $filename = str_replace('.mp4', '', $filename);
            array_push($descripciones, $filename);

            $filename = str_replace('_', ' ', $filename);
            array_push($subtitulos, $filename);
        }

        sort($subtitulos);
        sort($descripciones);
        sort($urls);

        $duration = 0;

        try {

            $url = Storage::url($urls[$entprb["Videntprb"]]);

            $getID3 = new \getID3;
            $url2 = str_replace('/storage/', '/', $url);
            $file = $getID3->analyze(storage_path('app/public' .  $url2));

            $duration = $file['playtime_seconds'];
            $duration = ceil($duration);
            $url = "https://emprendedoresconproposito.online" . $url;
            $start = $entprb["Hrrentprb"];
            $end = date("Y-m-d H:i:s", strtotime("+ " . $duration . " seconds", strtotime($entprb["Hrrentprb"])));

            $filename = substr($url, strrpos($url, '/') + 1);
            $filename = str_replace('.mp4', '', $filename);

            $desc = File::get(storage_path('app/public/cursos//' . $entprb['Uidentcrs'] . '/desc//' . $filename . '.txt'));
        } catch (Exception $e) {
            Log::debug($e);
            return ReturnHandler::rtrerrjsn('Este video no esta disponible, intente más tarde');
        }


        return json_encode([
            'success' => true,
            'data' => $entprb,
            'url' => $url,
            'lista' => $subtitulos,
            'titulo' => $subtitulos[$entprb["Videntprb"]],
            'descripcion' => $desc,
            'start' => $start,
            'end' => $end,
            'checkoutuuid' => $entcrs["Lnkentcrs"]
        ]);
    }
}
