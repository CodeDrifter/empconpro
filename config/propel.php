<?php
return [
    'propel' => [
        'database' => [
            'connections' => [
                'empconpro' => [
                    'adapter' => 'mysql',
                    'dsn' => 'mysql:host=127.0.0.1;dbname=empconpro',
                    'user' => 'empconpro',
                    'password' => 'empconpro',
                    'settings' => [
                        'charset' => 'utf8'
                    ]
                ]
            ]
        ],
        'runtime' => [
            'defaultConnection' => 'empconpro',
            'connections' => ['empconpro']
        ],
        'generator' => [
            'defaultConnection' => 'empconpro',
            'connections' => ['empconpro'],
            'dateTime' => [
                'defaultTimeStampFormat' =>'Y-m-d H:i:s',
                'defaultTimeFormat' => 'H:i:s',
                'defaultDateFormat' => 'Y-m-d'
            ]
        ]
    ]
];
?>
