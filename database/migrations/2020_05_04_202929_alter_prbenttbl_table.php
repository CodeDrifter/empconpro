<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterPrbenttblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prbenttbl', function (Blueprint $table) {
            $table->string('emlentprb')->nullable()->default(null)->comment('Correo');
            $table->string('stdentprb')->nullable()->default('No visto')->comment('Estado')->change();
            $table->boolean('bndentprb')->default(false)->comment('BanderaVisto');
            $table->string('vstentprb')->nullable()->comment('HorarioVisto');
            DB::statement("ALTER TABLE `prbenttbl` CHANGE COLUMN `hrrentprb` `hrrentprb` TIMESTAMP NULL DEFAULT current_timestamp() COMMENT 'Horario' AFTER `videntprb`;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
