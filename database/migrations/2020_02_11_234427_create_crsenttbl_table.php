<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCrsenttblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crsenttbl', function (Blueprint $table) {
            $table->increments('idnentcrs')->comment('Id');
            $table->uuid('uuid')->unique()->comment('Uuid');
            $table->string('nmbentcrs')->comment('Nombre');
            $table->mediumText('dscentcrs')->comment('Descripcion');
            $table->integer('prcentcrs')->comment('Precio');
            $table->string('lnkentcrs')->nullable()->comment('Link');
            $table->boolean('rmventcrs')->comment('Borrado')->default(false);
            $table->timestampTz('created_at')->nullable()->comment('Creado');
            $table->timestampTz('updated_at')->nullable()->comment('Actualizado');
        });

        $table = "crsenttbl";
        $comment = "Cursos";

        DB::statement("ALTER TABLE " . $table . " COMMENT = '" . $comment . "'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crsenttbl');
    }
}
