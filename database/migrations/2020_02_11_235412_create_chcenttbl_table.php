<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateChcenttblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chcenttbl', function (Blueprint $table) {
            $table->increments('idnentchc')->comment('Id');
            $table->uuid('uuid')->unique()->comment('Uuid');

            $table->unsignedInteger('idnentusr')->nullable()->comment('Internal');
            $table->foreign('idnentusr')->references('id')->on('users');
            $table->uuid('uidentusr')->nullable()->comment('Internal');
            $table->foreign('uidentusr')->references('uuid')->on('users');

            $table->unsignedInteger('idnentcrs')->nullable()->comment('Internal');
            $table->foreign('idnentcrs')->references('idnentcrs')->on('crsenttbl');
            $table->uuid('uidentcrs')->nullable()->comment('Internal');
            $table->foreign('uidentcrs')->references('uuid')->on('crsenttbl');

            $table->boolean('rmventchc')->comment('Borrado')->default(false);
            $table->timestampTz('created_at')->nullable()->comment('Creado');
            $table->timestampTz('updated_at')->nullable()->comment('Actualizado');
        });

        $table = "chcenttbl";
        $comment = "Checkout";

        DB::statement("ALTER TABLE " . $table . " COMMENT = '" . $comment . "'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chcenttbl');
    }
}
