<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->comment('Id');
            $table->uuid('uuid')->unique()->comment('Uuid');
            $table->string('email')->comment('Correo');
            $table->string('name')->comment('Nombre');
            $table->timestamp('email_verified_at')->nullable()->comment('Internal');
            $table->string('password')->comment('Contraseña');
            $table->string('rol')->comment('Rol');
            $table->boolean('rmv')->comment('Borrado')->default(false);
            $table->timestampTz('created_at')->nullable()->comment('Creado');
            $table->timestampTz('updated_at')->nullable()->comment('Actualizado');
            $table->rememberToken()->comment('Internal');
        });

        $table = "users";
        $comment = "Usuarios";

        DB::statement("ALTER TABLE " . $table . " COMMENT = '" . $comment . "'");
    }
      /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
