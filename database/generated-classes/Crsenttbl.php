<?php

use Base\Crsenttbl as BaseCrsenttbl;
use Illuminate\Support\Facades\Log;

class Crsenttbl extends BaseCrsenttbl
{
    public static function crtcrsenttbl(array $data, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $crs = new \Crsenttbl();
        try {
            if (array_key_exists('uuid', $data)) {
                if (!is_null($data['uuid'])) {
                    $crs->setUuid($data['uuid']);
                } else {
                    throw new \Propel\Runtime\Exception\PropelException('uuid cannot be null');
                }
            }
            if (array_key_exists('nmbentcrs', $data)) {
                if (!is_null($data['nmbentcrs'])) {
                    $crs->setNmbentcrs($data['nmbentcrs']);
                } else {
                    throw new \Propel\Runtime\Exception\PropelException('nmbentcrs cannot be null');
                }
            }
            if (array_key_exists('dscentcrs', $data)) {
                if (!is_null($data['dscentcrs'])) {
                    $crs->setDscentcrs($data['dscentcrs']);
                } else {
                    throw new \Propel\Runtime\Exception\PropelException('dscentcrs cannot be null');
                }
            }
            if (array_key_exists('prcentcrs', $data)) {
                if (!is_null($data['prcentcrs'])) {
                    $crs->setPrcentcrs($data['prcentcrs']);
                } else {
                    throw new \Propel\Runtime\Exception\PropelException('prcentcrs cannot be null');
                }
            }
            if (array_key_exists('lnkentcrs', $data)) {
                if (!is_null($data['lnkentcrs'])) {
                    $crs->setLnkentcrs($data['lnkentcrs']);
                }
            }
            if (array_key_exists('created_at', $data)) {
                if (!is_null($data['created_at'])) {
                    $crs->setCreatedAt($data['created_at']);
                }
            }
            if (array_key_exists('updated_at', $data)) {
                if (!is_null($data['updated_at'])) {
                    $crs->setUpdatedAt($data['updated_at']);
                }
            }
            $crs->save($connection);
        } catch (\Propel\Runtime\Exception\PropelException $e) {
            Illuminate\Support\Facades\Log::debug($e);
            return false;
        }
        return $crs;
    }

    public static function rmvcrsenttbl($idnentcrs, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $crs = \CrsenttblQuery::create()
            ->filterByIdnentcrs($idnentcrs)
            ->findOne($connection);

        if (!$crs) return false;

        try {
            $crs->delete($connection);
        } catch (\Propel\Runtime\Exception\PropelException $e) {
            Illuminate\Support\Facades\Log::debug($e);
            return false;
        }

        return true;
    }

    public static function updcrsenttbl(array $data, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $crs = \CrsenttblQuery::create()
            ->filterByIdnentcrs($data['idnentcrs'])
            ->findOne($connection);

        if (!$crs) return false;

        try {
            if (array_key_exists('uuid', $data)) {
                if (!is_null($data['uuid'])) {
                    $crs->setUuid($data['uuid']);
                } else {
                    throw new \Propel\Runtime\Exception\PropelException('uuid cannot be null');
                }
            }
            if (array_key_exists('nmbentcrs', $data)) {
                if (!is_null($data['nmbentcrs'])) {
                    $crs->setNmbentcrs($data['nmbentcrs']);
                } else {
                    throw new \Propel\Runtime\Exception\PropelException('nmbentcrs cannot be null');
                }
            }
            if (array_key_exists('dscentcrs', $data)) {
                if (!is_null($data['dscentcrs'])) {
                    $crs->setDscentcrs($data['dscentcrs']);
                } else {
                    throw new \Propel\Runtime\Exception\PropelException('dscentcrs cannot be null');
                }
            }
            if (array_key_exists('prcentcrs', $data)) {
                if (!is_null($data['prcentcrs'])) {
                    $crs->setPrcentcrs($data['prcentcrs']);
                } else {
                    throw new \Propel\Runtime\Exception\PropelException('prcentcrs cannot be null');
                }
            }
            $crs->setLnkentcrs(array_key_exists('lnkentcrs', $data) ? $data['lnkentcrs'] : null);
            if (array_key_exists('rmventcrs', $data)) {
                if (!is_null($data['rmventcrs'])) {
                    $crs->setRmventcrs($data['rmventcrs']);
                } else {
                    throw new \Propel\Runtime\Exception\PropelException('rmventcrs cannot be null');
                }
            }
            $crs->setCreatedAt(array_key_exists('created_at', $data) ? $data['created_at'] : null);
            $crs->setUpdatedAt(array_key_exists('updated_at', $data) ? $data['updated_at'] : null);
            $crs->save($connection);
        } catch (\Propel\Runtime\Exception\PropelException $e) {
            Illuminate\Support\Facades\Log::debug($e);
            return false;
        }
        return $crs;
    }

    public static function dspcrsenttbl(\Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $allcrs = \CrsenttblQuery::create();

        $allcrs = $allcrs
            ->where("rmventcrs != true")
            ->find();

        if (!$allcrs) return false;

        return $allcrs;
    }

    public static function dsplstcrsenttbl(\Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $allcrs = \CrsenttblQuery::create();

        $allcrs = $allcrs
            ->select(array('uuid',
                'nmbentcrs'))
            ->withColumn('nmbentcrs', 'label')
            ->withColumn('uuid', 'value')
            ->find();

        if (!$allcrs) return false;

        return $allcrs;
    }

    public static function fnocrsenttbl($idnentcrs, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $crs = \CrsenttblQuery::create()
            ->filterByIdnentcrs($idnentcrs)
            ->findOne($connection);

        if (!$crs) return false;

        return $crs;
    }

    public static function fnucrsenttbl($uuid, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $crs = \CrsenttblQuery::create()
            ->filterByUuid($uuid)
            ->findOne($connection);

        if (!$crs) return false;

        return $crs;
    }

    public static function fndusrenttbl($uuid, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $crs = \ChcenttblQuery::create()
            ->filterByUidentcrs($uuid)
            ->useUsersRelatedByUidentusrQuery()
            ->withColumn('name')
            ->withColumn('email')
            ->endUse()
            ->find($connection);

        if (!$crs) return false;

        return $crs;
    }

    public static function fndcrsenttbl(\Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $crs = \CrsenttblQuery::create()
            ->find($connection);

        if (!$crs) return false;

        return $crs;
    }

    public static function fndbyuenttbl($uuid, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $crs = \ChcenttblQuery::create()
            ->filterByUidentusr($uuid)
            ->useCrsenttblRelatedByUidentcrsQuery()
            ->withColumn('nmbentcrs')
            ->withColumn('dscentcrs')
            ->endUse()
            ->find($connection);

        if (!$crs) return false;

        return $crs;
    }

}
    //TODO *CRUD Generator control separator line* (Don't remove this line!)
