<?php

namespace Base;

use \Chcenttbl as ChildChcenttbl;
use \ChcenttblQuery as ChildChcenttblQuery;
use \Crsenttbl as ChildCrsenttbl;
use \CrsenttblQuery as ChildCrsenttblQuery;
use \Prbenttbl as ChildPrbenttbl;
use \PrbenttblQuery as ChildPrbenttblQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\ChcenttblTableMap;
use Map\CrsenttblTableMap;
use Map\PrbenttblTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'crsenttbl' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Crsenttbl implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\CrsenttblTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idnentcrs field.
     *
     * @var        int
     */
    protected $idnentcrs;

    /**
     * The value for the uuid field.
     *
     * @var        string
     */
    protected $uuid;

    /**
     * The value for the nmbentcrs field.
     *
     * @var        string
     */
    protected $nmbentcrs;

    /**
     * The value for the dscentcrs field.
     *
     * @var        string
     */
    protected $dscentcrs;

    /**
     * The value for the prcentcrs field.
     *
     * @var        int
     */
    protected $prcentcrs;

    /**
     * The value for the lnkentcrs field.
     *
     * @var        string
     */
    protected $lnkentcrs;

    /**
     * The value for the rmventcrs field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $rmventcrs;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ObjectCollection|ChildChcenttbl[] Collection to store aggregation of ChildChcenttbl objects.
     */
    protected $collChcenttblsRelatedByIdnentcrs;
    protected $collChcenttblsRelatedByIdnentcrsPartial;

    /**
     * @var        ObjectCollection|ChildChcenttbl[] Collection to store aggregation of ChildChcenttbl objects.
     */
    protected $collChcenttblsRelatedByUidentcrs;
    protected $collChcenttblsRelatedByUidentcrsPartial;

    /**
     * @var        ObjectCollection|ChildPrbenttbl[] Collection to store aggregation of ChildPrbenttbl objects.
     */
    protected $collPrbenttblsRelatedByIdnentcrs;
    protected $collPrbenttblsRelatedByIdnentcrsPartial;

    /**
     * @var        ObjectCollection|ChildPrbenttbl[] Collection to store aggregation of ChildPrbenttbl objects.
     */
    protected $collPrbenttblsRelatedByUidentcrs;
    protected $collPrbenttblsRelatedByUidentcrsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildChcenttbl[]
     */
    protected $chcenttblsRelatedByIdnentcrsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildChcenttbl[]
     */
    protected $chcenttblsRelatedByUidentcrsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrbenttbl[]
     */
    protected $prbenttblsRelatedByIdnentcrsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrbenttbl[]
     */
    protected $prbenttblsRelatedByUidentcrsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->rmventcrs = false;
    }

    /**
     * Initializes internal state of Base\Crsenttbl object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Crsenttbl</code> instance.  If
     * <code>obj</code> is an instance of <code>Crsenttbl</code>, delegates to
     * <code>equals(Crsenttbl)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Crsenttbl The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idnentcrs] column value.
     *
     * @return int
     */
    public function getIdnentcrs()
    {
        return $this->idnentcrs;
    }

    /**
     * Get the [uuid] column value.
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Get the [nmbentcrs] column value.
     *
     * @return string
     */
    public function getNmbentcrs()
    {
        return $this->nmbentcrs;
    }

    /**
     * Get the [dscentcrs] column value.
     *
     * @return string
     */
    public function getDscentcrs()
    {
        return $this->dscentcrs;
    }

    /**
     * Get the [prcentcrs] column value.
     *
     * @return int
     */
    public function getPrcentcrs()
    {
        return $this->prcentcrs;
    }

    /**
     * Get the [lnkentcrs] column value.
     *
     * @return string
     */
    public function getLnkentcrs()
    {
        return $this->lnkentcrs;
    }

    /**
     * Get the [rmventcrs] column value.
     *
     * @return boolean
     */
    public function getRmventcrs()
    {
        return $this->rmventcrs;
    }

    /**
     * Get the [rmventcrs] column value.
     *
     * @return boolean
     */
    public function isRmventcrs()
    {
        return $this->getRmventcrs();
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [idnentcrs] column.
     *
     * @param int $v new value
     * @return $this|\Crsenttbl The current object (for fluent API support)
     */
    public function setIdnentcrs($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idnentcrs !== $v) {
            $this->idnentcrs = $v;
            $this->modifiedColumns[CrsenttblTableMap::COL_IDNENTCRS] = true;
        }

        return $this;
    } // setIdnentcrs()

    /**
     * Set the value of [uuid] column.
     *
     * @param string $v new value
     * @return $this|\Crsenttbl The current object (for fluent API support)
     */
    public function setUuid($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uuid !== $v) {
            $this->uuid = $v;
            $this->modifiedColumns[CrsenttblTableMap::COL_UUID] = true;
        }

        return $this;
    } // setUuid()

    /**
     * Set the value of [nmbentcrs] column.
     *
     * @param string $v new value
     * @return $this|\Crsenttbl The current object (for fluent API support)
     */
    public function setNmbentcrs($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nmbentcrs !== $v) {
            $this->nmbentcrs = $v;
            $this->modifiedColumns[CrsenttblTableMap::COL_NMBENTCRS] = true;
        }

        return $this;
    } // setNmbentcrs()

    /**
     * Set the value of [dscentcrs] column.
     *
     * @param string $v new value
     * @return $this|\Crsenttbl The current object (for fluent API support)
     */
    public function setDscentcrs($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->dscentcrs !== $v) {
            $this->dscentcrs = $v;
            $this->modifiedColumns[CrsenttblTableMap::COL_DSCENTCRS] = true;
        }

        return $this;
    } // setDscentcrs()

    /**
     * Set the value of [prcentcrs] column.
     *
     * @param int $v new value
     * @return $this|\Crsenttbl The current object (for fluent API support)
     */
    public function setPrcentcrs($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prcentcrs !== $v) {
            $this->prcentcrs = $v;
            $this->modifiedColumns[CrsenttblTableMap::COL_PRCENTCRS] = true;
        }

        return $this;
    } // setPrcentcrs()

    /**
     * Set the value of [lnkentcrs] column.
     *
     * @param string $v new value
     * @return $this|\Crsenttbl The current object (for fluent API support)
     */
    public function setLnkentcrs($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->lnkentcrs !== $v) {
            $this->lnkentcrs = $v;
            $this->modifiedColumns[CrsenttblTableMap::COL_LNKENTCRS] = true;
        }

        return $this;
    } // setLnkentcrs()

    /**
     * Sets the value of the [rmventcrs] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Crsenttbl The current object (for fluent API support)
     */
    public function setRmventcrs($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->rmventcrs !== $v) {
            $this->rmventcrs = $v;
            $this->modifiedColumns[CrsenttblTableMap::COL_RMVENTCRS] = true;
        }

        return $this;
    } // setRmventcrs()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Crsenttbl The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CrsenttblTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Crsenttbl The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CrsenttblTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->rmventcrs !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CrsenttblTableMap::translateFieldName('Idnentcrs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idnentcrs = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CrsenttblTableMap::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uuid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CrsenttblTableMap::translateFieldName('Nmbentcrs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nmbentcrs = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CrsenttblTableMap::translateFieldName('Dscentcrs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dscentcrs = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CrsenttblTableMap::translateFieldName('Prcentcrs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prcentcrs = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CrsenttblTableMap::translateFieldName('Lnkentcrs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->lnkentcrs = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CrsenttblTableMap::translateFieldName('Rmventcrs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->rmventcrs = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CrsenttblTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CrsenttblTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 9; // 9 = CrsenttblTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Crsenttbl'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CrsenttblTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCrsenttblQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collChcenttblsRelatedByIdnentcrs = null;

            $this->collChcenttblsRelatedByUidentcrs = null;

            $this->collPrbenttblsRelatedByIdnentcrs = null;

            $this->collPrbenttblsRelatedByUidentcrs = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Crsenttbl::setDeleted()
     * @see Crsenttbl::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrsenttblTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCrsenttblQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrsenttblTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CrsenttblTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->chcenttblsRelatedByIdnentcrsScheduledForDeletion !== null) {
                if (!$this->chcenttblsRelatedByIdnentcrsScheduledForDeletion->isEmpty()) {
                    foreach ($this->chcenttblsRelatedByIdnentcrsScheduledForDeletion as $chcenttblRelatedByIdnentcrs) {
                        // need to save related object because we set the relation to null
                        $chcenttblRelatedByIdnentcrs->save($con);
                    }
                    $this->chcenttblsRelatedByIdnentcrsScheduledForDeletion = null;
                }
            }

            if ($this->collChcenttblsRelatedByIdnentcrs !== null) {
                foreach ($this->collChcenttblsRelatedByIdnentcrs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->chcenttblsRelatedByUidentcrsScheduledForDeletion !== null) {
                if (!$this->chcenttblsRelatedByUidentcrsScheduledForDeletion->isEmpty()) {
                    foreach ($this->chcenttblsRelatedByUidentcrsScheduledForDeletion as $chcenttblRelatedByUidentcrs) {
                        // need to save related object because we set the relation to null
                        $chcenttblRelatedByUidentcrs->save($con);
                    }
                    $this->chcenttblsRelatedByUidentcrsScheduledForDeletion = null;
                }
            }

            if ($this->collChcenttblsRelatedByUidentcrs !== null) {
                foreach ($this->collChcenttblsRelatedByUidentcrs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prbenttblsRelatedByIdnentcrsScheduledForDeletion !== null) {
                if (!$this->prbenttblsRelatedByIdnentcrsScheduledForDeletion->isEmpty()) {
                    foreach ($this->prbenttblsRelatedByIdnentcrsScheduledForDeletion as $prbenttblRelatedByIdnentcrs) {
                        // need to save related object because we set the relation to null
                        $prbenttblRelatedByIdnentcrs->save($con);
                    }
                    $this->prbenttblsRelatedByIdnentcrsScheduledForDeletion = null;
                }
            }

            if ($this->collPrbenttblsRelatedByIdnentcrs !== null) {
                foreach ($this->collPrbenttblsRelatedByIdnentcrs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prbenttblsRelatedByUidentcrsScheduledForDeletion !== null) {
                if (!$this->prbenttblsRelatedByUidentcrsScheduledForDeletion->isEmpty()) {
                    foreach ($this->prbenttblsRelatedByUidentcrsScheduledForDeletion as $prbenttblRelatedByUidentcrs) {
                        // need to save related object because we set the relation to null
                        $prbenttblRelatedByUidentcrs->save($con);
                    }
                    $this->prbenttblsRelatedByUidentcrsScheduledForDeletion = null;
                }
            }

            if ($this->collPrbenttblsRelatedByUidentcrs !== null) {
                foreach ($this->collPrbenttblsRelatedByUidentcrs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CrsenttblTableMap::COL_IDNENTCRS] = true;
        if (null !== $this->idnentcrs) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CrsenttblTableMap::COL_IDNENTCRS . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CrsenttblTableMap::COL_IDNENTCRS)) {
            $modifiedColumns[':p' . $index++]  = 'idnentcrs';
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_UUID)) {
            $modifiedColumns[':p' . $index++]  = 'uuid';
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_NMBENTCRS)) {
            $modifiedColumns[':p' . $index++]  = 'nmbentcrs';
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_DSCENTCRS)) {
            $modifiedColumns[':p' . $index++]  = 'dscentcrs';
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_PRCENTCRS)) {
            $modifiedColumns[':p' . $index++]  = 'prcentcrs';
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_LNKENTCRS)) {
            $modifiedColumns[':p' . $index++]  = 'lnkentcrs';
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_RMVENTCRS)) {
            $modifiedColumns[':p' . $index++]  = 'rmventcrs';
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO crsenttbl (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idnentcrs':
                        $stmt->bindValue($identifier, $this->idnentcrs, PDO::PARAM_INT);
                        break;
                    case 'uuid':
                        $stmt->bindValue($identifier, $this->uuid, PDO::PARAM_STR);
                        break;
                    case 'nmbentcrs':
                        $stmt->bindValue($identifier, $this->nmbentcrs, PDO::PARAM_STR);
                        break;
                    case 'dscentcrs':
                        $stmt->bindValue($identifier, $this->dscentcrs, PDO::PARAM_STR);
                        break;
                    case 'prcentcrs':
                        $stmt->bindValue($identifier, $this->prcentcrs, PDO::PARAM_INT);
                        break;
                    case 'lnkentcrs':
                        $stmt->bindValue($identifier, $this->lnkentcrs, PDO::PARAM_STR);
                        break;
                    case 'rmventcrs':
                        $stmt->bindValue($identifier, (int) $this->rmventcrs, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdnentcrs($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CrsenttblTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdnentcrs();
                break;
            case 1:
                return $this->getUuid();
                break;
            case 2:
                return $this->getNmbentcrs();
                break;
            case 3:
                return $this->getDscentcrs();
                break;
            case 4:
                return $this->getPrcentcrs();
                break;
            case 5:
                return $this->getLnkentcrs();
                break;
            case 6:
                return $this->getRmventcrs();
                break;
            case 7:
                return $this->getCreatedAt();
                break;
            case 8:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Crsenttbl'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Crsenttbl'][$this->hashCode()] = true;
        $keys = CrsenttblTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdnentcrs(),
            $keys[1] => $this->getUuid(),
            $keys[2] => $this->getNmbentcrs(),
            $keys[3] => $this->getDscentcrs(),
            $keys[4] => $this->getPrcentcrs(),
            $keys[5] => $this->getLnkentcrs(),
            $keys[6] => $this->getRmventcrs(),
            $keys[7] => $this->getCreatedAt(),
            $keys[8] => $this->getUpdatedAt(),
        );
        if ($result[$keys[7]] instanceof \DateTimeInterface) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collChcenttblsRelatedByIdnentcrs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'chcenttbls';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'chcenttbls';
                        break;
                    default:
                        $key = 'Chcenttbls';
                }

                $result[$key] = $this->collChcenttblsRelatedByIdnentcrs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collChcenttblsRelatedByUidentcrs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'chcenttbls';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'chcenttbls';
                        break;
                    default:
                        $key = 'Chcenttbls';
                }

                $result[$key] = $this->collChcenttblsRelatedByUidentcrs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrbenttblsRelatedByIdnentcrs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prbenttbls';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'prbenttbls';
                        break;
                    default:
                        $key = 'Prbenttbls';
                }

                $result[$key] = $this->collPrbenttblsRelatedByIdnentcrs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrbenttblsRelatedByUidentcrs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prbenttbls';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'prbenttbls';
                        break;
                    default:
                        $key = 'Prbenttbls';
                }

                $result[$key] = $this->collPrbenttblsRelatedByUidentcrs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Crsenttbl
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CrsenttblTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Crsenttbl
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdnentcrs($value);
                break;
            case 1:
                $this->setUuid($value);
                break;
            case 2:
                $this->setNmbentcrs($value);
                break;
            case 3:
                $this->setDscentcrs($value);
                break;
            case 4:
                $this->setPrcentcrs($value);
                break;
            case 5:
                $this->setLnkentcrs($value);
                break;
            case 6:
                $this->setRmventcrs($value);
                break;
            case 7:
                $this->setCreatedAt($value);
                break;
            case 8:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CrsenttblTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdnentcrs($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUuid($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNmbentcrs($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDscentcrs($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setPrcentcrs($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setLnkentcrs($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setRmventcrs($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setCreatedAt($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setUpdatedAt($arr[$keys[8]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Crsenttbl The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CrsenttblTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CrsenttblTableMap::COL_IDNENTCRS)) {
            $criteria->add(CrsenttblTableMap::COL_IDNENTCRS, $this->idnentcrs);
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_UUID)) {
            $criteria->add(CrsenttblTableMap::COL_UUID, $this->uuid);
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_NMBENTCRS)) {
            $criteria->add(CrsenttblTableMap::COL_NMBENTCRS, $this->nmbentcrs);
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_DSCENTCRS)) {
            $criteria->add(CrsenttblTableMap::COL_DSCENTCRS, $this->dscentcrs);
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_PRCENTCRS)) {
            $criteria->add(CrsenttblTableMap::COL_PRCENTCRS, $this->prcentcrs);
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_LNKENTCRS)) {
            $criteria->add(CrsenttblTableMap::COL_LNKENTCRS, $this->lnkentcrs);
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_RMVENTCRS)) {
            $criteria->add(CrsenttblTableMap::COL_RMVENTCRS, $this->rmventcrs);
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_CREATED_AT)) {
            $criteria->add(CrsenttblTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(CrsenttblTableMap::COL_UPDATED_AT)) {
            $criteria->add(CrsenttblTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCrsenttblQuery::create();
        $criteria->add(CrsenttblTableMap::COL_IDNENTCRS, $this->idnentcrs);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdnentcrs();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdnentcrs();
    }

    /**
     * Generic method to set the primary key (idnentcrs column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdnentcrs($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdnentcrs();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Crsenttbl (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUuid($this->getUuid());
        $copyObj->setNmbentcrs($this->getNmbentcrs());
        $copyObj->setDscentcrs($this->getDscentcrs());
        $copyObj->setPrcentcrs($this->getPrcentcrs());
        $copyObj->setLnkentcrs($this->getLnkentcrs());
        $copyObj->setRmventcrs($this->getRmventcrs());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getChcenttblsRelatedByIdnentcrs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addChcenttblRelatedByIdnentcrs($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getChcenttblsRelatedByUidentcrs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addChcenttblRelatedByUidentcrs($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrbenttblsRelatedByIdnentcrs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrbenttblRelatedByIdnentcrs($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrbenttblsRelatedByUidentcrs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrbenttblRelatedByUidentcrs($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdnentcrs(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Crsenttbl Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ChcenttblRelatedByIdnentcrs' == $relationName) {
            $this->initChcenttblsRelatedByIdnentcrs();
            return;
        }
        if ('ChcenttblRelatedByUidentcrs' == $relationName) {
            $this->initChcenttblsRelatedByUidentcrs();
            return;
        }
        if ('PrbenttblRelatedByIdnentcrs' == $relationName) {
            $this->initPrbenttblsRelatedByIdnentcrs();
            return;
        }
        if ('PrbenttblRelatedByUidentcrs' == $relationName) {
            $this->initPrbenttblsRelatedByUidentcrs();
            return;
        }
    }

    /**
     * Clears out the collChcenttblsRelatedByIdnentcrs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addChcenttblsRelatedByIdnentcrs()
     */
    public function clearChcenttblsRelatedByIdnentcrs()
    {
        $this->collChcenttblsRelatedByIdnentcrs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collChcenttblsRelatedByIdnentcrs collection loaded partially.
     */
    public function resetPartialChcenttblsRelatedByIdnentcrs($v = true)
    {
        $this->collChcenttblsRelatedByIdnentcrsPartial = $v;
    }

    /**
     * Initializes the collChcenttblsRelatedByIdnentcrs collection.
     *
     * By default this just sets the collChcenttblsRelatedByIdnentcrs collection to an empty array (like clearcollChcenttblsRelatedByIdnentcrs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initChcenttblsRelatedByIdnentcrs($overrideExisting = true)
    {
        if (null !== $this->collChcenttblsRelatedByIdnentcrs && !$overrideExisting) {
            return;
        }

        $collectionClassName = ChcenttblTableMap::getTableMap()->getCollectionClassName();

        $this->collChcenttblsRelatedByIdnentcrs = new $collectionClassName;
        $this->collChcenttblsRelatedByIdnentcrs->setModel('\Chcenttbl');
    }

    /**
     * Gets an array of ChildChcenttbl objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCrsenttbl is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildChcenttbl[] List of ChildChcenttbl objects
     * @throws PropelException
     */
    public function getChcenttblsRelatedByIdnentcrs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collChcenttblsRelatedByIdnentcrsPartial && !$this->isNew();
        if (null === $this->collChcenttblsRelatedByIdnentcrs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collChcenttblsRelatedByIdnentcrs) {
                // return empty collection
                $this->initChcenttblsRelatedByIdnentcrs();
            } else {
                $collChcenttblsRelatedByIdnentcrs = ChildChcenttblQuery::create(null, $criteria)
                    ->filterByCrsenttblRelatedByIdnentcrs($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collChcenttblsRelatedByIdnentcrsPartial && count($collChcenttblsRelatedByIdnentcrs)) {
                        $this->initChcenttblsRelatedByIdnentcrs(false);

                        foreach ($collChcenttblsRelatedByIdnentcrs as $obj) {
                            if (false == $this->collChcenttblsRelatedByIdnentcrs->contains($obj)) {
                                $this->collChcenttblsRelatedByIdnentcrs->append($obj);
                            }
                        }

                        $this->collChcenttblsRelatedByIdnentcrsPartial = true;
                    }

                    return $collChcenttblsRelatedByIdnentcrs;
                }

                if ($partial && $this->collChcenttblsRelatedByIdnentcrs) {
                    foreach ($this->collChcenttblsRelatedByIdnentcrs as $obj) {
                        if ($obj->isNew()) {
                            $collChcenttblsRelatedByIdnentcrs[] = $obj;
                        }
                    }
                }

                $this->collChcenttblsRelatedByIdnentcrs = $collChcenttblsRelatedByIdnentcrs;
                $this->collChcenttblsRelatedByIdnentcrsPartial = false;
            }
        }

        return $this->collChcenttblsRelatedByIdnentcrs;
    }

    /**
     * Sets a collection of ChildChcenttbl objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $chcenttblsRelatedByIdnentcrs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCrsenttbl The current object (for fluent API support)
     */
    public function setChcenttblsRelatedByIdnentcrs(Collection $chcenttblsRelatedByIdnentcrs, ConnectionInterface $con = null)
    {
        /** @var ChildChcenttbl[] $chcenttblsRelatedByIdnentcrsToDelete */
        $chcenttblsRelatedByIdnentcrsToDelete = $this->getChcenttblsRelatedByIdnentcrs(new Criteria(), $con)->diff($chcenttblsRelatedByIdnentcrs);


        $this->chcenttblsRelatedByIdnentcrsScheduledForDeletion = $chcenttblsRelatedByIdnentcrsToDelete;

        foreach ($chcenttblsRelatedByIdnentcrsToDelete as $chcenttblRelatedByIdnentcrsRemoved) {
            $chcenttblRelatedByIdnentcrsRemoved->setCrsenttblRelatedByIdnentcrs(null);
        }

        $this->collChcenttblsRelatedByIdnentcrs = null;
        foreach ($chcenttblsRelatedByIdnentcrs as $chcenttblRelatedByIdnentcrs) {
            $this->addChcenttblRelatedByIdnentcrs($chcenttblRelatedByIdnentcrs);
        }

        $this->collChcenttblsRelatedByIdnentcrs = $chcenttblsRelatedByIdnentcrs;
        $this->collChcenttblsRelatedByIdnentcrsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Chcenttbl objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Chcenttbl objects.
     * @throws PropelException
     */
    public function countChcenttblsRelatedByIdnentcrs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collChcenttblsRelatedByIdnentcrsPartial && !$this->isNew();
        if (null === $this->collChcenttblsRelatedByIdnentcrs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collChcenttblsRelatedByIdnentcrs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getChcenttblsRelatedByIdnentcrs());
            }

            $query = ChildChcenttblQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCrsenttblRelatedByIdnentcrs($this)
                ->count($con);
        }

        return count($this->collChcenttblsRelatedByIdnentcrs);
    }

    /**
     * Method called to associate a ChildChcenttbl object to this object
     * through the ChildChcenttbl foreign key attribute.
     *
     * @param  ChildChcenttbl $l ChildChcenttbl
     * @return $this|\Crsenttbl The current object (for fluent API support)
     */
    public function addChcenttblRelatedByIdnentcrs(ChildChcenttbl $l)
    {
        if ($this->collChcenttblsRelatedByIdnentcrs === null) {
            $this->initChcenttblsRelatedByIdnentcrs();
            $this->collChcenttblsRelatedByIdnentcrsPartial = true;
        }

        if (!$this->collChcenttblsRelatedByIdnentcrs->contains($l)) {
            $this->doAddChcenttblRelatedByIdnentcrs($l);

            if ($this->chcenttblsRelatedByIdnentcrsScheduledForDeletion and $this->chcenttblsRelatedByIdnentcrsScheduledForDeletion->contains($l)) {
                $this->chcenttblsRelatedByIdnentcrsScheduledForDeletion->remove($this->chcenttblsRelatedByIdnentcrsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildChcenttbl $chcenttblRelatedByIdnentcrs The ChildChcenttbl object to add.
     */
    protected function doAddChcenttblRelatedByIdnentcrs(ChildChcenttbl $chcenttblRelatedByIdnentcrs)
    {
        $this->collChcenttblsRelatedByIdnentcrs[]= $chcenttblRelatedByIdnentcrs;
        $chcenttblRelatedByIdnentcrs->setCrsenttblRelatedByIdnentcrs($this);
    }

    /**
     * @param  ChildChcenttbl $chcenttblRelatedByIdnentcrs The ChildChcenttbl object to remove.
     * @return $this|ChildCrsenttbl The current object (for fluent API support)
     */
    public function removeChcenttblRelatedByIdnentcrs(ChildChcenttbl $chcenttblRelatedByIdnentcrs)
    {
        if ($this->getChcenttblsRelatedByIdnentcrs()->contains($chcenttblRelatedByIdnentcrs)) {
            $pos = $this->collChcenttblsRelatedByIdnentcrs->search($chcenttblRelatedByIdnentcrs);
            $this->collChcenttblsRelatedByIdnentcrs->remove($pos);
            if (null === $this->chcenttblsRelatedByIdnentcrsScheduledForDeletion) {
                $this->chcenttblsRelatedByIdnentcrsScheduledForDeletion = clone $this->collChcenttblsRelatedByIdnentcrs;
                $this->chcenttblsRelatedByIdnentcrsScheduledForDeletion->clear();
            }
            $this->chcenttblsRelatedByIdnentcrsScheduledForDeletion[]= $chcenttblRelatedByIdnentcrs;
            $chcenttblRelatedByIdnentcrs->setCrsenttblRelatedByIdnentcrs(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Crsenttbl is new, it will return
     * an empty collection; or if this Crsenttbl has previously
     * been saved, it will retrieve related ChcenttblsRelatedByIdnentcrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Crsenttbl.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChcenttbl[] List of ChildChcenttbl objects
     */
    public function getChcenttblsRelatedByIdnentcrsJoinUsersRelatedByIdnentusr(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChcenttblQuery::create(null, $criteria);
        $query->joinWith('UsersRelatedByIdnentusr', $joinBehavior);

        return $this->getChcenttblsRelatedByIdnentcrs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Crsenttbl is new, it will return
     * an empty collection; or if this Crsenttbl has previously
     * been saved, it will retrieve related ChcenttblsRelatedByIdnentcrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Crsenttbl.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChcenttbl[] List of ChildChcenttbl objects
     */
    public function getChcenttblsRelatedByIdnentcrsJoinUsersRelatedByUidentusr(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChcenttblQuery::create(null, $criteria);
        $query->joinWith('UsersRelatedByUidentusr', $joinBehavior);

        return $this->getChcenttblsRelatedByIdnentcrs($query, $con);
    }

    /**
     * Clears out the collChcenttblsRelatedByUidentcrs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addChcenttblsRelatedByUidentcrs()
     */
    public function clearChcenttblsRelatedByUidentcrs()
    {
        $this->collChcenttblsRelatedByUidentcrs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collChcenttblsRelatedByUidentcrs collection loaded partially.
     */
    public function resetPartialChcenttblsRelatedByUidentcrs($v = true)
    {
        $this->collChcenttblsRelatedByUidentcrsPartial = $v;
    }

    /**
     * Initializes the collChcenttblsRelatedByUidentcrs collection.
     *
     * By default this just sets the collChcenttblsRelatedByUidentcrs collection to an empty array (like clearcollChcenttblsRelatedByUidentcrs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initChcenttblsRelatedByUidentcrs($overrideExisting = true)
    {
        if (null !== $this->collChcenttblsRelatedByUidentcrs && !$overrideExisting) {
            return;
        }

        $collectionClassName = ChcenttblTableMap::getTableMap()->getCollectionClassName();

        $this->collChcenttblsRelatedByUidentcrs = new $collectionClassName;
        $this->collChcenttblsRelatedByUidentcrs->setModel('\Chcenttbl');
    }

    /**
     * Gets an array of ChildChcenttbl objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCrsenttbl is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildChcenttbl[] List of ChildChcenttbl objects
     * @throws PropelException
     */
    public function getChcenttblsRelatedByUidentcrs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collChcenttblsRelatedByUidentcrsPartial && !$this->isNew();
        if (null === $this->collChcenttblsRelatedByUidentcrs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collChcenttblsRelatedByUidentcrs) {
                // return empty collection
                $this->initChcenttblsRelatedByUidentcrs();
            } else {
                $collChcenttblsRelatedByUidentcrs = ChildChcenttblQuery::create(null, $criteria)
                    ->filterByCrsenttblRelatedByUidentcrs($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collChcenttblsRelatedByUidentcrsPartial && count($collChcenttblsRelatedByUidentcrs)) {
                        $this->initChcenttblsRelatedByUidentcrs(false);

                        foreach ($collChcenttblsRelatedByUidentcrs as $obj) {
                            if (false == $this->collChcenttblsRelatedByUidentcrs->contains($obj)) {
                                $this->collChcenttblsRelatedByUidentcrs->append($obj);
                            }
                        }

                        $this->collChcenttblsRelatedByUidentcrsPartial = true;
                    }

                    return $collChcenttblsRelatedByUidentcrs;
                }

                if ($partial && $this->collChcenttblsRelatedByUidentcrs) {
                    foreach ($this->collChcenttblsRelatedByUidentcrs as $obj) {
                        if ($obj->isNew()) {
                            $collChcenttblsRelatedByUidentcrs[] = $obj;
                        }
                    }
                }

                $this->collChcenttblsRelatedByUidentcrs = $collChcenttblsRelatedByUidentcrs;
                $this->collChcenttblsRelatedByUidentcrsPartial = false;
            }
        }

        return $this->collChcenttblsRelatedByUidentcrs;
    }

    /**
     * Sets a collection of ChildChcenttbl objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $chcenttblsRelatedByUidentcrs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCrsenttbl The current object (for fluent API support)
     */
    public function setChcenttblsRelatedByUidentcrs(Collection $chcenttblsRelatedByUidentcrs, ConnectionInterface $con = null)
    {
        /** @var ChildChcenttbl[] $chcenttblsRelatedByUidentcrsToDelete */
        $chcenttblsRelatedByUidentcrsToDelete = $this->getChcenttblsRelatedByUidentcrs(new Criteria(), $con)->diff($chcenttblsRelatedByUidentcrs);


        $this->chcenttblsRelatedByUidentcrsScheduledForDeletion = $chcenttblsRelatedByUidentcrsToDelete;

        foreach ($chcenttblsRelatedByUidentcrsToDelete as $chcenttblRelatedByUidentcrsRemoved) {
            $chcenttblRelatedByUidentcrsRemoved->setCrsenttblRelatedByUidentcrs(null);
        }

        $this->collChcenttblsRelatedByUidentcrs = null;
        foreach ($chcenttblsRelatedByUidentcrs as $chcenttblRelatedByUidentcrs) {
            $this->addChcenttblRelatedByUidentcrs($chcenttblRelatedByUidentcrs);
        }

        $this->collChcenttblsRelatedByUidentcrs = $chcenttblsRelatedByUidentcrs;
        $this->collChcenttblsRelatedByUidentcrsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Chcenttbl objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Chcenttbl objects.
     * @throws PropelException
     */
    public function countChcenttblsRelatedByUidentcrs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collChcenttblsRelatedByUidentcrsPartial && !$this->isNew();
        if (null === $this->collChcenttblsRelatedByUidentcrs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collChcenttblsRelatedByUidentcrs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getChcenttblsRelatedByUidentcrs());
            }

            $query = ChildChcenttblQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCrsenttblRelatedByUidentcrs($this)
                ->count($con);
        }

        return count($this->collChcenttblsRelatedByUidentcrs);
    }

    /**
     * Method called to associate a ChildChcenttbl object to this object
     * through the ChildChcenttbl foreign key attribute.
     *
     * @param  ChildChcenttbl $l ChildChcenttbl
     * @return $this|\Crsenttbl The current object (for fluent API support)
     */
    public function addChcenttblRelatedByUidentcrs(ChildChcenttbl $l)
    {
        if ($this->collChcenttblsRelatedByUidentcrs === null) {
            $this->initChcenttblsRelatedByUidentcrs();
            $this->collChcenttblsRelatedByUidentcrsPartial = true;
        }

        if (!$this->collChcenttblsRelatedByUidentcrs->contains($l)) {
            $this->doAddChcenttblRelatedByUidentcrs($l);

            if ($this->chcenttblsRelatedByUidentcrsScheduledForDeletion and $this->chcenttblsRelatedByUidentcrsScheduledForDeletion->contains($l)) {
                $this->chcenttblsRelatedByUidentcrsScheduledForDeletion->remove($this->chcenttblsRelatedByUidentcrsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildChcenttbl $chcenttblRelatedByUidentcrs The ChildChcenttbl object to add.
     */
    protected function doAddChcenttblRelatedByUidentcrs(ChildChcenttbl $chcenttblRelatedByUidentcrs)
    {
        $this->collChcenttblsRelatedByUidentcrs[]= $chcenttblRelatedByUidentcrs;
        $chcenttblRelatedByUidentcrs->setCrsenttblRelatedByUidentcrs($this);
    }

    /**
     * @param  ChildChcenttbl $chcenttblRelatedByUidentcrs The ChildChcenttbl object to remove.
     * @return $this|ChildCrsenttbl The current object (for fluent API support)
     */
    public function removeChcenttblRelatedByUidentcrs(ChildChcenttbl $chcenttblRelatedByUidentcrs)
    {
        if ($this->getChcenttblsRelatedByUidentcrs()->contains($chcenttblRelatedByUidentcrs)) {
            $pos = $this->collChcenttblsRelatedByUidentcrs->search($chcenttblRelatedByUidentcrs);
            $this->collChcenttblsRelatedByUidentcrs->remove($pos);
            if (null === $this->chcenttblsRelatedByUidentcrsScheduledForDeletion) {
                $this->chcenttblsRelatedByUidentcrsScheduledForDeletion = clone $this->collChcenttblsRelatedByUidentcrs;
                $this->chcenttblsRelatedByUidentcrsScheduledForDeletion->clear();
            }
            $this->chcenttblsRelatedByUidentcrsScheduledForDeletion[]= $chcenttblRelatedByUidentcrs;
            $chcenttblRelatedByUidentcrs->setCrsenttblRelatedByUidentcrs(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Crsenttbl is new, it will return
     * an empty collection; or if this Crsenttbl has previously
     * been saved, it will retrieve related ChcenttblsRelatedByUidentcrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Crsenttbl.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChcenttbl[] List of ChildChcenttbl objects
     */
    public function getChcenttblsRelatedByUidentcrsJoinUsersRelatedByIdnentusr(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChcenttblQuery::create(null, $criteria);
        $query->joinWith('UsersRelatedByIdnentusr', $joinBehavior);

        return $this->getChcenttblsRelatedByUidentcrs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Crsenttbl is new, it will return
     * an empty collection; or if this Crsenttbl has previously
     * been saved, it will retrieve related ChcenttblsRelatedByUidentcrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Crsenttbl.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildChcenttbl[] List of ChildChcenttbl objects
     */
    public function getChcenttblsRelatedByUidentcrsJoinUsersRelatedByUidentusr(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildChcenttblQuery::create(null, $criteria);
        $query->joinWith('UsersRelatedByUidentusr', $joinBehavior);

        return $this->getChcenttblsRelatedByUidentcrs($query, $con);
    }

    /**
     * Clears out the collPrbenttblsRelatedByIdnentcrs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPrbenttblsRelatedByIdnentcrs()
     */
    public function clearPrbenttblsRelatedByIdnentcrs()
    {
        $this->collPrbenttblsRelatedByIdnentcrs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPrbenttblsRelatedByIdnentcrs collection loaded partially.
     */
    public function resetPartialPrbenttblsRelatedByIdnentcrs($v = true)
    {
        $this->collPrbenttblsRelatedByIdnentcrsPartial = $v;
    }

    /**
     * Initializes the collPrbenttblsRelatedByIdnentcrs collection.
     *
     * By default this just sets the collPrbenttblsRelatedByIdnentcrs collection to an empty array (like clearcollPrbenttblsRelatedByIdnentcrs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrbenttblsRelatedByIdnentcrs($overrideExisting = true)
    {
        if (null !== $this->collPrbenttblsRelatedByIdnentcrs && !$overrideExisting) {
            return;
        }

        $collectionClassName = PrbenttblTableMap::getTableMap()->getCollectionClassName();

        $this->collPrbenttblsRelatedByIdnentcrs = new $collectionClassName;
        $this->collPrbenttblsRelatedByIdnentcrs->setModel('\Prbenttbl');
    }

    /**
     * Gets an array of ChildPrbenttbl objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCrsenttbl is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrbenttbl[] List of ChildPrbenttbl objects
     * @throws PropelException
     */
    public function getPrbenttblsRelatedByIdnentcrs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPrbenttblsRelatedByIdnentcrsPartial && !$this->isNew();
        if (null === $this->collPrbenttblsRelatedByIdnentcrs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrbenttblsRelatedByIdnentcrs) {
                // return empty collection
                $this->initPrbenttblsRelatedByIdnentcrs();
            } else {
                $collPrbenttblsRelatedByIdnentcrs = ChildPrbenttblQuery::create(null, $criteria)
                    ->filterByCrsenttblRelatedByIdnentcrs($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPrbenttblsRelatedByIdnentcrsPartial && count($collPrbenttblsRelatedByIdnentcrs)) {
                        $this->initPrbenttblsRelatedByIdnentcrs(false);

                        foreach ($collPrbenttblsRelatedByIdnentcrs as $obj) {
                            if (false == $this->collPrbenttblsRelatedByIdnentcrs->contains($obj)) {
                                $this->collPrbenttblsRelatedByIdnentcrs->append($obj);
                            }
                        }

                        $this->collPrbenttblsRelatedByIdnentcrsPartial = true;
                    }

                    return $collPrbenttblsRelatedByIdnentcrs;
                }

                if ($partial && $this->collPrbenttblsRelatedByIdnentcrs) {
                    foreach ($this->collPrbenttblsRelatedByIdnentcrs as $obj) {
                        if ($obj->isNew()) {
                            $collPrbenttblsRelatedByIdnentcrs[] = $obj;
                        }
                    }
                }

                $this->collPrbenttblsRelatedByIdnentcrs = $collPrbenttblsRelatedByIdnentcrs;
                $this->collPrbenttblsRelatedByIdnentcrsPartial = false;
            }
        }

        return $this->collPrbenttblsRelatedByIdnentcrs;
    }

    /**
     * Sets a collection of ChildPrbenttbl objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $prbenttblsRelatedByIdnentcrs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCrsenttbl The current object (for fluent API support)
     */
    public function setPrbenttblsRelatedByIdnentcrs(Collection $prbenttblsRelatedByIdnentcrs, ConnectionInterface $con = null)
    {
        /** @var ChildPrbenttbl[] $prbenttblsRelatedByIdnentcrsToDelete */
        $prbenttblsRelatedByIdnentcrsToDelete = $this->getPrbenttblsRelatedByIdnentcrs(new Criteria(), $con)->diff($prbenttblsRelatedByIdnentcrs);


        $this->prbenttblsRelatedByIdnentcrsScheduledForDeletion = $prbenttblsRelatedByIdnentcrsToDelete;

        foreach ($prbenttblsRelatedByIdnentcrsToDelete as $prbenttblRelatedByIdnentcrsRemoved) {
            $prbenttblRelatedByIdnentcrsRemoved->setCrsenttblRelatedByIdnentcrs(null);
        }

        $this->collPrbenttblsRelatedByIdnentcrs = null;
        foreach ($prbenttblsRelatedByIdnentcrs as $prbenttblRelatedByIdnentcrs) {
            $this->addPrbenttblRelatedByIdnentcrs($prbenttblRelatedByIdnentcrs);
        }

        $this->collPrbenttblsRelatedByIdnentcrs = $prbenttblsRelatedByIdnentcrs;
        $this->collPrbenttblsRelatedByIdnentcrsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prbenttbl objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Prbenttbl objects.
     * @throws PropelException
     */
    public function countPrbenttblsRelatedByIdnentcrs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPrbenttblsRelatedByIdnentcrsPartial && !$this->isNew();
        if (null === $this->collPrbenttblsRelatedByIdnentcrs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrbenttblsRelatedByIdnentcrs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPrbenttblsRelatedByIdnentcrs());
            }

            $query = ChildPrbenttblQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCrsenttblRelatedByIdnentcrs($this)
                ->count($con);
        }

        return count($this->collPrbenttblsRelatedByIdnentcrs);
    }

    /**
     * Method called to associate a ChildPrbenttbl object to this object
     * through the ChildPrbenttbl foreign key attribute.
     *
     * @param  ChildPrbenttbl $l ChildPrbenttbl
     * @return $this|\Crsenttbl The current object (for fluent API support)
     */
    public function addPrbenttblRelatedByIdnentcrs(ChildPrbenttbl $l)
    {
        if ($this->collPrbenttblsRelatedByIdnentcrs === null) {
            $this->initPrbenttblsRelatedByIdnentcrs();
            $this->collPrbenttblsRelatedByIdnentcrsPartial = true;
        }

        if (!$this->collPrbenttblsRelatedByIdnentcrs->contains($l)) {
            $this->doAddPrbenttblRelatedByIdnentcrs($l);

            if ($this->prbenttblsRelatedByIdnentcrsScheduledForDeletion and $this->prbenttblsRelatedByIdnentcrsScheduledForDeletion->contains($l)) {
                $this->prbenttblsRelatedByIdnentcrsScheduledForDeletion->remove($this->prbenttblsRelatedByIdnentcrsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrbenttbl $prbenttblRelatedByIdnentcrs The ChildPrbenttbl object to add.
     */
    protected function doAddPrbenttblRelatedByIdnentcrs(ChildPrbenttbl $prbenttblRelatedByIdnentcrs)
    {
        $this->collPrbenttblsRelatedByIdnentcrs[]= $prbenttblRelatedByIdnentcrs;
        $prbenttblRelatedByIdnentcrs->setCrsenttblRelatedByIdnentcrs($this);
    }

    /**
     * @param  ChildPrbenttbl $prbenttblRelatedByIdnentcrs The ChildPrbenttbl object to remove.
     * @return $this|ChildCrsenttbl The current object (for fluent API support)
     */
    public function removePrbenttblRelatedByIdnentcrs(ChildPrbenttbl $prbenttblRelatedByIdnentcrs)
    {
        if ($this->getPrbenttblsRelatedByIdnentcrs()->contains($prbenttblRelatedByIdnentcrs)) {
            $pos = $this->collPrbenttblsRelatedByIdnentcrs->search($prbenttblRelatedByIdnentcrs);
            $this->collPrbenttblsRelatedByIdnentcrs->remove($pos);
            if (null === $this->prbenttblsRelatedByIdnentcrsScheduledForDeletion) {
                $this->prbenttblsRelatedByIdnentcrsScheduledForDeletion = clone $this->collPrbenttblsRelatedByIdnentcrs;
                $this->prbenttblsRelatedByIdnentcrsScheduledForDeletion->clear();
            }
            $this->prbenttblsRelatedByIdnentcrsScheduledForDeletion[]= $prbenttblRelatedByIdnentcrs;
            $prbenttblRelatedByIdnentcrs->setCrsenttblRelatedByIdnentcrs(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Crsenttbl is new, it will return
     * an empty collection; or if this Crsenttbl has previously
     * been saved, it will retrieve related PrbenttblsRelatedByIdnentcrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Crsenttbl.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrbenttbl[] List of ChildPrbenttbl objects
     */
    public function getPrbenttblsRelatedByIdnentcrsJoinUsersRelatedByIdnentusr(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrbenttblQuery::create(null, $criteria);
        $query->joinWith('UsersRelatedByIdnentusr', $joinBehavior);

        return $this->getPrbenttblsRelatedByIdnentcrs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Crsenttbl is new, it will return
     * an empty collection; or if this Crsenttbl has previously
     * been saved, it will retrieve related PrbenttblsRelatedByIdnentcrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Crsenttbl.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrbenttbl[] List of ChildPrbenttbl objects
     */
    public function getPrbenttblsRelatedByIdnentcrsJoinUsersRelatedByUidentusr(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrbenttblQuery::create(null, $criteria);
        $query->joinWith('UsersRelatedByUidentusr', $joinBehavior);

        return $this->getPrbenttblsRelatedByIdnentcrs($query, $con);
    }

    /**
     * Clears out the collPrbenttblsRelatedByUidentcrs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPrbenttblsRelatedByUidentcrs()
     */
    public function clearPrbenttblsRelatedByUidentcrs()
    {
        $this->collPrbenttblsRelatedByUidentcrs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPrbenttblsRelatedByUidentcrs collection loaded partially.
     */
    public function resetPartialPrbenttblsRelatedByUidentcrs($v = true)
    {
        $this->collPrbenttblsRelatedByUidentcrsPartial = $v;
    }

    /**
     * Initializes the collPrbenttblsRelatedByUidentcrs collection.
     *
     * By default this just sets the collPrbenttblsRelatedByUidentcrs collection to an empty array (like clearcollPrbenttblsRelatedByUidentcrs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrbenttblsRelatedByUidentcrs($overrideExisting = true)
    {
        if (null !== $this->collPrbenttblsRelatedByUidentcrs && !$overrideExisting) {
            return;
        }

        $collectionClassName = PrbenttblTableMap::getTableMap()->getCollectionClassName();

        $this->collPrbenttblsRelatedByUidentcrs = new $collectionClassName;
        $this->collPrbenttblsRelatedByUidentcrs->setModel('\Prbenttbl');
    }

    /**
     * Gets an array of ChildPrbenttbl objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCrsenttbl is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrbenttbl[] List of ChildPrbenttbl objects
     * @throws PropelException
     */
    public function getPrbenttblsRelatedByUidentcrs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPrbenttblsRelatedByUidentcrsPartial && !$this->isNew();
        if (null === $this->collPrbenttblsRelatedByUidentcrs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrbenttblsRelatedByUidentcrs) {
                // return empty collection
                $this->initPrbenttblsRelatedByUidentcrs();
            } else {
                $collPrbenttblsRelatedByUidentcrs = ChildPrbenttblQuery::create(null, $criteria)
                    ->filterByCrsenttblRelatedByUidentcrs($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPrbenttblsRelatedByUidentcrsPartial && count($collPrbenttblsRelatedByUidentcrs)) {
                        $this->initPrbenttblsRelatedByUidentcrs(false);

                        foreach ($collPrbenttblsRelatedByUidentcrs as $obj) {
                            if (false == $this->collPrbenttblsRelatedByUidentcrs->contains($obj)) {
                                $this->collPrbenttblsRelatedByUidentcrs->append($obj);
                            }
                        }

                        $this->collPrbenttblsRelatedByUidentcrsPartial = true;
                    }

                    return $collPrbenttblsRelatedByUidentcrs;
                }

                if ($partial && $this->collPrbenttblsRelatedByUidentcrs) {
                    foreach ($this->collPrbenttblsRelatedByUidentcrs as $obj) {
                        if ($obj->isNew()) {
                            $collPrbenttblsRelatedByUidentcrs[] = $obj;
                        }
                    }
                }

                $this->collPrbenttblsRelatedByUidentcrs = $collPrbenttblsRelatedByUidentcrs;
                $this->collPrbenttblsRelatedByUidentcrsPartial = false;
            }
        }

        return $this->collPrbenttblsRelatedByUidentcrs;
    }

    /**
     * Sets a collection of ChildPrbenttbl objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $prbenttblsRelatedByUidentcrs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCrsenttbl The current object (for fluent API support)
     */
    public function setPrbenttblsRelatedByUidentcrs(Collection $prbenttblsRelatedByUidentcrs, ConnectionInterface $con = null)
    {
        /** @var ChildPrbenttbl[] $prbenttblsRelatedByUidentcrsToDelete */
        $prbenttblsRelatedByUidentcrsToDelete = $this->getPrbenttblsRelatedByUidentcrs(new Criteria(), $con)->diff($prbenttblsRelatedByUidentcrs);


        $this->prbenttblsRelatedByUidentcrsScheduledForDeletion = $prbenttblsRelatedByUidentcrsToDelete;

        foreach ($prbenttblsRelatedByUidentcrsToDelete as $prbenttblRelatedByUidentcrsRemoved) {
            $prbenttblRelatedByUidentcrsRemoved->setCrsenttblRelatedByUidentcrs(null);
        }

        $this->collPrbenttblsRelatedByUidentcrs = null;
        foreach ($prbenttblsRelatedByUidentcrs as $prbenttblRelatedByUidentcrs) {
            $this->addPrbenttblRelatedByUidentcrs($prbenttblRelatedByUidentcrs);
        }

        $this->collPrbenttblsRelatedByUidentcrs = $prbenttblsRelatedByUidentcrs;
        $this->collPrbenttblsRelatedByUidentcrsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prbenttbl objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Prbenttbl objects.
     * @throws PropelException
     */
    public function countPrbenttblsRelatedByUidentcrs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPrbenttblsRelatedByUidentcrsPartial && !$this->isNew();
        if (null === $this->collPrbenttblsRelatedByUidentcrs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrbenttblsRelatedByUidentcrs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPrbenttblsRelatedByUidentcrs());
            }

            $query = ChildPrbenttblQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCrsenttblRelatedByUidentcrs($this)
                ->count($con);
        }

        return count($this->collPrbenttblsRelatedByUidentcrs);
    }

    /**
     * Method called to associate a ChildPrbenttbl object to this object
     * through the ChildPrbenttbl foreign key attribute.
     *
     * @param  ChildPrbenttbl $l ChildPrbenttbl
     * @return $this|\Crsenttbl The current object (for fluent API support)
     */
    public function addPrbenttblRelatedByUidentcrs(ChildPrbenttbl $l)
    {
        if ($this->collPrbenttblsRelatedByUidentcrs === null) {
            $this->initPrbenttblsRelatedByUidentcrs();
            $this->collPrbenttblsRelatedByUidentcrsPartial = true;
        }

        if (!$this->collPrbenttblsRelatedByUidentcrs->contains($l)) {
            $this->doAddPrbenttblRelatedByUidentcrs($l);

            if ($this->prbenttblsRelatedByUidentcrsScheduledForDeletion and $this->prbenttblsRelatedByUidentcrsScheduledForDeletion->contains($l)) {
                $this->prbenttblsRelatedByUidentcrsScheduledForDeletion->remove($this->prbenttblsRelatedByUidentcrsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrbenttbl $prbenttblRelatedByUidentcrs The ChildPrbenttbl object to add.
     */
    protected function doAddPrbenttblRelatedByUidentcrs(ChildPrbenttbl $prbenttblRelatedByUidentcrs)
    {
        $this->collPrbenttblsRelatedByUidentcrs[]= $prbenttblRelatedByUidentcrs;
        $prbenttblRelatedByUidentcrs->setCrsenttblRelatedByUidentcrs($this);
    }

    /**
     * @param  ChildPrbenttbl $prbenttblRelatedByUidentcrs The ChildPrbenttbl object to remove.
     * @return $this|ChildCrsenttbl The current object (for fluent API support)
     */
    public function removePrbenttblRelatedByUidentcrs(ChildPrbenttbl $prbenttblRelatedByUidentcrs)
    {
        if ($this->getPrbenttblsRelatedByUidentcrs()->contains($prbenttblRelatedByUidentcrs)) {
            $pos = $this->collPrbenttblsRelatedByUidentcrs->search($prbenttblRelatedByUidentcrs);
            $this->collPrbenttblsRelatedByUidentcrs->remove($pos);
            if (null === $this->prbenttblsRelatedByUidentcrsScheduledForDeletion) {
                $this->prbenttblsRelatedByUidentcrsScheduledForDeletion = clone $this->collPrbenttblsRelatedByUidentcrs;
                $this->prbenttblsRelatedByUidentcrsScheduledForDeletion->clear();
            }
            $this->prbenttblsRelatedByUidentcrsScheduledForDeletion[]= $prbenttblRelatedByUidentcrs;
            $prbenttblRelatedByUidentcrs->setCrsenttblRelatedByUidentcrs(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Crsenttbl is new, it will return
     * an empty collection; or if this Crsenttbl has previously
     * been saved, it will retrieve related PrbenttblsRelatedByUidentcrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Crsenttbl.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrbenttbl[] List of ChildPrbenttbl objects
     */
    public function getPrbenttblsRelatedByUidentcrsJoinUsersRelatedByIdnentusr(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrbenttblQuery::create(null, $criteria);
        $query->joinWith('UsersRelatedByIdnentusr', $joinBehavior);

        return $this->getPrbenttblsRelatedByUidentcrs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Crsenttbl is new, it will return
     * an empty collection; or if this Crsenttbl has previously
     * been saved, it will retrieve related PrbenttblsRelatedByUidentcrs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Crsenttbl.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrbenttbl[] List of ChildPrbenttbl objects
     */
    public function getPrbenttblsRelatedByUidentcrsJoinUsersRelatedByUidentusr(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPrbenttblQuery::create(null, $criteria);
        $query->joinWith('UsersRelatedByUidentusr', $joinBehavior);

        return $this->getPrbenttblsRelatedByUidentcrs($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->idnentcrs = null;
        $this->uuid = null;
        $this->nmbentcrs = null;
        $this->dscentcrs = null;
        $this->prcentcrs = null;
        $this->lnkentcrs = null;
        $this->rmventcrs = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collChcenttblsRelatedByIdnentcrs) {
                foreach ($this->collChcenttblsRelatedByIdnentcrs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collChcenttblsRelatedByUidentcrs) {
                foreach ($this->collChcenttblsRelatedByUidentcrs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrbenttblsRelatedByIdnentcrs) {
                foreach ($this->collPrbenttblsRelatedByIdnentcrs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrbenttblsRelatedByUidentcrs) {
                foreach ($this->collPrbenttblsRelatedByUidentcrs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collChcenttblsRelatedByIdnentcrs = null;
        $this->collChcenttblsRelatedByUidentcrs = null;
        $this->collPrbenttblsRelatedByIdnentcrs = null;
        $this->collPrbenttblsRelatedByUidentcrs = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CrsenttblTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
