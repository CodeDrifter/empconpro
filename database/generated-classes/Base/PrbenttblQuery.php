<?php

namespace Base;

use \Prbenttbl as ChildPrbenttbl;
use \PrbenttblQuery as ChildPrbenttblQuery;
use \Exception;
use \PDO;
use Map\PrbenttblTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'prbenttbl' table.
 *
 *
 *
 * @method     ChildPrbenttblQuery orderByIdnentprb($order = Criteria::ASC) Order by the idnentprb column
 * @method     ChildPrbenttblQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildPrbenttblQuery orderByIdnentusr($order = Criteria::ASC) Order by the idnentusr column
 * @method     ChildPrbenttblQuery orderByUidentusr($order = Criteria::ASC) Order by the uidentusr column
 * @method     ChildPrbenttblQuery orderByIdnentcrs($order = Criteria::ASC) Order by the idnentcrs column
 * @method     ChildPrbenttblQuery orderByUidentcrs($order = Criteria::ASC) Order by the uidentcrs column
 * @method     ChildPrbenttblQuery orderByVidentprb($order = Criteria::ASC) Order by the videntprb column
 * @method     ChildPrbenttblQuery orderByHrrentprb($order = Criteria::ASC) Order by the hrrentprb column
 * @method     ChildPrbenttblQuery orderByStdentprb($order = Criteria::ASC) Order by the stdentprb column
 * @method     ChildPrbenttblQuery orderByRmventprb($order = Criteria::ASC) Order by the rmventprb column
 * @method     ChildPrbenttblQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPrbenttblQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildPrbenttblQuery orderByEmlentprb($order = Criteria::ASC) Order by the emlentprb column
 * @method     ChildPrbenttblQuery orderByBndentprb($order = Criteria::ASC) Order by the bndentprb column
 * @method     ChildPrbenttblQuery orderByVstentprb($order = Criteria::ASC) Order by the vstentprb column
 *
 * @method     ChildPrbenttblQuery groupByIdnentprb() Group by the idnentprb column
 * @method     ChildPrbenttblQuery groupByUuid() Group by the uuid column
 * @method     ChildPrbenttblQuery groupByIdnentusr() Group by the idnentusr column
 * @method     ChildPrbenttblQuery groupByUidentusr() Group by the uidentusr column
 * @method     ChildPrbenttblQuery groupByIdnentcrs() Group by the idnentcrs column
 * @method     ChildPrbenttblQuery groupByUidentcrs() Group by the uidentcrs column
 * @method     ChildPrbenttblQuery groupByVidentprb() Group by the videntprb column
 * @method     ChildPrbenttblQuery groupByHrrentprb() Group by the hrrentprb column
 * @method     ChildPrbenttblQuery groupByStdentprb() Group by the stdentprb column
 * @method     ChildPrbenttblQuery groupByRmventprb() Group by the rmventprb column
 * @method     ChildPrbenttblQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPrbenttblQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildPrbenttblQuery groupByEmlentprb() Group by the emlentprb column
 * @method     ChildPrbenttblQuery groupByBndentprb() Group by the bndentprb column
 * @method     ChildPrbenttblQuery groupByVstentprb() Group by the vstentprb column
 *
 * @method     ChildPrbenttblQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPrbenttblQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPrbenttblQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPrbenttblQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPrbenttblQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPrbenttblQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPrbenttblQuery leftJoinCrsenttblRelatedByIdnentcrs($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrsenttblRelatedByIdnentcrs relation
 * @method     ChildPrbenttblQuery rightJoinCrsenttblRelatedByIdnentcrs($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrsenttblRelatedByIdnentcrs relation
 * @method     ChildPrbenttblQuery innerJoinCrsenttblRelatedByIdnentcrs($relationAlias = null) Adds a INNER JOIN clause to the query using the CrsenttblRelatedByIdnentcrs relation
 *
 * @method     ChildPrbenttblQuery joinWithCrsenttblRelatedByIdnentcrs($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrsenttblRelatedByIdnentcrs relation
 *
 * @method     ChildPrbenttblQuery leftJoinWithCrsenttblRelatedByIdnentcrs() Adds a LEFT JOIN clause and with to the query using the CrsenttblRelatedByIdnentcrs relation
 * @method     ChildPrbenttblQuery rightJoinWithCrsenttblRelatedByIdnentcrs() Adds a RIGHT JOIN clause and with to the query using the CrsenttblRelatedByIdnentcrs relation
 * @method     ChildPrbenttblQuery innerJoinWithCrsenttblRelatedByIdnentcrs() Adds a INNER JOIN clause and with to the query using the CrsenttblRelatedByIdnentcrs relation
 *
 * @method     ChildPrbenttblQuery leftJoinUsersRelatedByIdnentusr($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsersRelatedByIdnentusr relation
 * @method     ChildPrbenttblQuery rightJoinUsersRelatedByIdnentusr($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsersRelatedByIdnentusr relation
 * @method     ChildPrbenttblQuery innerJoinUsersRelatedByIdnentusr($relationAlias = null) Adds a INNER JOIN clause to the query using the UsersRelatedByIdnentusr relation
 *
 * @method     ChildPrbenttblQuery joinWithUsersRelatedByIdnentusr($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsersRelatedByIdnentusr relation
 *
 * @method     ChildPrbenttblQuery leftJoinWithUsersRelatedByIdnentusr() Adds a LEFT JOIN clause and with to the query using the UsersRelatedByIdnentusr relation
 * @method     ChildPrbenttblQuery rightJoinWithUsersRelatedByIdnentusr() Adds a RIGHT JOIN clause and with to the query using the UsersRelatedByIdnentusr relation
 * @method     ChildPrbenttblQuery innerJoinWithUsersRelatedByIdnentusr() Adds a INNER JOIN clause and with to the query using the UsersRelatedByIdnentusr relation
 *
 * @method     ChildPrbenttblQuery leftJoinCrsenttblRelatedByUidentcrs($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrsenttblRelatedByUidentcrs relation
 * @method     ChildPrbenttblQuery rightJoinCrsenttblRelatedByUidentcrs($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrsenttblRelatedByUidentcrs relation
 * @method     ChildPrbenttblQuery innerJoinCrsenttblRelatedByUidentcrs($relationAlias = null) Adds a INNER JOIN clause to the query using the CrsenttblRelatedByUidentcrs relation
 *
 * @method     ChildPrbenttblQuery joinWithCrsenttblRelatedByUidentcrs($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrsenttblRelatedByUidentcrs relation
 *
 * @method     ChildPrbenttblQuery leftJoinWithCrsenttblRelatedByUidentcrs() Adds a LEFT JOIN clause and with to the query using the CrsenttblRelatedByUidentcrs relation
 * @method     ChildPrbenttblQuery rightJoinWithCrsenttblRelatedByUidentcrs() Adds a RIGHT JOIN clause and with to the query using the CrsenttblRelatedByUidentcrs relation
 * @method     ChildPrbenttblQuery innerJoinWithCrsenttblRelatedByUidentcrs() Adds a INNER JOIN clause and with to the query using the CrsenttblRelatedByUidentcrs relation
 *
 * @method     ChildPrbenttblQuery leftJoinUsersRelatedByUidentusr($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsersRelatedByUidentusr relation
 * @method     ChildPrbenttblQuery rightJoinUsersRelatedByUidentusr($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsersRelatedByUidentusr relation
 * @method     ChildPrbenttblQuery innerJoinUsersRelatedByUidentusr($relationAlias = null) Adds a INNER JOIN clause to the query using the UsersRelatedByUidentusr relation
 *
 * @method     ChildPrbenttblQuery joinWithUsersRelatedByUidentusr($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsersRelatedByUidentusr relation
 *
 * @method     ChildPrbenttblQuery leftJoinWithUsersRelatedByUidentusr() Adds a LEFT JOIN clause and with to the query using the UsersRelatedByUidentusr relation
 * @method     ChildPrbenttblQuery rightJoinWithUsersRelatedByUidentusr() Adds a RIGHT JOIN clause and with to the query using the UsersRelatedByUidentusr relation
 * @method     ChildPrbenttblQuery innerJoinWithUsersRelatedByUidentusr() Adds a INNER JOIN clause and with to the query using the UsersRelatedByUidentusr relation
 *
 * @method     \CrsenttblQuery|\UsersQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPrbenttbl findOne(ConnectionInterface $con = null) Return the first ChildPrbenttbl matching the query
 * @method     ChildPrbenttbl findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPrbenttbl matching the query, or a new ChildPrbenttbl object populated from the query conditions when no match is found
 *
 * @method     ChildPrbenttbl findOneByIdnentprb(int $idnentprb) Return the first ChildPrbenttbl filtered by the idnentprb column
 * @method     ChildPrbenttbl findOneByUuid(string $uuid) Return the first ChildPrbenttbl filtered by the uuid column
 * @method     ChildPrbenttbl findOneByIdnentusr(int $idnentusr) Return the first ChildPrbenttbl filtered by the idnentusr column
 * @method     ChildPrbenttbl findOneByUidentusr(string $uidentusr) Return the first ChildPrbenttbl filtered by the uidentusr column
 * @method     ChildPrbenttbl findOneByIdnentcrs(int $idnentcrs) Return the first ChildPrbenttbl filtered by the idnentcrs column
 * @method     ChildPrbenttbl findOneByUidentcrs(string $uidentcrs) Return the first ChildPrbenttbl filtered by the uidentcrs column
 * @method     ChildPrbenttbl findOneByVidentprb(int $videntprb) Return the first ChildPrbenttbl filtered by the videntprb column
 * @method     ChildPrbenttbl findOneByHrrentprb(string $hrrentprb) Return the first ChildPrbenttbl filtered by the hrrentprb column
 * @method     ChildPrbenttbl findOneByStdentprb(string $stdentprb) Return the first ChildPrbenttbl filtered by the stdentprb column
 * @method     ChildPrbenttbl findOneByRmventprb(boolean $rmventprb) Return the first ChildPrbenttbl filtered by the rmventprb column
 * @method     ChildPrbenttbl findOneByCreatedAt(string $created_at) Return the first ChildPrbenttbl filtered by the created_at column
 * @method     ChildPrbenttbl findOneByUpdatedAt(string $updated_at) Return the first ChildPrbenttbl filtered by the updated_at column
 * @method     ChildPrbenttbl findOneByEmlentprb(string $emlentprb) Return the first ChildPrbenttbl filtered by the emlentprb column
 * @method     ChildPrbenttbl findOneByBndentprb(boolean $bndentprb) Return the first ChildPrbenttbl filtered by the bndentprb column
 * @method     ChildPrbenttbl findOneByVstentprb(string $vstentprb) Return the first ChildPrbenttbl filtered by the vstentprb column *

 * @method     ChildPrbenttbl requirePk($key, ConnectionInterface $con = null) Return the ChildPrbenttbl by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOne(ConnectionInterface $con = null) Return the first ChildPrbenttbl matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPrbenttbl requireOneByIdnentprb(int $idnentprb) Return the first ChildPrbenttbl filtered by the idnentprb column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOneByUuid(string $uuid) Return the first ChildPrbenttbl filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOneByIdnentusr(int $idnentusr) Return the first ChildPrbenttbl filtered by the idnentusr column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOneByUidentusr(string $uidentusr) Return the first ChildPrbenttbl filtered by the uidentusr column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOneByIdnentcrs(int $idnentcrs) Return the first ChildPrbenttbl filtered by the idnentcrs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOneByUidentcrs(string $uidentcrs) Return the first ChildPrbenttbl filtered by the uidentcrs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOneByVidentprb(int $videntprb) Return the first ChildPrbenttbl filtered by the videntprb column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOneByHrrentprb(string $hrrentprb) Return the first ChildPrbenttbl filtered by the hrrentprb column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOneByStdentprb(string $stdentprb) Return the first ChildPrbenttbl filtered by the stdentprb column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOneByRmventprb(boolean $rmventprb) Return the first ChildPrbenttbl filtered by the rmventprb column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOneByCreatedAt(string $created_at) Return the first ChildPrbenttbl filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOneByUpdatedAt(string $updated_at) Return the first ChildPrbenttbl filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOneByEmlentprb(string $emlentprb) Return the first ChildPrbenttbl filtered by the emlentprb column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOneByBndentprb(boolean $bndentprb) Return the first ChildPrbenttbl filtered by the bndentprb column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrbenttbl requireOneByVstentprb(string $vstentprb) Return the first ChildPrbenttbl filtered by the vstentprb column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPrbenttbl[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPrbenttbl objects based on current ModelCriteria
 * @method     ChildPrbenttbl[]|ObjectCollection findByIdnentprb(int $idnentprb) Return ChildPrbenttbl objects filtered by the idnentprb column
 * @method     ChildPrbenttbl[]|ObjectCollection findByUuid(string $uuid) Return ChildPrbenttbl objects filtered by the uuid column
 * @method     ChildPrbenttbl[]|ObjectCollection findByIdnentusr(int $idnentusr) Return ChildPrbenttbl objects filtered by the idnentusr column
 * @method     ChildPrbenttbl[]|ObjectCollection findByUidentusr(string $uidentusr) Return ChildPrbenttbl objects filtered by the uidentusr column
 * @method     ChildPrbenttbl[]|ObjectCollection findByIdnentcrs(int $idnentcrs) Return ChildPrbenttbl objects filtered by the idnentcrs column
 * @method     ChildPrbenttbl[]|ObjectCollection findByUidentcrs(string $uidentcrs) Return ChildPrbenttbl objects filtered by the uidentcrs column
 * @method     ChildPrbenttbl[]|ObjectCollection findByVidentprb(int $videntprb) Return ChildPrbenttbl objects filtered by the videntprb column
 * @method     ChildPrbenttbl[]|ObjectCollection findByHrrentprb(string $hrrentprb) Return ChildPrbenttbl objects filtered by the hrrentprb column
 * @method     ChildPrbenttbl[]|ObjectCollection findByStdentprb(string $stdentprb) Return ChildPrbenttbl objects filtered by the stdentprb column
 * @method     ChildPrbenttbl[]|ObjectCollection findByRmventprb(boolean $rmventprb) Return ChildPrbenttbl objects filtered by the rmventprb column
 * @method     ChildPrbenttbl[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPrbenttbl objects filtered by the created_at column
 * @method     ChildPrbenttbl[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPrbenttbl objects filtered by the updated_at column
 * @method     ChildPrbenttbl[]|ObjectCollection findByEmlentprb(string $emlentprb) Return ChildPrbenttbl objects filtered by the emlentprb column
 * @method     ChildPrbenttbl[]|ObjectCollection findByBndentprb(boolean $bndentprb) Return ChildPrbenttbl objects filtered by the bndentprb column
 * @method     ChildPrbenttbl[]|ObjectCollection findByVstentprb(string $vstentprb) Return ChildPrbenttbl objects filtered by the vstentprb column
 * @method     ChildPrbenttbl[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PrbenttblQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PrbenttblQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'empconpro', $modelName = '\\Prbenttbl', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPrbenttblQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPrbenttblQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPrbenttblQuery) {
            return $criteria;
        }
        $query = new ChildPrbenttblQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPrbenttbl|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PrbenttblTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PrbenttblTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrbenttbl A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idnentprb, uuid, idnentusr, uidentusr, idnentcrs, uidentcrs, videntprb, hrrentprb, stdentprb, rmventprb, created_at, updated_at, emlentprb, bndentprb, vstentprb FROM prbenttbl WHERE idnentprb = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPrbenttbl $obj */
            $obj = new ChildPrbenttbl();
            $obj->hydrate($row);
            PrbenttblTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPrbenttbl|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PrbenttblTableMap::COL_IDNENTPRB, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PrbenttblTableMap::COL_IDNENTPRB, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idnentprb column
     *
     * Example usage:
     * <code>
     * $query->filterByIdnentprb(1234); // WHERE idnentprb = 1234
     * $query->filterByIdnentprb(array(12, 34)); // WHERE idnentprb IN (12, 34)
     * $query->filterByIdnentprb(array('min' => 12)); // WHERE idnentprb > 12
     * </code>
     *
     * @param     mixed $idnentprb The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByIdnentprb($idnentprb = null, $comparison = null)
    {
        if (is_array($idnentprb)) {
            $useMinMax = false;
            if (isset($idnentprb['min'])) {
                $this->addUsingAlias(PrbenttblTableMap::COL_IDNENTPRB, $idnentprb['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idnentprb['max'])) {
                $this->addUsingAlias(PrbenttblTableMap::COL_IDNENTPRB, $idnentprb['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_IDNENTPRB, $idnentprb, $comparison);
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uuid The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_UUID, $uuid, $comparison);
    }

    /**
     * Filter the query on the idnentusr column
     *
     * Example usage:
     * <code>
     * $query->filterByIdnentusr(1234); // WHERE idnentusr = 1234
     * $query->filterByIdnentusr(array(12, 34)); // WHERE idnentusr IN (12, 34)
     * $query->filterByIdnentusr(array('min' => 12)); // WHERE idnentusr > 12
     * </code>
     *
     * @see       filterByUsersRelatedByIdnentusr()
     *
     * @param     mixed $idnentusr The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByIdnentusr($idnentusr = null, $comparison = null)
    {
        if (is_array($idnentusr)) {
            $useMinMax = false;
            if (isset($idnentusr['min'])) {
                $this->addUsingAlias(PrbenttblTableMap::COL_IDNENTUSR, $idnentusr['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idnentusr['max'])) {
                $this->addUsingAlias(PrbenttblTableMap::COL_IDNENTUSR, $idnentusr['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_IDNENTUSR, $idnentusr, $comparison);
    }

    /**
     * Filter the query on the uidentusr column
     *
     * Example usage:
     * <code>
     * $query->filterByUidentusr('fooValue');   // WHERE uidentusr = 'fooValue'
     * $query->filterByUidentusr('%fooValue%', Criteria::LIKE); // WHERE uidentusr LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uidentusr The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByUidentusr($uidentusr = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uidentusr)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_UIDENTUSR, $uidentusr, $comparison);
    }

    /**
     * Filter the query on the idnentcrs column
     *
     * Example usage:
     * <code>
     * $query->filterByIdnentcrs(1234); // WHERE idnentcrs = 1234
     * $query->filterByIdnentcrs(array(12, 34)); // WHERE idnentcrs IN (12, 34)
     * $query->filterByIdnentcrs(array('min' => 12)); // WHERE idnentcrs > 12
     * </code>
     *
     * @see       filterByCrsenttblRelatedByIdnentcrs()
     *
     * @param     mixed $idnentcrs The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByIdnentcrs($idnentcrs = null, $comparison = null)
    {
        if (is_array($idnentcrs)) {
            $useMinMax = false;
            if (isset($idnentcrs['min'])) {
                $this->addUsingAlias(PrbenttblTableMap::COL_IDNENTCRS, $idnentcrs['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idnentcrs['max'])) {
                $this->addUsingAlias(PrbenttblTableMap::COL_IDNENTCRS, $idnentcrs['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_IDNENTCRS, $idnentcrs, $comparison);
    }

    /**
     * Filter the query on the uidentcrs column
     *
     * Example usage:
     * <code>
     * $query->filterByUidentcrs('fooValue');   // WHERE uidentcrs = 'fooValue'
     * $query->filterByUidentcrs('%fooValue%', Criteria::LIKE); // WHERE uidentcrs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uidentcrs The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByUidentcrs($uidentcrs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uidentcrs)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_UIDENTCRS, $uidentcrs, $comparison);
    }

    /**
     * Filter the query on the videntprb column
     *
     * Example usage:
     * <code>
     * $query->filterByVidentprb(1234); // WHERE videntprb = 1234
     * $query->filterByVidentprb(array(12, 34)); // WHERE videntprb IN (12, 34)
     * $query->filterByVidentprb(array('min' => 12)); // WHERE videntprb > 12
     * </code>
     *
     * @param     mixed $videntprb The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByVidentprb($videntprb = null, $comparison = null)
    {
        if (is_array($videntprb)) {
            $useMinMax = false;
            if (isset($videntprb['min'])) {
                $this->addUsingAlias(PrbenttblTableMap::COL_VIDENTPRB, $videntprb['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($videntprb['max'])) {
                $this->addUsingAlias(PrbenttblTableMap::COL_VIDENTPRB, $videntprb['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_VIDENTPRB, $videntprb, $comparison);
    }

    /**
     * Filter the query on the hrrentprb column
     *
     * Example usage:
     * <code>
     * $query->filterByHrrentprb('2011-03-14'); // WHERE hrrentprb = '2011-03-14'
     * $query->filterByHrrentprb('now'); // WHERE hrrentprb = '2011-03-14'
     * $query->filterByHrrentprb(array('max' => 'yesterday')); // WHERE hrrentprb > '2011-03-13'
     * </code>
     *
     * @param     mixed $hrrentprb The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByHrrentprb($hrrentprb = null, $comparison = null)
    {
        if (is_array($hrrentprb)) {
            $useMinMax = false;
            if (isset($hrrentprb['min'])) {
                $this->addUsingAlias(PrbenttblTableMap::COL_HRRENTPRB, $hrrentprb['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($hrrentprb['max'])) {
                $this->addUsingAlias(PrbenttblTableMap::COL_HRRENTPRB, $hrrentprb['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_HRRENTPRB, $hrrentprb, $comparison);
    }

    /**
     * Filter the query on the stdentprb column
     *
     * Example usage:
     * <code>
     * $query->filterByStdentprb('fooValue');   // WHERE stdentprb = 'fooValue'
     * $query->filterByStdentprb('%fooValue%', Criteria::LIKE); // WHERE stdentprb LIKE '%fooValue%'
     * </code>
     *
     * @param     string $stdentprb The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByStdentprb($stdentprb = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($stdentprb)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_STDENTPRB, $stdentprb, $comparison);
    }

    /**
     * Filter the query on the rmventprb column
     *
     * Example usage:
     * <code>
     * $query->filterByRmventprb(true); // WHERE rmventprb = true
     * $query->filterByRmventprb('yes'); // WHERE rmventprb = true
     * </code>
     *
     * @param     boolean|string $rmventprb The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByRmventprb($rmventprb = null, $comparison = null)
    {
        if (is_string($rmventprb)) {
            $rmventprb = in_array(strtolower($rmventprb), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_RMVENTPRB, $rmventprb, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PrbenttblTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PrbenttblTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PrbenttblTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PrbenttblTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the emlentprb column
     *
     * Example usage:
     * <code>
     * $query->filterByEmlentprb('fooValue');   // WHERE emlentprb = 'fooValue'
     * $query->filterByEmlentprb('%fooValue%', Criteria::LIKE); // WHERE emlentprb LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emlentprb The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByEmlentprb($emlentprb = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emlentprb)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_EMLENTPRB, $emlentprb, $comparison);
    }

    /**
     * Filter the query on the bndentprb column
     *
     * Example usage:
     * <code>
     * $query->filterByBndentprb(true); // WHERE bndentprb = true
     * $query->filterByBndentprb('yes'); // WHERE bndentprb = true
     * </code>
     *
     * @param     boolean|string $bndentprb The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByBndentprb($bndentprb = null, $comparison = null)
    {
        if (is_string($bndentprb)) {
            $bndentprb = in_array(strtolower($bndentprb), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_BNDENTPRB, $bndentprb, $comparison);
    }

    /**
     * Filter the query on the vstentprb column
     *
     * Example usage:
     * <code>
     * $query->filterByVstentprb('fooValue');   // WHERE vstentprb = 'fooValue'
     * $query->filterByVstentprb('%fooValue%', Criteria::LIKE); // WHERE vstentprb LIKE '%fooValue%'
     * </code>
     *
     * @param     string $vstentprb The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByVstentprb($vstentprb = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($vstentprb)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrbenttblTableMap::COL_VSTENTPRB, $vstentprb, $comparison);
    }

    /**
     * Filter the query by a related \Crsenttbl object
     *
     * @param \Crsenttbl|ObjectCollection $crsenttbl The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByCrsenttblRelatedByIdnentcrs($crsenttbl, $comparison = null)
    {
        if ($crsenttbl instanceof \Crsenttbl) {
            return $this
                ->addUsingAlias(PrbenttblTableMap::COL_IDNENTCRS, $crsenttbl->getIdnentcrs(), $comparison);
        } elseif ($crsenttbl instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrbenttblTableMap::COL_IDNENTCRS, $crsenttbl->toKeyValue('PrimaryKey', 'Idnentcrs'), $comparison);
        } else {
            throw new PropelException('filterByCrsenttblRelatedByIdnentcrs() only accepts arguments of type \Crsenttbl or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrsenttblRelatedByIdnentcrs relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function joinCrsenttblRelatedByIdnentcrs($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrsenttblRelatedByIdnentcrs');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrsenttblRelatedByIdnentcrs');
        }

        return $this;
    }

    /**
     * Use the CrsenttblRelatedByIdnentcrs relation Crsenttbl object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CrsenttblQuery A secondary query class using the current class as primary query
     */
    public function useCrsenttblRelatedByIdnentcrsQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCrsenttblRelatedByIdnentcrs($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrsenttblRelatedByIdnentcrs', '\CrsenttblQuery');
    }

    /**
     * Filter the query by a related \Users object
     *
     * @param \Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByUsersRelatedByIdnentusr($users, $comparison = null)
    {
        if ($users instanceof \Users) {
            return $this
                ->addUsingAlias(PrbenttblTableMap::COL_IDNENTUSR, $users->getId(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrbenttblTableMap::COL_IDNENTUSR, $users->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsersRelatedByIdnentusr() only accepts arguments of type \Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsersRelatedByIdnentusr relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function joinUsersRelatedByIdnentusr($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsersRelatedByIdnentusr');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsersRelatedByIdnentusr');
        }

        return $this;
    }

    /**
     * Use the UsersRelatedByIdnentusr relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsersQuery A secondary query class using the current class as primary query
     */
    public function useUsersRelatedByIdnentusrQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUsersRelatedByIdnentusr($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsersRelatedByIdnentusr', '\UsersQuery');
    }

    /**
     * Filter the query by a related \Crsenttbl object
     *
     * @param \Crsenttbl|ObjectCollection $crsenttbl The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByCrsenttblRelatedByUidentcrs($crsenttbl, $comparison = null)
    {
        if ($crsenttbl instanceof \Crsenttbl) {
            return $this
                ->addUsingAlias(PrbenttblTableMap::COL_UIDENTCRS, $crsenttbl->getUuid(), $comparison);
        } elseif ($crsenttbl instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrbenttblTableMap::COL_UIDENTCRS, $crsenttbl->toKeyValue('PrimaryKey', 'Uuid'), $comparison);
        } else {
            throw new PropelException('filterByCrsenttblRelatedByUidentcrs() only accepts arguments of type \Crsenttbl or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrsenttblRelatedByUidentcrs relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function joinCrsenttblRelatedByUidentcrs($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrsenttblRelatedByUidentcrs');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrsenttblRelatedByUidentcrs');
        }

        return $this;
    }

    /**
     * Use the CrsenttblRelatedByUidentcrs relation Crsenttbl object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CrsenttblQuery A secondary query class using the current class as primary query
     */
    public function useCrsenttblRelatedByUidentcrsQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCrsenttblRelatedByUidentcrs($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrsenttblRelatedByUidentcrs', '\CrsenttblQuery');
    }

    /**
     * Filter the query by a related \Users object
     *
     * @param \Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrbenttblQuery The current query, for fluid interface
     */
    public function filterByUsersRelatedByUidentusr($users, $comparison = null)
    {
        if ($users instanceof \Users) {
            return $this
                ->addUsingAlias(PrbenttblTableMap::COL_UIDENTUSR, $users->getUuid(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrbenttblTableMap::COL_UIDENTUSR, $users->toKeyValue('PrimaryKey', 'Uuid'), $comparison);
        } else {
            throw new PropelException('filterByUsersRelatedByUidentusr() only accepts arguments of type \Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsersRelatedByUidentusr relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function joinUsersRelatedByUidentusr($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsersRelatedByUidentusr');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsersRelatedByUidentusr');
        }

        return $this;
    }

    /**
     * Use the UsersRelatedByUidentusr relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsersQuery A secondary query class using the current class as primary query
     */
    public function useUsersRelatedByUidentusrQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUsersRelatedByUidentusr($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsersRelatedByUidentusr', '\UsersQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPrbenttbl $prbenttbl Object to remove from the list of results
     *
     * @return $this|ChildPrbenttblQuery The current query, for fluid interface
     */
    public function prune($prbenttbl = null)
    {
        if ($prbenttbl) {
            $this->addUsingAlias(PrbenttblTableMap::COL_IDNENTPRB, $prbenttbl->getIdnentprb(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the prbenttbl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrbenttblTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PrbenttblTableMap::clearInstancePool();
            PrbenttblTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrbenttblTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PrbenttblTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PrbenttblTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PrbenttblTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PrbenttblQuery
