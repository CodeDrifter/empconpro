<?php

namespace Base;

use \Crsenttbl as ChildCrsenttbl;
use \CrsenttblQuery as ChildCrsenttblQuery;
use \Exception;
use \PDO;
use Map\CrsenttblTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'crsenttbl' table.
 *
 *
 *
 * @method     ChildCrsenttblQuery orderByIdnentcrs($order = Criteria::ASC) Order by the idnentcrs column
 * @method     ChildCrsenttblQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildCrsenttblQuery orderByNmbentcrs($order = Criteria::ASC) Order by the nmbentcrs column
 * @method     ChildCrsenttblQuery orderByDscentcrs($order = Criteria::ASC) Order by the dscentcrs column
 * @method     ChildCrsenttblQuery orderByPrcentcrs($order = Criteria::ASC) Order by the prcentcrs column
 * @method     ChildCrsenttblQuery orderByLnkentcrs($order = Criteria::ASC) Order by the lnkentcrs column
 * @method     ChildCrsenttblQuery orderByRmventcrs($order = Criteria::ASC) Order by the rmventcrs column
 * @method     ChildCrsenttblQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCrsenttblQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCrsenttblQuery groupByIdnentcrs() Group by the idnentcrs column
 * @method     ChildCrsenttblQuery groupByUuid() Group by the uuid column
 * @method     ChildCrsenttblQuery groupByNmbentcrs() Group by the nmbentcrs column
 * @method     ChildCrsenttblQuery groupByDscentcrs() Group by the dscentcrs column
 * @method     ChildCrsenttblQuery groupByPrcentcrs() Group by the prcentcrs column
 * @method     ChildCrsenttblQuery groupByLnkentcrs() Group by the lnkentcrs column
 * @method     ChildCrsenttblQuery groupByRmventcrs() Group by the rmventcrs column
 * @method     ChildCrsenttblQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCrsenttblQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCrsenttblQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCrsenttblQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCrsenttblQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCrsenttblQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCrsenttblQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCrsenttblQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCrsenttblQuery leftJoinChcenttblRelatedByIdnentcrs($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChcenttblRelatedByIdnentcrs relation
 * @method     ChildCrsenttblQuery rightJoinChcenttblRelatedByIdnentcrs($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChcenttblRelatedByIdnentcrs relation
 * @method     ChildCrsenttblQuery innerJoinChcenttblRelatedByIdnentcrs($relationAlias = null) Adds a INNER JOIN clause to the query using the ChcenttblRelatedByIdnentcrs relation
 *
 * @method     ChildCrsenttblQuery joinWithChcenttblRelatedByIdnentcrs($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChcenttblRelatedByIdnentcrs relation
 *
 * @method     ChildCrsenttblQuery leftJoinWithChcenttblRelatedByIdnentcrs() Adds a LEFT JOIN clause and with to the query using the ChcenttblRelatedByIdnentcrs relation
 * @method     ChildCrsenttblQuery rightJoinWithChcenttblRelatedByIdnentcrs() Adds a RIGHT JOIN clause and with to the query using the ChcenttblRelatedByIdnentcrs relation
 * @method     ChildCrsenttblQuery innerJoinWithChcenttblRelatedByIdnentcrs() Adds a INNER JOIN clause and with to the query using the ChcenttblRelatedByIdnentcrs relation
 *
 * @method     ChildCrsenttblQuery leftJoinChcenttblRelatedByUidentcrs($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChcenttblRelatedByUidentcrs relation
 * @method     ChildCrsenttblQuery rightJoinChcenttblRelatedByUidentcrs($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChcenttblRelatedByUidentcrs relation
 * @method     ChildCrsenttblQuery innerJoinChcenttblRelatedByUidentcrs($relationAlias = null) Adds a INNER JOIN clause to the query using the ChcenttblRelatedByUidentcrs relation
 *
 * @method     ChildCrsenttblQuery joinWithChcenttblRelatedByUidentcrs($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChcenttblRelatedByUidentcrs relation
 *
 * @method     ChildCrsenttblQuery leftJoinWithChcenttblRelatedByUidentcrs() Adds a LEFT JOIN clause and with to the query using the ChcenttblRelatedByUidentcrs relation
 * @method     ChildCrsenttblQuery rightJoinWithChcenttblRelatedByUidentcrs() Adds a RIGHT JOIN clause and with to the query using the ChcenttblRelatedByUidentcrs relation
 * @method     ChildCrsenttblQuery innerJoinWithChcenttblRelatedByUidentcrs() Adds a INNER JOIN clause and with to the query using the ChcenttblRelatedByUidentcrs relation
 *
 * @method     ChildCrsenttblQuery leftJoinPrbenttblRelatedByIdnentcrs($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrbenttblRelatedByIdnentcrs relation
 * @method     ChildCrsenttblQuery rightJoinPrbenttblRelatedByIdnentcrs($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrbenttblRelatedByIdnentcrs relation
 * @method     ChildCrsenttblQuery innerJoinPrbenttblRelatedByIdnentcrs($relationAlias = null) Adds a INNER JOIN clause to the query using the PrbenttblRelatedByIdnentcrs relation
 *
 * @method     ChildCrsenttblQuery joinWithPrbenttblRelatedByIdnentcrs($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PrbenttblRelatedByIdnentcrs relation
 *
 * @method     ChildCrsenttblQuery leftJoinWithPrbenttblRelatedByIdnentcrs() Adds a LEFT JOIN clause and with to the query using the PrbenttblRelatedByIdnentcrs relation
 * @method     ChildCrsenttblQuery rightJoinWithPrbenttblRelatedByIdnentcrs() Adds a RIGHT JOIN clause and with to the query using the PrbenttblRelatedByIdnentcrs relation
 * @method     ChildCrsenttblQuery innerJoinWithPrbenttblRelatedByIdnentcrs() Adds a INNER JOIN clause and with to the query using the PrbenttblRelatedByIdnentcrs relation
 *
 * @method     ChildCrsenttblQuery leftJoinPrbenttblRelatedByUidentcrs($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrbenttblRelatedByUidentcrs relation
 * @method     ChildCrsenttblQuery rightJoinPrbenttblRelatedByUidentcrs($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrbenttblRelatedByUidentcrs relation
 * @method     ChildCrsenttblQuery innerJoinPrbenttblRelatedByUidentcrs($relationAlias = null) Adds a INNER JOIN clause to the query using the PrbenttblRelatedByUidentcrs relation
 *
 * @method     ChildCrsenttblQuery joinWithPrbenttblRelatedByUidentcrs($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PrbenttblRelatedByUidentcrs relation
 *
 * @method     ChildCrsenttblQuery leftJoinWithPrbenttblRelatedByUidentcrs() Adds a LEFT JOIN clause and with to the query using the PrbenttblRelatedByUidentcrs relation
 * @method     ChildCrsenttblQuery rightJoinWithPrbenttblRelatedByUidentcrs() Adds a RIGHT JOIN clause and with to the query using the PrbenttblRelatedByUidentcrs relation
 * @method     ChildCrsenttblQuery innerJoinWithPrbenttblRelatedByUidentcrs() Adds a INNER JOIN clause and with to the query using the PrbenttblRelatedByUidentcrs relation
 *
 * @method     \ChcenttblQuery|\PrbenttblQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCrsenttbl findOne(ConnectionInterface $con = null) Return the first ChildCrsenttbl matching the query
 * @method     ChildCrsenttbl findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCrsenttbl matching the query, or a new ChildCrsenttbl object populated from the query conditions when no match is found
 *
 * @method     ChildCrsenttbl findOneByIdnentcrs(int $idnentcrs) Return the first ChildCrsenttbl filtered by the idnentcrs column
 * @method     ChildCrsenttbl findOneByUuid(string $uuid) Return the first ChildCrsenttbl filtered by the uuid column
 * @method     ChildCrsenttbl findOneByNmbentcrs(string $nmbentcrs) Return the first ChildCrsenttbl filtered by the nmbentcrs column
 * @method     ChildCrsenttbl findOneByDscentcrs(string $dscentcrs) Return the first ChildCrsenttbl filtered by the dscentcrs column
 * @method     ChildCrsenttbl findOneByPrcentcrs(int $prcentcrs) Return the first ChildCrsenttbl filtered by the prcentcrs column
 * @method     ChildCrsenttbl findOneByLnkentcrs(string $lnkentcrs) Return the first ChildCrsenttbl filtered by the lnkentcrs column
 * @method     ChildCrsenttbl findOneByRmventcrs(boolean $rmventcrs) Return the first ChildCrsenttbl filtered by the rmventcrs column
 * @method     ChildCrsenttbl findOneByCreatedAt(string $created_at) Return the first ChildCrsenttbl filtered by the created_at column
 * @method     ChildCrsenttbl findOneByUpdatedAt(string $updated_at) Return the first ChildCrsenttbl filtered by the updated_at column *

 * @method     ChildCrsenttbl requirePk($key, ConnectionInterface $con = null) Return the ChildCrsenttbl by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrsenttbl requireOne(ConnectionInterface $con = null) Return the first ChildCrsenttbl matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrsenttbl requireOneByIdnentcrs(int $idnentcrs) Return the first ChildCrsenttbl filtered by the idnentcrs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrsenttbl requireOneByUuid(string $uuid) Return the first ChildCrsenttbl filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrsenttbl requireOneByNmbentcrs(string $nmbentcrs) Return the first ChildCrsenttbl filtered by the nmbentcrs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrsenttbl requireOneByDscentcrs(string $dscentcrs) Return the first ChildCrsenttbl filtered by the dscentcrs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrsenttbl requireOneByPrcentcrs(int $prcentcrs) Return the first ChildCrsenttbl filtered by the prcentcrs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrsenttbl requireOneByLnkentcrs(string $lnkentcrs) Return the first ChildCrsenttbl filtered by the lnkentcrs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrsenttbl requireOneByRmventcrs(boolean $rmventcrs) Return the first ChildCrsenttbl filtered by the rmventcrs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrsenttbl requireOneByCreatedAt(string $created_at) Return the first ChildCrsenttbl filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrsenttbl requireOneByUpdatedAt(string $updated_at) Return the first ChildCrsenttbl filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrsenttbl[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCrsenttbl objects based on current ModelCriteria
 * @method     ChildCrsenttbl[]|ObjectCollection findByIdnentcrs(int $idnentcrs) Return ChildCrsenttbl objects filtered by the idnentcrs column
 * @method     ChildCrsenttbl[]|ObjectCollection findByUuid(string $uuid) Return ChildCrsenttbl objects filtered by the uuid column
 * @method     ChildCrsenttbl[]|ObjectCollection findByNmbentcrs(string $nmbentcrs) Return ChildCrsenttbl objects filtered by the nmbentcrs column
 * @method     ChildCrsenttbl[]|ObjectCollection findByDscentcrs(string $dscentcrs) Return ChildCrsenttbl objects filtered by the dscentcrs column
 * @method     ChildCrsenttbl[]|ObjectCollection findByPrcentcrs(int $prcentcrs) Return ChildCrsenttbl objects filtered by the prcentcrs column
 * @method     ChildCrsenttbl[]|ObjectCollection findByLnkentcrs(string $lnkentcrs) Return ChildCrsenttbl objects filtered by the lnkentcrs column
 * @method     ChildCrsenttbl[]|ObjectCollection findByRmventcrs(boolean $rmventcrs) Return ChildCrsenttbl objects filtered by the rmventcrs column
 * @method     ChildCrsenttbl[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCrsenttbl objects filtered by the created_at column
 * @method     ChildCrsenttbl[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCrsenttbl objects filtered by the updated_at column
 * @method     ChildCrsenttbl[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CrsenttblQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CrsenttblQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'empconpro', $modelName = '\\Crsenttbl', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCrsenttblQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCrsenttblQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCrsenttblQuery) {
            return $criteria;
        }
        $query = new ChildCrsenttblQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCrsenttbl|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CrsenttblTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CrsenttblTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrsenttbl A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idnentcrs, uuid, nmbentcrs, dscentcrs, prcentcrs, lnkentcrs, rmventcrs, created_at, updated_at FROM crsenttbl WHERE idnentcrs = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCrsenttbl $obj */
            $obj = new ChildCrsenttbl();
            $obj->hydrate($row);
            CrsenttblTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCrsenttbl|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CrsenttblTableMap::COL_IDNENTCRS, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CrsenttblTableMap::COL_IDNENTCRS, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idnentcrs column
     *
     * Example usage:
     * <code>
     * $query->filterByIdnentcrs(1234); // WHERE idnentcrs = 1234
     * $query->filterByIdnentcrs(array(12, 34)); // WHERE idnentcrs IN (12, 34)
     * $query->filterByIdnentcrs(array('min' => 12)); // WHERE idnentcrs > 12
     * </code>
     *
     * @param     mixed $idnentcrs The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByIdnentcrs($idnentcrs = null, $comparison = null)
    {
        if (is_array($idnentcrs)) {
            $useMinMax = false;
            if (isset($idnentcrs['min'])) {
                $this->addUsingAlias(CrsenttblTableMap::COL_IDNENTCRS, $idnentcrs['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idnentcrs['max'])) {
                $this->addUsingAlias(CrsenttblTableMap::COL_IDNENTCRS, $idnentcrs['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrsenttblTableMap::COL_IDNENTCRS, $idnentcrs, $comparison);
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uuid The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrsenttblTableMap::COL_UUID, $uuid, $comparison);
    }

    /**
     * Filter the query on the nmbentcrs column
     *
     * Example usage:
     * <code>
     * $query->filterByNmbentcrs('fooValue');   // WHERE nmbentcrs = 'fooValue'
     * $query->filterByNmbentcrs('%fooValue%', Criteria::LIKE); // WHERE nmbentcrs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nmbentcrs The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByNmbentcrs($nmbentcrs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nmbentcrs)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrsenttblTableMap::COL_NMBENTCRS, $nmbentcrs, $comparison);
    }

    /**
     * Filter the query on the dscentcrs column
     *
     * Example usage:
     * <code>
     * $query->filterByDscentcrs('fooValue');   // WHERE dscentcrs = 'fooValue'
     * $query->filterByDscentcrs('%fooValue%', Criteria::LIKE); // WHERE dscentcrs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dscentcrs The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByDscentcrs($dscentcrs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dscentcrs)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrsenttblTableMap::COL_DSCENTCRS, $dscentcrs, $comparison);
    }

    /**
     * Filter the query on the prcentcrs column
     *
     * Example usage:
     * <code>
     * $query->filterByPrcentcrs(1234); // WHERE prcentcrs = 1234
     * $query->filterByPrcentcrs(array(12, 34)); // WHERE prcentcrs IN (12, 34)
     * $query->filterByPrcentcrs(array('min' => 12)); // WHERE prcentcrs > 12
     * </code>
     *
     * @param     mixed $prcentcrs The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByPrcentcrs($prcentcrs = null, $comparison = null)
    {
        if (is_array($prcentcrs)) {
            $useMinMax = false;
            if (isset($prcentcrs['min'])) {
                $this->addUsingAlias(CrsenttblTableMap::COL_PRCENTCRS, $prcentcrs['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($prcentcrs['max'])) {
                $this->addUsingAlias(CrsenttblTableMap::COL_PRCENTCRS, $prcentcrs['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrsenttblTableMap::COL_PRCENTCRS, $prcentcrs, $comparison);
    }

    /**
     * Filter the query on the lnkentcrs column
     *
     * Example usage:
     * <code>
     * $query->filterByLnkentcrs('fooValue');   // WHERE lnkentcrs = 'fooValue'
     * $query->filterByLnkentcrs('%fooValue%', Criteria::LIKE); // WHERE lnkentcrs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lnkentcrs The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByLnkentcrs($lnkentcrs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lnkentcrs)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrsenttblTableMap::COL_LNKENTCRS, $lnkentcrs, $comparison);
    }

    /**
     * Filter the query on the rmventcrs column
     *
     * Example usage:
     * <code>
     * $query->filterByRmventcrs(true); // WHERE rmventcrs = true
     * $query->filterByRmventcrs('yes'); // WHERE rmventcrs = true
     * </code>
     *
     * @param     boolean|string $rmventcrs The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByRmventcrs($rmventcrs = null, $comparison = null)
    {
        if (is_string($rmventcrs)) {
            $rmventcrs = in_array(strtolower($rmventcrs), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CrsenttblTableMap::COL_RMVENTCRS, $rmventcrs, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CrsenttblTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CrsenttblTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrsenttblTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CrsenttblTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CrsenttblTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrsenttblTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Chcenttbl object
     *
     * @param \Chcenttbl|ObjectCollection $chcenttbl the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByChcenttblRelatedByIdnentcrs($chcenttbl, $comparison = null)
    {
        if ($chcenttbl instanceof \Chcenttbl) {
            return $this
                ->addUsingAlias(CrsenttblTableMap::COL_IDNENTCRS, $chcenttbl->getIdnentcrs(), $comparison);
        } elseif ($chcenttbl instanceof ObjectCollection) {
            return $this
                ->useChcenttblRelatedByIdnentcrsQuery()
                ->filterByPrimaryKeys($chcenttbl->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChcenttblRelatedByIdnentcrs() only accepts arguments of type \Chcenttbl or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChcenttblRelatedByIdnentcrs relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function joinChcenttblRelatedByIdnentcrs($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChcenttblRelatedByIdnentcrs');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChcenttblRelatedByIdnentcrs');
        }

        return $this;
    }

    /**
     * Use the ChcenttblRelatedByIdnentcrs relation Chcenttbl object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ChcenttblQuery A secondary query class using the current class as primary query
     */
    public function useChcenttblRelatedByIdnentcrsQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinChcenttblRelatedByIdnentcrs($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChcenttblRelatedByIdnentcrs', '\ChcenttblQuery');
    }

    /**
     * Filter the query by a related \Chcenttbl object
     *
     * @param \Chcenttbl|ObjectCollection $chcenttbl the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByChcenttblRelatedByUidentcrs($chcenttbl, $comparison = null)
    {
        if ($chcenttbl instanceof \Chcenttbl) {
            return $this
                ->addUsingAlias(CrsenttblTableMap::COL_UUID, $chcenttbl->getUidentcrs(), $comparison);
        } elseif ($chcenttbl instanceof ObjectCollection) {
            return $this
                ->useChcenttblRelatedByUidentcrsQuery()
                ->filterByPrimaryKeys($chcenttbl->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChcenttblRelatedByUidentcrs() only accepts arguments of type \Chcenttbl or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChcenttblRelatedByUidentcrs relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function joinChcenttblRelatedByUidentcrs($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChcenttblRelatedByUidentcrs');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChcenttblRelatedByUidentcrs');
        }

        return $this;
    }

    /**
     * Use the ChcenttblRelatedByUidentcrs relation Chcenttbl object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ChcenttblQuery A secondary query class using the current class as primary query
     */
    public function useChcenttblRelatedByUidentcrsQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinChcenttblRelatedByUidentcrs($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChcenttblRelatedByUidentcrs', '\ChcenttblQuery');
    }

    /**
     * Filter the query by a related \Prbenttbl object
     *
     * @param \Prbenttbl|ObjectCollection $prbenttbl the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByPrbenttblRelatedByIdnentcrs($prbenttbl, $comparison = null)
    {
        if ($prbenttbl instanceof \Prbenttbl) {
            return $this
                ->addUsingAlias(CrsenttblTableMap::COL_IDNENTCRS, $prbenttbl->getIdnentcrs(), $comparison);
        } elseif ($prbenttbl instanceof ObjectCollection) {
            return $this
                ->usePrbenttblRelatedByIdnentcrsQuery()
                ->filterByPrimaryKeys($prbenttbl->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrbenttblRelatedByIdnentcrs() only accepts arguments of type \Prbenttbl or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrbenttblRelatedByIdnentcrs relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function joinPrbenttblRelatedByIdnentcrs($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrbenttblRelatedByIdnentcrs');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrbenttblRelatedByIdnentcrs');
        }

        return $this;
    }

    /**
     * Use the PrbenttblRelatedByIdnentcrs relation Prbenttbl object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrbenttblQuery A secondary query class using the current class as primary query
     */
    public function usePrbenttblRelatedByIdnentcrsQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPrbenttblRelatedByIdnentcrs($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrbenttblRelatedByIdnentcrs', '\PrbenttblQuery');
    }

    /**
     * Filter the query by a related \Prbenttbl object
     *
     * @param \Prbenttbl|ObjectCollection $prbenttbl the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrsenttblQuery The current query, for fluid interface
     */
    public function filterByPrbenttblRelatedByUidentcrs($prbenttbl, $comparison = null)
    {
        if ($prbenttbl instanceof \Prbenttbl) {
            return $this
                ->addUsingAlias(CrsenttblTableMap::COL_UUID, $prbenttbl->getUidentcrs(), $comparison);
        } elseif ($prbenttbl instanceof ObjectCollection) {
            return $this
                ->usePrbenttblRelatedByUidentcrsQuery()
                ->filterByPrimaryKeys($prbenttbl->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrbenttblRelatedByUidentcrs() only accepts arguments of type \Prbenttbl or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrbenttblRelatedByUidentcrs relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function joinPrbenttblRelatedByUidentcrs($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrbenttblRelatedByUidentcrs');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrbenttblRelatedByUidentcrs');
        }

        return $this;
    }

    /**
     * Use the PrbenttblRelatedByUidentcrs relation Prbenttbl object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrbenttblQuery A secondary query class using the current class as primary query
     */
    public function usePrbenttblRelatedByUidentcrsQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPrbenttblRelatedByUidentcrs($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrbenttblRelatedByUidentcrs', '\PrbenttblQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCrsenttbl $crsenttbl Object to remove from the list of results
     *
     * @return $this|ChildCrsenttblQuery The current query, for fluid interface
     */
    public function prune($crsenttbl = null)
    {
        if ($crsenttbl) {
            $this->addUsingAlias(CrsenttblTableMap::COL_IDNENTCRS, $crsenttbl->getIdnentcrs(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the crsenttbl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrsenttblTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CrsenttblTableMap::clearInstancePool();
            CrsenttblTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrsenttblTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CrsenttblTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CrsenttblTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CrsenttblTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CrsenttblQuery
