<?php

namespace Base;

use \Chcenttbl as ChildChcenttbl;
use \ChcenttblQuery as ChildChcenttblQuery;
use \Exception;
use \PDO;
use Map\ChcenttblTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'chcenttbl' table.
 *
 *
 *
 * @method     ChildChcenttblQuery orderByIdnentchc($order = Criteria::ASC) Order by the idnentchc column
 * @method     ChildChcenttblQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildChcenttblQuery orderByIdnentusr($order = Criteria::ASC) Order by the idnentusr column
 * @method     ChildChcenttblQuery orderByUidentusr($order = Criteria::ASC) Order by the uidentusr column
 * @method     ChildChcenttblQuery orderByIdnentcrs($order = Criteria::ASC) Order by the idnentcrs column
 * @method     ChildChcenttblQuery orderByUidentcrs($order = Criteria::ASC) Order by the uidentcrs column
 * @method     ChildChcenttblQuery orderByRmventchc($order = Criteria::ASC) Order by the rmventchc column
 * @method     ChildChcenttblQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildChcenttblQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildChcenttblQuery groupByIdnentchc() Group by the idnentchc column
 * @method     ChildChcenttblQuery groupByUuid() Group by the uuid column
 * @method     ChildChcenttblQuery groupByIdnentusr() Group by the idnentusr column
 * @method     ChildChcenttblQuery groupByUidentusr() Group by the uidentusr column
 * @method     ChildChcenttblQuery groupByIdnentcrs() Group by the idnentcrs column
 * @method     ChildChcenttblQuery groupByUidentcrs() Group by the uidentcrs column
 * @method     ChildChcenttblQuery groupByRmventchc() Group by the rmventchc column
 * @method     ChildChcenttblQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildChcenttblQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildChcenttblQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildChcenttblQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildChcenttblQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildChcenttblQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildChcenttblQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildChcenttblQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildChcenttblQuery leftJoinCrsenttblRelatedByIdnentcrs($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrsenttblRelatedByIdnentcrs relation
 * @method     ChildChcenttblQuery rightJoinCrsenttblRelatedByIdnentcrs($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrsenttblRelatedByIdnentcrs relation
 * @method     ChildChcenttblQuery innerJoinCrsenttblRelatedByIdnentcrs($relationAlias = null) Adds a INNER JOIN clause to the query using the CrsenttblRelatedByIdnentcrs relation
 *
 * @method     ChildChcenttblQuery joinWithCrsenttblRelatedByIdnentcrs($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrsenttblRelatedByIdnentcrs relation
 *
 * @method     ChildChcenttblQuery leftJoinWithCrsenttblRelatedByIdnentcrs() Adds a LEFT JOIN clause and with to the query using the CrsenttblRelatedByIdnentcrs relation
 * @method     ChildChcenttblQuery rightJoinWithCrsenttblRelatedByIdnentcrs() Adds a RIGHT JOIN clause and with to the query using the CrsenttblRelatedByIdnentcrs relation
 * @method     ChildChcenttblQuery innerJoinWithCrsenttblRelatedByIdnentcrs() Adds a INNER JOIN clause and with to the query using the CrsenttblRelatedByIdnentcrs relation
 *
 * @method     ChildChcenttblQuery leftJoinUsersRelatedByIdnentusr($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsersRelatedByIdnentusr relation
 * @method     ChildChcenttblQuery rightJoinUsersRelatedByIdnentusr($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsersRelatedByIdnentusr relation
 * @method     ChildChcenttblQuery innerJoinUsersRelatedByIdnentusr($relationAlias = null) Adds a INNER JOIN clause to the query using the UsersRelatedByIdnentusr relation
 *
 * @method     ChildChcenttblQuery joinWithUsersRelatedByIdnentusr($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsersRelatedByIdnentusr relation
 *
 * @method     ChildChcenttblQuery leftJoinWithUsersRelatedByIdnentusr() Adds a LEFT JOIN clause and with to the query using the UsersRelatedByIdnentusr relation
 * @method     ChildChcenttblQuery rightJoinWithUsersRelatedByIdnentusr() Adds a RIGHT JOIN clause and with to the query using the UsersRelatedByIdnentusr relation
 * @method     ChildChcenttblQuery innerJoinWithUsersRelatedByIdnentusr() Adds a INNER JOIN clause and with to the query using the UsersRelatedByIdnentusr relation
 *
 * @method     ChildChcenttblQuery leftJoinCrsenttblRelatedByUidentcrs($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrsenttblRelatedByUidentcrs relation
 * @method     ChildChcenttblQuery rightJoinCrsenttblRelatedByUidentcrs($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrsenttblRelatedByUidentcrs relation
 * @method     ChildChcenttblQuery innerJoinCrsenttblRelatedByUidentcrs($relationAlias = null) Adds a INNER JOIN clause to the query using the CrsenttblRelatedByUidentcrs relation
 *
 * @method     ChildChcenttblQuery joinWithCrsenttblRelatedByUidentcrs($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrsenttblRelatedByUidentcrs relation
 *
 * @method     ChildChcenttblQuery leftJoinWithCrsenttblRelatedByUidentcrs() Adds a LEFT JOIN clause and with to the query using the CrsenttblRelatedByUidentcrs relation
 * @method     ChildChcenttblQuery rightJoinWithCrsenttblRelatedByUidentcrs() Adds a RIGHT JOIN clause and with to the query using the CrsenttblRelatedByUidentcrs relation
 * @method     ChildChcenttblQuery innerJoinWithCrsenttblRelatedByUidentcrs() Adds a INNER JOIN clause and with to the query using the CrsenttblRelatedByUidentcrs relation
 *
 * @method     ChildChcenttblQuery leftJoinUsersRelatedByUidentusr($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsersRelatedByUidentusr relation
 * @method     ChildChcenttblQuery rightJoinUsersRelatedByUidentusr($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsersRelatedByUidentusr relation
 * @method     ChildChcenttblQuery innerJoinUsersRelatedByUidentusr($relationAlias = null) Adds a INNER JOIN clause to the query using the UsersRelatedByUidentusr relation
 *
 * @method     ChildChcenttblQuery joinWithUsersRelatedByUidentusr($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsersRelatedByUidentusr relation
 *
 * @method     ChildChcenttblQuery leftJoinWithUsersRelatedByUidentusr() Adds a LEFT JOIN clause and with to the query using the UsersRelatedByUidentusr relation
 * @method     ChildChcenttblQuery rightJoinWithUsersRelatedByUidentusr() Adds a RIGHT JOIN clause and with to the query using the UsersRelatedByUidentusr relation
 * @method     ChildChcenttblQuery innerJoinWithUsersRelatedByUidentusr() Adds a INNER JOIN clause and with to the query using the UsersRelatedByUidentusr relation
 *
 * @method     \CrsenttblQuery|\UsersQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildChcenttbl findOne(ConnectionInterface $con = null) Return the first ChildChcenttbl matching the query
 * @method     ChildChcenttbl findOneOrCreate(ConnectionInterface $con = null) Return the first ChildChcenttbl matching the query, or a new ChildChcenttbl object populated from the query conditions when no match is found
 *
 * @method     ChildChcenttbl findOneByIdnentchc(int $idnentchc) Return the first ChildChcenttbl filtered by the idnentchc column
 * @method     ChildChcenttbl findOneByUuid(string $uuid) Return the first ChildChcenttbl filtered by the uuid column
 * @method     ChildChcenttbl findOneByIdnentusr(int $idnentusr) Return the first ChildChcenttbl filtered by the idnentusr column
 * @method     ChildChcenttbl findOneByUidentusr(string $uidentusr) Return the first ChildChcenttbl filtered by the uidentusr column
 * @method     ChildChcenttbl findOneByIdnentcrs(int $idnentcrs) Return the first ChildChcenttbl filtered by the idnentcrs column
 * @method     ChildChcenttbl findOneByUidentcrs(string $uidentcrs) Return the first ChildChcenttbl filtered by the uidentcrs column
 * @method     ChildChcenttbl findOneByRmventchc(boolean $rmventchc) Return the first ChildChcenttbl filtered by the rmventchc column
 * @method     ChildChcenttbl findOneByCreatedAt(string $created_at) Return the first ChildChcenttbl filtered by the created_at column
 * @method     ChildChcenttbl findOneByUpdatedAt(string $updated_at) Return the first ChildChcenttbl filtered by the updated_at column *

 * @method     ChildChcenttbl requirePk($key, ConnectionInterface $con = null) Return the ChildChcenttbl by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChcenttbl requireOne(ConnectionInterface $con = null) Return the first ChildChcenttbl matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChcenttbl requireOneByIdnentchc(int $idnentchc) Return the first ChildChcenttbl filtered by the idnentchc column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChcenttbl requireOneByUuid(string $uuid) Return the first ChildChcenttbl filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChcenttbl requireOneByIdnentusr(int $idnentusr) Return the first ChildChcenttbl filtered by the idnentusr column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChcenttbl requireOneByUidentusr(string $uidentusr) Return the first ChildChcenttbl filtered by the uidentusr column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChcenttbl requireOneByIdnentcrs(int $idnentcrs) Return the first ChildChcenttbl filtered by the idnentcrs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChcenttbl requireOneByUidentcrs(string $uidentcrs) Return the first ChildChcenttbl filtered by the uidentcrs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChcenttbl requireOneByRmventchc(boolean $rmventchc) Return the first ChildChcenttbl filtered by the rmventchc column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChcenttbl requireOneByCreatedAt(string $created_at) Return the first ChildChcenttbl filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChcenttbl requireOneByUpdatedAt(string $updated_at) Return the first ChildChcenttbl filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChcenttbl[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildChcenttbl objects based on current ModelCriteria
 * @method     ChildChcenttbl[]|ObjectCollection findByIdnentchc(int $idnentchc) Return ChildChcenttbl objects filtered by the idnentchc column
 * @method     ChildChcenttbl[]|ObjectCollection findByUuid(string $uuid) Return ChildChcenttbl objects filtered by the uuid column
 * @method     ChildChcenttbl[]|ObjectCollection findByIdnentusr(int $idnentusr) Return ChildChcenttbl objects filtered by the idnentusr column
 * @method     ChildChcenttbl[]|ObjectCollection findByUidentusr(string $uidentusr) Return ChildChcenttbl objects filtered by the uidentusr column
 * @method     ChildChcenttbl[]|ObjectCollection findByIdnentcrs(int $idnentcrs) Return ChildChcenttbl objects filtered by the idnentcrs column
 * @method     ChildChcenttbl[]|ObjectCollection findByUidentcrs(string $uidentcrs) Return ChildChcenttbl objects filtered by the uidentcrs column
 * @method     ChildChcenttbl[]|ObjectCollection findByRmventchc(boolean $rmventchc) Return ChildChcenttbl objects filtered by the rmventchc column
 * @method     ChildChcenttbl[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildChcenttbl objects filtered by the created_at column
 * @method     ChildChcenttbl[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildChcenttbl objects filtered by the updated_at column
 * @method     ChildChcenttbl[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ChcenttblQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ChcenttblQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'empconpro', $modelName = '\\Chcenttbl', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildChcenttblQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildChcenttblQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildChcenttblQuery) {
            return $criteria;
        }
        $query = new ChildChcenttblQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildChcenttbl|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ChcenttblTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ChcenttblTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChcenttbl A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idnentchc, uuid, idnentusr, uidentusr, idnentcrs, uidentcrs, rmventchc, created_at, updated_at FROM chcenttbl WHERE idnentchc = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildChcenttbl $obj */
            $obj = new ChildChcenttbl();
            $obj->hydrate($row);
            ChcenttblTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildChcenttbl|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ChcenttblTableMap::COL_IDNENTCHC, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ChcenttblTableMap::COL_IDNENTCHC, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idnentchc column
     *
     * Example usage:
     * <code>
     * $query->filterByIdnentchc(1234); // WHERE idnentchc = 1234
     * $query->filterByIdnentchc(array(12, 34)); // WHERE idnentchc IN (12, 34)
     * $query->filterByIdnentchc(array('min' => 12)); // WHERE idnentchc > 12
     * </code>
     *
     * @param     mixed $idnentchc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByIdnentchc($idnentchc = null, $comparison = null)
    {
        if (is_array($idnentchc)) {
            $useMinMax = false;
            if (isset($idnentchc['min'])) {
                $this->addUsingAlias(ChcenttblTableMap::COL_IDNENTCHC, $idnentchc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idnentchc['max'])) {
                $this->addUsingAlias(ChcenttblTableMap::COL_IDNENTCHC, $idnentchc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChcenttblTableMap::COL_IDNENTCHC, $idnentchc, $comparison);
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uuid The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChcenttblTableMap::COL_UUID, $uuid, $comparison);
    }

    /**
     * Filter the query on the idnentusr column
     *
     * Example usage:
     * <code>
     * $query->filterByIdnentusr(1234); // WHERE idnentusr = 1234
     * $query->filterByIdnentusr(array(12, 34)); // WHERE idnentusr IN (12, 34)
     * $query->filterByIdnentusr(array('min' => 12)); // WHERE idnentusr > 12
     * </code>
     *
     * @see       filterByUsersRelatedByIdnentusr()
     *
     * @param     mixed $idnentusr The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByIdnentusr($idnentusr = null, $comparison = null)
    {
        if (is_array($idnentusr)) {
            $useMinMax = false;
            if (isset($idnentusr['min'])) {
                $this->addUsingAlias(ChcenttblTableMap::COL_IDNENTUSR, $idnentusr['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idnentusr['max'])) {
                $this->addUsingAlias(ChcenttblTableMap::COL_IDNENTUSR, $idnentusr['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChcenttblTableMap::COL_IDNENTUSR, $idnentusr, $comparison);
    }

    /**
     * Filter the query on the uidentusr column
     *
     * Example usage:
     * <code>
     * $query->filterByUidentusr('fooValue');   // WHERE uidentusr = 'fooValue'
     * $query->filterByUidentusr('%fooValue%', Criteria::LIKE); // WHERE uidentusr LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uidentusr The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByUidentusr($uidentusr = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uidentusr)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChcenttblTableMap::COL_UIDENTUSR, $uidentusr, $comparison);
    }

    /**
     * Filter the query on the idnentcrs column
     *
     * Example usage:
     * <code>
     * $query->filterByIdnentcrs(1234); // WHERE idnentcrs = 1234
     * $query->filterByIdnentcrs(array(12, 34)); // WHERE idnentcrs IN (12, 34)
     * $query->filterByIdnentcrs(array('min' => 12)); // WHERE idnentcrs > 12
     * </code>
     *
     * @see       filterByCrsenttblRelatedByIdnentcrs()
     *
     * @param     mixed $idnentcrs The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByIdnentcrs($idnentcrs = null, $comparison = null)
    {
        if (is_array($idnentcrs)) {
            $useMinMax = false;
            if (isset($idnentcrs['min'])) {
                $this->addUsingAlias(ChcenttblTableMap::COL_IDNENTCRS, $idnentcrs['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idnentcrs['max'])) {
                $this->addUsingAlias(ChcenttblTableMap::COL_IDNENTCRS, $idnentcrs['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChcenttblTableMap::COL_IDNENTCRS, $idnentcrs, $comparison);
    }

    /**
     * Filter the query on the uidentcrs column
     *
     * Example usage:
     * <code>
     * $query->filterByUidentcrs('fooValue');   // WHERE uidentcrs = 'fooValue'
     * $query->filterByUidentcrs('%fooValue%', Criteria::LIKE); // WHERE uidentcrs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uidentcrs The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByUidentcrs($uidentcrs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uidentcrs)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChcenttblTableMap::COL_UIDENTCRS, $uidentcrs, $comparison);
    }

    /**
     * Filter the query on the rmventchc column
     *
     * Example usage:
     * <code>
     * $query->filterByRmventchc(true); // WHERE rmventchc = true
     * $query->filterByRmventchc('yes'); // WHERE rmventchc = true
     * </code>
     *
     * @param     boolean|string $rmventchc The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByRmventchc($rmventchc = null, $comparison = null)
    {
        if (is_string($rmventchc)) {
            $rmventchc = in_array(strtolower($rmventchc), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ChcenttblTableMap::COL_RMVENTCHC, $rmventchc, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ChcenttblTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ChcenttblTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChcenttblTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ChcenttblTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ChcenttblTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChcenttblTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Crsenttbl object
     *
     * @param \Crsenttbl|ObjectCollection $crsenttbl The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByCrsenttblRelatedByIdnentcrs($crsenttbl, $comparison = null)
    {
        if ($crsenttbl instanceof \Crsenttbl) {
            return $this
                ->addUsingAlias(ChcenttblTableMap::COL_IDNENTCRS, $crsenttbl->getIdnentcrs(), $comparison);
        } elseif ($crsenttbl instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChcenttblTableMap::COL_IDNENTCRS, $crsenttbl->toKeyValue('PrimaryKey', 'Idnentcrs'), $comparison);
        } else {
            throw new PropelException('filterByCrsenttblRelatedByIdnentcrs() only accepts arguments of type \Crsenttbl or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrsenttblRelatedByIdnentcrs relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function joinCrsenttblRelatedByIdnentcrs($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrsenttblRelatedByIdnentcrs');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrsenttblRelatedByIdnentcrs');
        }

        return $this;
    }

    /**
     * Use the CrsenttblRelatedByIdnentcrs relation Crsenttbl object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CrsenttblQuery A secondary query class using the current class as primary query
     */
    public function useCrsenttblRelatedByIdnentcrsQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCrsenttblRelatedByIdnentcrs($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrsenttblRelatedByIdnentcrs', '\CrsenttblQuery');
    }

    /**
     * Filter the query by a related \Users object
     *
     * @param \Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByUsersRelatedByIdnentusr($users, $comparison = null)
    {
        if ($users instanceof \Users) {
            return $this
                ->addUsingAlias(ChcenttblTableMap::COL_IDNENTUSR, $users->getId(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChcenttblTableMap::COL_IDNENTUSR, $users->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsersRelatedByIdnentusr() only accepts arguments of type \Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsersRelatedByIdnentusr relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function joinUsersRelatedByIdnentusr($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsersRelatedByIdnentusr');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsersRelatedByIdnentusr');
        }

        return $this;
    }

    /**
     * Use the UsersRelatedByIdnentusr relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsersQuery A secondary query class using the current class as primary query
     */
    public function useUsersRelatedByIdnentusrQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUsersRelatedByIdnentusr($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsersRelatedByIdnentusr', '\UsersQuery');
    }

    /**
     * Filter the query by a related \Crsenttbl object
     *
     * @param \Crsenttbl|ObjectCollection $crsenttbl The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByCrsenttblRelatedByUidentcrs($crsenttbl, $comparison = null)
    {
        if ($crsenttbl instanceof \Crsenttbl) {
            return $this
                ->addUsingAlias(ChcenttblTableMap::COL_UIDENTCRS, $crsenttbl->getUuid(), $comparison);
        } elseif ($crsenttbl instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChcenttblTableMap::COL_UIDENTCRS, $crsenttbl->toKeyValue('PrimaryKey', 'Uuid'), $comparison);
        } else {
            throw new PropelException('filterByCrsenttblRelatedByUidentcrs() only accepts arguments of type \Crsenttbl or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrsenttblRelatedByUidentcrs relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function joinCrsenttblRelatedByUidentcrs($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrsenttblRelatedByUidentcrs');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrsenttblRelatedByUidentcrs');
        }

        return $this;
    }

    /**
     * Use the CrsenttblRelatedByUidentcrs relation Crsenttbl object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CrsenttblQuery A secondary query class using the current class as primary query
     */
    public function useCrsenttblRelatedByUidentcrsQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCrsenttblRelatedByUidentcrs($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrsenttblRelatedByUidentcrs', '\CrsenttblQuery');
    }

    /**
     * Filter the query by a related \Users object
     *
     * @param \Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChcenttblQuery The current query, for fluid interface
     */
    public function filterByUsersRelatedByUidentusr($users, $comparison = null)
    {
        if ($users instanceof \Users) {
            return $this
                ->addUsingAlias(ChcenttblTableMap::COL_UIDENTUSR, $users->getUuid(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChcenttblTableMap::COL_UIDENTUSR, $users->toKeyValue('PrimaryKey', 'Uuid'), $comparison);
        } else {
            throw new PropelException('filterByUsersRelatedByUidentusr() only accepts arguments of type \Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsersRelatedByUidentusr relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function joinUsersRelatedByUidentusr($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsersRelatedByUidentusr');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsersRelatedByUidentusr');
        }

        return $this;
    }

    /**
     * Use the UsersRelatedByUidentusr relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsersQuery A secondary query class using the current class as primary query
     */
    public function useUsersRelatedByUidentusrQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUsersRelatedByUidentusr($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsersRelatedByUidentusr', '\UsersQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildChcenttbl $chcenttbl Object to remove from the list of results
     *
     * @return $this|ChildChcenttblQuery The current query, for fluid interface
     */
    public function prune($chcenttbl = null)
    {
        if ($chcenttbl) {
            $this->addUsingAlias(ChcenttblTableMap::COL_IDNENTCHC, $chcenttbl->getIdnentchc(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the chcenttbl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChcenttblTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ChcenttblTableMap::clearInstancePool();
            ChcenttblTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChcenttblTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ChcenttblTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ChcenttblTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ChcenttblTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ChcenttblQuery
