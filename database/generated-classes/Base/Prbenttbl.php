<?php

namespace Base;

use \Crsenttbl as ChildCrsenttbl;
use \CrsenttblQuery as ChildCrsenttblQuery;
use \PrbenttblQuery as ChildPrbenttblQuery;
use \Users as ChildUsers;
use \UsersQuery as ChildUsersQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\PrbenttblTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'prbenttbl' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Prbenttbl implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\PrbenttblTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idnentprb field.
     *
     * @var        int
     */
    protected $idnentprb;

    /**
     * The value for the uuid field.
     *
     * @var        string
     */
    protected $uuid;

    /**
     * The value for the idnentusr field.
     *
     * @var        int
     */
    protected $idnentusr;

    /**
     * The value for the uidentusr field.
     *
     * @var        string
     */
    protected $uidentusr;

    /**
     * The value for the idnentcrs field.
     *
     * @var        int
     */
    protected $idnentcrs;

    /**
     * The value for the uidentcrs field.
     *
     * @var        string
     */
    protected $uidentcrs;

    /**
     * The value for the videntprb field.
     *
     * @var        int
     */
    protected $videntprb;

    /**
     * The value for the hrrentprb field.
     *
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        DateTime
     */
    protected $hrrentprb;

    /**
     * The value for the stdentprb field.
     *
     * Note: this column has a database default value of: 'No visto'
     * @var        string
     */
    protected $stdentprb;

    /**
     * The value for the rmventprb field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $rmventprb;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * The value for the emlentprb field.
     *
     * @var        string
     */
    protected $emlentprb;

    /**
     * The value for the bndentprb field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $bndentprb;

    /**
     * The value for the vstentprb field.
     *
     * @var        string
     */
    protected $vstentprb;

    /**
     * @var        ChildCrsenttbl
     */
    protected $aCrsenttblRelatedByIdnentcrs;

    /**
     * @var        ChildUsers
     */
    protected $aUsersRelatedByIdnentusr;

    /**
     * @var        ChildCrsenttbl
     */
    protected $aCrsenttblRelatedByUidentcrs;

    /**
     * @var        ChildUsers
     */
    protected $aUsersRelatedByUidentusr;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->stdentprb = 'No visto';
        $this->rmventprb = false;
        $this->bndentprb = false;
    }

    /**
     * Initializes internal state of Base\Prbenttbl object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Prbenttbl</code> instance.  If
     * <code>obj</code> is an instance of <code>Prbenttbl</code>, delegates to
     * <code>equals(Prbenttbl)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Prbenttbl The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idnentprb] column value.
     *
     * @return int
     */
    public function getIdnentprb()
    {
        return $this->idnentprb;
    }

    /**
     * Get the [uuid] column value.
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Get the [idnentusr] column value.
     *
     * @return int
     */
    public function getIdnentusr()
    {
        return $this->idnentusr;
    }

    /**
     * Get the [uidentusr] column value.
     *
     * @return string
     */
    public function getUidentusr()
    {
        return $this->uidentusr;
    }

    /**
     * Get the [idnentcrs] column value.
     *
     * @return int
     */
    public function getIdnentcrs()
    {
        return $this->idnentcrs;
    }

    /**
     * Get the [uidentcrs] column value.
     *
     * @return string
     */
    public function getUidentcrs()
    {
        return $this->uidentcrs;
    }

    /**
     * Get the [videntprb] column value.
     *
     * @return int
     */
    public function getVidentprb()
    {
        return $this->videntprb;
    }

    /**
     * Get the [optionally formatted] temporal [hrrentprb] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getHrrentprb($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->hrrentprb;
        } else {
            return $this->hrrentprb instanceof \DateTimeInterface ? $this->hrrentprb->format($format) : null;
        }
    }

    /**
     * Get the [stdentprb] column value.
     *
     * @return string
     */
    public function getStdentprb()
    {
        return $this->stdentprb;
    }

    /**
     * Get the [rmventprb] column value.
     *
     * @return boolean
     */
    public function getRmventprb()
    {
        return $this->rmventprb;
    }

    /**
     * Get the [rmventprb] column value.
     *
     * @return boolean
     */
    public function isRmventprb()
    {
        return $this->getRmventprb();
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Get the [emlentprb] column value.
     *
     * @return string
     */
    public function getEmlentprb()
    {
        return $this->emlentprb;
    }

    /**
     * Get the [bndentprb] column value.
     *
     * @return boolean
     */
    public function getBndentprb()
    {
        return $this->bndentprb;
    }

    /**
     * Get the [bndentprb] column value.
     *
     * @return boolean
     */
    public function isBndentprb()
    {
        return $this->getBndentprb();
    }

    /**
     * Get the [vstentprb] column value.
     *
     * @return string
     */
    public function getVstentprb()
    {
        return $this->vstentprb;
    }

    /**
     * Set the value of [idnentprb] column.
     *
     * @param int $v new value
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setIdnentprb($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idnentprb !== $v) {
            $this->idnentprb = $v;
            $this->modifiedColumns[PrbenttblTableMap::COL_IDNENTPRB] = true;
        }

        return $this;
    } // setIdnentprb()

    /**
     * Set the value of [uuid] column.
     *
     * @param string $v new value
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setUuid($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uuid !== $v) {
            $this->uuid = $v;
            $this->modifiedColumns[PrbenttblTableMap::COL_UUID] = true;
        }

        return $this;
    } // setUuid()

    /**
     * Set the value of [idnentusr] column.
     *
     * @param int $v new value
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setIdnentusr($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idnentusr !== $v) {
            $this->idnentusr = $v;
            $this->modifiedColumns[PrbenttblTableMap::COL_IDNENTUSR] = true;
        }

        if ($this->aUsersRelatedByIdnentusr !== null && $this->aUsersRelatedByIdnentusr->getId() !== $v) {
            $this->aUsersRelatedByIdnentusr = null;
        }

        return $this;
    } // setIdnentusr()

    /**
     * Set the value of [uidentusr] column.
     *
     * @param string $v new value
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setUidentusr($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uidentusr !== $v) {
            $this->uidentusr = $v;
            $this->modifiedColumns[PrbenttblTableMap::COL_UIDENTUSR] = true;
        }

        if ($this->aUsersRelatedByUidentusr !== null && $this->aUsersRelatedByUidentusr->getUuid() !== $v) {
            $this->aUsersRelatedByUidentusr = null;
        }

        return $this;
    } // setUidentusr()

    /**
     * Set the value of [idnentcrs] column.
     *
     * @param int $v new value
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setIdnentcrs($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idnentcrs !== $v) {
            $this->idnentcrs = $v;
            $this->modifiedColumns[PrbenttblTableMap::COL_IDNENTCRS] = true;
        }

        if ($this->aCrsenttblRelatedByIdnentcrs !== null && $this->aCrsenttblRelatedByIdnentcrs->getIdnentcrs() !== $v) {
            $this->aCrsenttblRelatedByIdnentcrs = null;
        }

        return $this;
    } // setIdnentcrs()

    /**
     * Set the value of [uidentcrs] column.
     *
     * @param string $v new value
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setUidentcrs($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uidentcrs !== $v) {
            $this->uidentcrs = $v;
            $this->modifiedColumns[PrbenttblTableMap::COL_UIDENTCRS] = true;
        }

        if ($this->aCrsenttblRelatedByUidentcrs !== null && $this->aCrsenttblRelatedByUidentcrs->getUuid() !== $v) {
            $this->aCrsenttblRelatedByUidentcrs = null;
        }

        return $this;
    } // setUidentcrs()

    /**
     * Set the value of [videntprb] column.
     *
     * @param int $v new value
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setVidentprb($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->videntprb !== $v) {
            $this->videntprb = $v;
            $this->modifiedColumns[PrbenttblTableMap::COL_VIDENTPRB] = true;
        }

        return $this;
    } // setVidentprb()

    /**
     * Sets the value of [hrrentprb] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setHrrentprb($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->hrrentprb !== null || $dt !== null) {
            if ($this->hrrentprb === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->hrrentprb->format("Y-m-d H:i:s.u")) {
                $this->hrrentprb = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PrbenttblTableMap::COL_HRRENTPRB] = true;
            }
        } // if either are not null

        return $this;
    } // setHrrentprb()

    /**
     * Set the value of [stdentprb] column.
     *
     * @param string $v new value
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setStdentprb($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->stdentprb !== $v) {
            $this->stdentprb = $v;
            $this->modifiedColumns[PrbenttblTableMap::COL_STDENTPRB] = true;
        }

        return $this;
    } // setStdentprb()

    /**
     * Sets the value of the [rmventprb] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setRmventprb($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->rmventprb !== $v) {
            $this->rmventprb = $v;
            $this->modifiedColumns[PrbenttblTableMap::COL_RMVENTPRB] = true;
        }

        return $this;
    } // setRmventprb()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PrbenttblTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PrbenttblTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Set the value of [emlentprb] column.
     *
     * @param string $v new value
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setEmlentprb($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->emlentprb !== $v) {
            $this->emlentprb = $v;
            $this->modifiedColumns[PrbenttblTableMap::COL_EMLENTPRB] = true;
        }

        return $this;
    } // setEmlentprb()

    /**
     * Sets the value of the [bndentprb] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setBndentprb($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->bndentprb !== $v) {
            $this->bndentprb = $v;
            $this->modifiedColumns[PrbenttblTableMap::COL_BNDENTPRB] = true;
        }

        return $this;
    } // setBndentprb()

    /**
     * Set the value of [vstentprb] column.
     *
     * @param string $v new value
     * @return $this|\Prbenttbl The current object (for fluent API support)
     */
    public function setVstentprb($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vstentprb !== $v) {
            $this->vstentprb = $v;
            $this->modifiedColumns[PrbenttblTableMap::COL_VSTENTPRB] = true;
        }

        return $this;
    } // setVstentprb()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->stdentprb !== 'No visto') {
                return false;
            }

            if ($this->rmventprb !== false) {
                return false;
            }

            if ($this->bndentprb !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PrbenttblTableMap::translateFieldName('Idnentprb', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idnentprb = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PrbenttblTableMap::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uuid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PrbenttblTableMap::translateFieldName('Idnentusr', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idnentusr = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : PrbenttblTableMap::translateFieldName('Uidentusr', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uidentusr = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : PrbenttblTableMap::translateFieldName('Idnentcrs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idnentcrs = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : PrbenttblTableMap::translateFieldName('Uidentcrs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uidentcrs = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : PrbenttblTableMap::translateFieldName('Videntprb', TableMap::TYPE_PHPNAME, $indexType)];
            $this->videntprb = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : PrbenttblTableMap::translateFieldName('Hrrentprb', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->hrrentprb = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : PrbenttblTableMap::translateFieldName('Stdentprb', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stdentprb = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : PrbenttblTableMap::translateFieldName('Rmventprb', TableMap::TYPE_PHPNAME, $indexType)];
            $this->rmventprb = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : PrbenttblTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : PrbenttblTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : PrbenttblTableMap::translateFieldName('Emlentprb', TableMap::TYPE_PHPNAME, $indexType)];
            $this->emlentprb = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : PrbenttblTableMap::translateFieldName('Bndentprb', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bndentprb = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : PrbenttblTableMap::translateFieldName('Vstentprb', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vstentprb = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 15; // 15 = PrbenttblTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Prbenttbl'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aUsersRelatedByIdnentusr !== null && $this->idnentusr !== $this->aUsersRelatedByIdnentusr->getId()) {
            $this->aUsersRelatedByIdnentusr = null;
        }
        if ($this->aUsersRelatedByUidentusr !== null && $this->uidentusr !== $this->aUsersRelatedByUidentusr->getUuid()) {
            $this->aUsersRelatedByUidentusr = null;
        }
        if ($this->aCrsenttblRelatedByIdnentcrs !== null && $this->idnentcrs !== $this->aCrsenttblRelatedByIdnentcrs->getIdnentcrs()) {
            $this->aCrsenttblRelatedByIdnentcrs = null;
        }
        if ($this->aCrsenttblRelatedByUidentcrs !== null && $this->uidentcrs !== $this->aCrsenttblRelatedByUidentcrs->getUuid()) {
            $this->aCrsenttblRelatedByUidentcrs = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PrbenttblTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPrbenttblQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCrsenttblRelatedByIdnentcrs = null;
            $this->aUsersRelatedByIdnentusr = null;
            $this->aCrsenttblRelatedByUidentcrs = null;
            $this->aUsersRelatedByUidentusr = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Prbenttbl::setDeleted()
     * @see Prbenttbl::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrbenttblTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPrbenttblQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrbenttblTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PrbenttblTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCrsenttblRelatedByIdnentcrs !== null) {
                if ($this->aCrsenttblRelatedByIdnentcrs->isModified() || $this->aCrsenttblRelatedByIdnentcrs->isNew()) {
                    $affectedRows += $this->aCrsenttblRelatedByIdnentcrs->save($con);
                }
                $this->setCrsenttblRelatedByIdnentcrs($this->aCrsenttblRelatedByIdnentcrs);
            }

            if ($this->aUsersRelatedByIdnentusr !== null) {
                if ($this->aUsersRelatedByIdnentusr->isModified() || $this->aUsersRelatedByIdnentusr->isNew()) {
                    $affectedRows += $this->aUsersRelatedByIdnentusr->save($con);
                }
                $this->setUsersRelatedByIdnentusr($this->aUsersRelatedByIdnentusr);
            }

            if ($this->aCrsenttblRelatedByUidentcrs !== null) {
                if ($this->aCrsenttblRelatedByUidentcrs->isModified() || $this->aCrsenttblRelatedByUidentcrs->isNew()) {
                    $affectedRows += $this->aCrsenttblRelatedByUidentcrs->save($con);
                }
                $this->setCrsenttblRelatedByUidentcrs($this->aCrsenttblRelatedByUidentcrs);
            }

            if ($this->aUsersRelatedByUidentusr !== null) {
                if ($this->aUsersRelatedByUidentusr->isModified() || $this->aUsersRelatedByUidentusr->isNew()) {
                    $affectedRows += $this->aUsersRelatedByUidentusr->save($con);
                }
                $this->setUsersRelatedByUidentusr($this->aUsersRelatedByUidentusr);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[PrbenttblTableMap::COL_IDNENTPRB] = true;
        if (null !== $this->idnentprb) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PrbenttblTableMap::COL_IDNENTPRB . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PrbenttblTableMap::COL_IDNENTPRB)) {
            $modifiedColumns[':p' . $index++]  = 'idnentprb';
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_UUID)) {
            $modifiedColumns[':p' . $index++]  = 'uuid';
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_IDNENTUSR)) {
            $modifiedColumns[':p' . $index++]  = 'idnentusr';
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_UIDENTUSR)) {
            $modifiedColumns[':p' . $index++]  = 'uidentusr';
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_IDNENTCRS)) {
            $modifiedColumns[':p' . $index++]  = 'idnentcrs';
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_UIDENTCRS)) {
            $modifiedColumns[':p' . $index++]  = 'uidentcrs';
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_VIDENTPRB)) {
            $modifiedColumns[':p' . $index++]  = 'videntprb';
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_HRRENTPRB)) {
            $modifiedColumns[':p' . $index++]  = 'hrrentprb';
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_STDENTPRB)) {
            $modifiedColumns[':p' . $index++]  = 'stdentprb';
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_RMVENTPRB)) {
            $modifiedColumns[':p' . $index++]  = 'rmventprb';
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_EMLENTPRB)) {
            $modifiedColumns[':p' . $index++]  = 'emlentprb';
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_BNDENTPRB)) {
            $modifiedColumns[':p' . $index++]  = 'bndentprb';
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_VSTENTPRB)) {
            $modifiedColumns[':p' . $index++]  = 'vstentprb';
        }

        $sql = sprintf(
            'INSERT INTO prbenttbl (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idnentprb':
                        $stmt->bindValue($identifier, $this->idnentprb, PDO::PARAM_INT);
                        break;
                    case 'uuid':
                        $stmt->bindValue($identifier, $this->uuid, PDO::PARAM_STR);
                        break;
                    case 'idnentusr':
                        $stmt->bindValue($identifier, $this->idnentusr, PDO::PARAM_INT);
                        break;
                    case 'uidentusr':
                        $stmt->bindValue($identifier, $this->uidentusr, PDO::PARAM_STR);
                        break;
                    case 'idnentcrs':
                        $stmt->bindValue($identifier, $this->idnentcrs, PDO::PARAM_INT);
                        break;
                    case 'uidentcrs':
                        $stmt->bindValue($identifier, $this->uidentcrs, PDO::PARAM_STR);
                        break;
                    case 'videntprb':
                        $stmt->bindValue($identifier, $this->videntprb, PDO::PARAM_INT);
                        break;
                    case 'hrrentprb':
                        $stmt->bindValue($identifier, $this->hrrentprb ? $this->hrrentprb->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'stdentprb':
                        $stmt->bindValue($identifier, $this->stdentprb, PDO::PARAM_STR);
                        break;
                    case 'rmventprb':
                        $stmt->bindValue($identifier, (int) $this->rmventprb, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'emlentprb':
                        $stmt->bindValue($identifier, $this->emlentprb, PDO::PARAM_STR);
                        break;
                    case 'bndentprb':
                        $stmt->bindValue($identifier, (int) $this->bndentprb, PDO::PARAM_INT);
                        break;
                    case 'vstentprb':
                        $stmt->bindValue($identifier, $this->vstentprb, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdnentprb($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PrbenttblTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdnentprb();
                break;
            case 1:
                return $this->getUuid();
                break;
            case 2:
                return $this->getIdnentusr();
                break;
            case 3:
                return $this->getUidentusr();
                break;
            case 4:
                return $this->getIdnentcrs();
                break;
            case 5:
                return $this->getUidentcrs();
                break;
            case 6:
                return $this->getVidentprb();
                break;
            case 7:
                return $this->getHrrentprb();
                break;
            case 8:
                return $this->getStdentprb();
                break;
            case 9:
                return $this->getRmventprb();
                break;
            case 10:
                return $this->getCreatedAt();
                break;
            case 11:
                return $this->getUpdatedAt();
                break;
            case 12:
                return $this->getEmlentprb();
                break;
            case 13:
                return $this->getBndentprb();
                break;
            case 14:
                return $this->getVstentprb();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Prbenttbl'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Prbenttbl'][$this->hashCode()] = true;
        $keys = PrbenttblTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdnentprb(),
            $keys[1] => $this->getUuid(),
            $keys[2] => $this->getIdnentusr(),
            $keys[3] => $this->getUidentusr(),
            $keys[4] => $this->getIdnentcrs(),
            $keys[5] => $this->getUidentcrs(),
            $keys[6] => $this->getVidentprb(),
            $keys[7] => $this->getHrrentprb(),
            $keys[8] => $this->getStdentprb(),
            $keys[9] => $this->getRmventprb(),
            $keys[10] => $this->getCreatedAt(),
            $keys[11] => $this->getUpdatedAt(),
            $keys[12] => $this->getEmlentprb(),
            $keys[13] => $this->getBndentprb(),
            $keys[14] => $this->getVstentprb(),
        );
        if ($result[$keys[7]] instanceof \DateTimeInterface) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCrsenttblRelatedByIdnentcrs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'crsenttbl';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'crsenttbl';
                        break;
                    default:
                        $key = 'Crsenttbl';
                }

                $result[$key] = $this->aCrsenttblRelatedByIdnentcrs->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsersRelatedByIdnentusr) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'users';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'users';
                        break;
                    default:
                        $key = 'Users';
                }

                $result[$key] = $this->aUsersRelatedByIdnentusr->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCrsenttblRelatedByUidentcrs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'crsenttbl';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'crsenttbl';
                        break;
                    default:
                        $key = 'Crsenttbl';
                }

                $result[$key] = $this->aCrsenttblRelatedByUidentcrs->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsersRelatedByUidentusr) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'users';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'users';
                        break;
                    default:
                        $key = 'Users';
                }

                $result[$key] = $this->aUsersRelatedByUidentusr->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Prbenttbl
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PrbenttblTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Prbenttbl
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdnentprb($value);
                break;
            case 1:
                $this->setUuid($value);
                break;
            case 2:
                $this->setIdnentusr($value);
                break;
            case 3:
                $this->setUidentusr($value);
                break;
            case 4:
                $this->setIdnentcrs($value);
                break;
            case 5:
                $this->setUidentcrs($value);
                break;
            case 6:
                $this->setVidentprb($value);
                break;
            case 7:
                $this->setHrrentprb($value);
                break;
            case 8:
                $this->setStdentprb($value);
                break;
            case 9:
                $this->setRmventprb($value);
                break;
            case 10:
                $this->setCreatedAt($value);
                break;
            case 11:
                $this->setUpdatedAt($value);
                break;
            case 12:
                $this->setEmlentprb($value);
                break;
            case 13:
                $this->setBndentprb($value);
                break;
            case 14:
                $this->setVstentprb($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = PrbenttblTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdnentprb($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUuid($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setIdnentusr($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setUidentusr($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setIdnentcrs($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setUidentcrs($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setVidentprb($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setHrrentprb($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setStdentprb($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setRmventprb($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCreatedAt($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setUpdatedAt($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setEmlentprb($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setBndentprb($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setVstentprb($arr[$keys[14]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Prbenttbl The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PrbenttblTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PrbenttblTableMap::COL_IDNENTPRB)) {
            $criteria->add(PrbenttblTableMap::COL_IDNENTPRB, $this->idnentprb);
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_UUID)) {
            $criteria->add(PrbenttblTableMap::COL_UUID, $this->uuid);
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_IDNENTUSR)) {
            $criteria->add(PrbenttblTableMap::COL_IDNENTUSR, $this->idnentusr);
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_UIDENTUSR)) {
            $criteria->add(PrbenttblTableMap::COL_UIDENTUSR, $this->uidentusr);
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_IDNENTCRS)) {
            $criteria->add(PrbenttblTableMap::COL_IDNENTCRS, $this->idnentcrs);
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_UIDENTCRS)) {
            $criteria->add(PrbenttblTableMap::COL_UIDENTCRS, $this->uidentcrs);
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_VIDENTPRB)) {
            $criteria->add(PrbenttblTableMap::COL_VIDENTPRB, $this->videntprb);
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_HRRENTPRB)) {
            $criteria->add(PrbenttblTableMap::COL_HRRENTPRB, $this->hrrentprb);
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_STDENTPRB)) {
            $criteria->add(PrbenttblTableMap::COL_STDENTPRB, $this->stdentprb);
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_RMVENTPRB)) {
            $criteria->add(PrbenttblTableMap::COL_RMVENTPRB, $this->rmventprb);
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_CREATED_AT)) {
            $criteria->add(PrbenttblTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_UPDATED_AT)) {
            $criteria->add(PrbenttblTableMap::COL_UPDATED_AT, $this->updated_at);
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_EMLENTPRB)) {
            $criteria->add(PrbenttblTableMap::COL_EMLENTPRB, $this->emlentprb);
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_BNDENTPRB)) {
            $criteria->add(PrbenttblTableMap::COL_BNDENTPRB, $this->bndentprb);
        }
        if ($this->isColumnModified(PrbenttblTableMap::COL_VSTENTPRB)) {
            $criteria->add(PrbenttblTableMap::COL_VSTENTPRB, $this->vstentprb);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildPrbenttblQuery::create();
        $criteria->add(PrbenttblTableMap::COL_IDNENTPRB, $this->idnentprb);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdnentprb();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdnentprb();
    }

    /**
     * Generic method to set the primary key (idnentprb column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdnentprb($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdnentprb();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Prbenttbl (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUuid($this->getUuid());
        $copyObj->setIdnentusr($this->getIdnentusr());
        $copyObj->setUidentusr($this->getUidentusr());
        $copyObj->setIdnentcrs($this->getIdnentcrs());
        $copyObj->setUidentcrs($this->getUidentcrs());
        $copyObj->setVidentprb($this->getVidentprb());
        $copyObj->setHrrentprb($this->getHrrentprb());
        $copyObj->setStdentprb($this->getStdentprb());
        $copyObj->setRmventprb($this->getRmventprb());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setEmlentprb($this->getEmlentprb());
        $copyObj->setBndentprb($this->getBndentprb());
        $copyObj->setVstentprb($this->getVstentprb());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdnentprb(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Prbenttbl Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildCrsenttbl object.
     *
     * @param  ChildCrsenttbl $v
     * @return $this|\Prbenttbl The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCrsenttblRelatedByIdnentcrs(ChildCrsenttbl $v = null)
    {
        if ($v === null) {
            $this->setIdnentcrs(NULL);
        } else {
            $this->setIdnentcrs($v->getIdnentcrs());
        }

        $this->aCrsenttblRelatedByIdnentcrs = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCrsenttbl object, it will not be re-added.
        if ($v !== null) {
            $v->addPrbenttblRelatedByIdnentcrs($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCrsenttbl object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCrsenttbl The associated ChildCrsenttbl object.
     * @throws PropelException
     */
    public function getCrsenttblRelatedByIdnentcrs(ConnectionInterface $con = null)
    {
        if ($this->aCrsenttblRelatedByIdnentcrs === null && ($this->idnentcrs != 0)) {
            $this->aCrsenttblRelatedByIdnentcrs = ChildCrsenttblQuery::create()->findPk($this->idnentcrs, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCrsenttblRelatedByIdnentcrs->addPrbenttblsRelatedByIdnentcrs($this);
             */
        }

        return $this->aCrsenttblRelatedByIdnentcrs;
    }

    /**
     * Declares an association between this object and a ChildUsers object.
     *
     * @param  ChildUsers $v
     * @return $this|\Prbenttbl The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsersRelatedByIdnentusr(ChildUsers $v = null)
    {
        if ($v === null) {
            $this->setIdnentusr(NULL);
        } else {
            $this->setIdnentusr($v->getId());
        }

        $this->aUsersRelatedByIdnentusr = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsers object, it will not be re-added.
        if ($v !== null) {
            $v->addPrbenttblRelatedByIdnentusr($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsers object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsers The associated ChildUsers object.
     * @throws PropelException
     */
    public function getUsersRelatedByIdnentusr(ConnectionInterface $con = null)
    {
        if ($this->aUsersRelatedByIdnentusr === null && ($this->idnentusr != 0)) {
            $this->aUsersRelatedByIdnentusr = ChildUsersQuery::create()->findPk($this->idnentusr, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsersRelatedByIdnentusr->addPrbenttblsRelatedByIdnentusr($this);
             */
        }

        return $this->aUsersRelatedByIdnentusr;
    }

    /**
     * Declares an association between this object and a ChildCrsenttbl object.
     *
     * @param  ChildCrsenttbl $v
     * @return $this|\Prbenttbl The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCrsenttblRelatedByUidentcrs(ChildCrsenttbl $v = null)
    {
        if ($v === null) {
            $this->setUidentcrs(NULL);
        } else {
            $this->setUidentcrs($v->getUuid());
        }

        $this->aCrsenttblRelatedByUidentcrs = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCrsenttbl object, it will not be re-added.
        if ($v !== null) {
            $v->addPrbenttblRelatedByUidentcrs($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCrsenttbl object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCrsenttbl The associated ChildCrsenttbl object.
     * @throws PropelException
     */
    public function getCrsenttblRelatedByUidentcrs(ConnectionInterface $con = null)
    {
        if ($this->aCrsenttblRelatedByUidentcrs === null && (($this->uidentcrs !== "" && $this->uidentcrs !== null))) {
            $this->aCrsenttblRelatedByUidentcrs = ChildCrsenttblQuery::create()
                ->filterByPrbenttblRelatedByUidentcrs($this) // here
                ->findOne($con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCrsenttblRelatedByUidentcrs->addPrbenttblsRelatedByUidentcrs($this);
             */
        }

        return $this->aCrsenttblRelatedByUidentcrs;
    }

    /**
     * Declares an association between this object and a ChildUsers object.
     *
     * @param  ChildUsers $v
     * @return $this|\Prbenttbl The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsersRelatedByUidentusr(ChildUsers $v = null)
    {
        if ($v === null) {
            $this->setUidentusr(NULL);
        } else {
            $this->setUidentusr($v->getUuid());
        }

        $this->aUsersRelatedByUidentusr = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsers object, it will not be re-added.
        if ($v !== null) {
            $v->addPrbenttblRelatedByUidentusr($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsers object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsers The associated ChildUsers object.
     * @throws PropelException
     */
    public function getUsersRelatedByUidentusr(ConnectionInterface $con = null)
    {
        if ($this->aUsersRelatedByUidentusr === null && (($this->uidentusr !== "" && $this->uidentusr !== null))) {
            $this->aUsersRelatedByUidentusr = ChildUsersQuery::create()
                ->filterByPrbenttblRelatedByUidentusr($this) // here
                ->findOne($con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsersRelatedByUidentusr->addPrbenttblsRelatedByUidentusr($this);
             */
        }

        return $this->aUsersRelatedByUidentusr;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCrsenttblRelatedByIdnentcrs) {
            $this->aCrsenttblRelatedByIdnentcrs->removePrbenttblRelatedByIdnentcrs($this);
        }
        if (null !== $this->aUsersRelatedByIdnentusr) {
            $this->aUsersRelatedByIdnentusr->removePrbenttblRelatedByIdnentusr($this);
        }
        if (null !== $this->aCrsenttblRelatedByUidentcrs) {
            $this->aCrsenttblRelatedByUidentcrs->removePrbenttblRelatedByUidentcrs($this);
        }
        if (null !== $this->aUsersRelatedByUidentusr) {
            $this->aUsersRelatedByUidentusr->removePrbenttblRelatedByUidentusr($this);
        }
        $this->idnentprb = null;
        $this->uuid = null;
        $this->idnentusr = null;
        $this->uidentusr = null;
        $this->idnentcrs = null;
        $this->uidentcrs = null;
        $this->videntprb = null;
        $this->hrrentprb = null;
        $this->stdentprb = null;
        $this->rmventprb = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->emlentprb = null;
        $this->bndentprb = null;
        $this->vstentprb = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aCrsenttblRelatedByIdnentcrs = null;
        $this->aUsersRelatedByIdnentusr = null;
        $this->aCrsenttblRelatedByUidentcrs = null;
        $this->aUsersRelatedByUidentusr = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PrbenttblTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
