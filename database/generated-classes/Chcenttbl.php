<?php

use Base\Chcenttbl as BaseChcenttbl;
use Illuminate\Support\Facades\Log;


class Chcenttbl extends BaseChcenttbl
{
    public static function crtchcenttbl(array $data, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $chc = new \Chcenttbl();
        try {
            if (array_key_exists('uuid', $data)) {
                if (!is_null($data['uuid'])) {
                    $chc->setUuid($data['uuid']);
                } else {
                    throw new \Propel\Runtime\Exception\PropelException('uuid cannot be null');
                }
            }
            if (array_key_exists('idnentusr', $data)) {
                if (!is_null($data['idnentusr'])) {
                    $chc->setIdnentusr($data['idnentusr']);
                }
            }
            if (array_key_exists('uidentusr', $data)) {
                if (!is_null($data['uidentusr'])) {
                    $chc->setUidentusr($data['uidentusr']);
                }
            }
            if (array_key_exists('idnentcrs', $data)) {
                if (!is_null($data['idnentcrs'])) {
                    $chc->setIdnentcrs($data['idnentcrs']);
                }
            }
            if (array_key_exists('uidentcrs', $data)) {
                if (!is_null($data['uidentcrs'])) {
                    $chc->setUidentcrs($data['uidentcrs']);
                }
            }
            if (array_key_exists('rmventchc', $data)) {
                if (!is_null($data['rmventchc'])) {
                    $chc->setRmventchc($data['rmventchc']);
                } else {
                    throw new \Propel\Runtime\Exception\PropelException('rmventchc cannot be null');
                }
            }
            if (array_key_exists('created_at', $data)) {
                if (!is_null($data['created_at'])) {
                    $chc->setCreatedAt($data['created_at']);
                }
            }
            if (array_key_exists('updated_at', $data)) {
                if (!is_null($data['updated_at'])) {
                    $chc->setUpdatedAt($data['updated_at']);
                }
            }
            $chc->save($connection);
        } catch (\Propel\Runtime\Exception\PropelException $e) {
            Illuminate\Support\Facades\Log::debug($e);
            return false;
        }
        return $chc;
    }

    public static function rmvchcenttbl($idnentchc, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $chc = \ChcenttblQuery::create()
            ->filterByIdnentchc($idnentchc)
            ->findOne($connection);

        if (!$chc) return false;

        try {
            $chc->delete($connection);
        } catch (\Propel\Runtime\Exception\PropelException $e) {
            Illuminate\Support\Facades\Log::debug($e);
            return false;
        }

        return true;
    }

    public static function updchcenttbl(array $data, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $chc = \ChcenttblQuery::create()
            ->filterByIdnentchc($data['idnentchc'])
            ->findOne($connection);

        if (!$chc) return false;

        try {
            if (array_key_exists('uuid', $data)) {
                if (!is_null($data['uuid'])) {
                    $chc->setUuid($data['uuid']);
                } else {
                    throw new \Propel\Runtime\Exception\PropelException('uuid cannot be null');
                }
            }
            $chc->setIdnentusr(array_key_exists('idnentusr', $data) ? $data['idnentusr'] : null);
            $chc->setUidentusr(array_key_exists('uidentusr', $data) ? $data['uidentusr'] : null);
            $chc->setIdnentcrs(array_key_exists('idnentcrs', $data) ? $data['idnentcrs'] : null);
            $chc->setUidentcrs(array_key_exists('uidentcrs', $data) ? $data['uidentcrs'] : null);
            if (array_key_exists('rmventchc', $data)) {
                if (!is_null($data['rmventchc'])) {
                    $chc->setRmventchc($data['rmventchc']);
                } else {
                    throw new \Propel\Runtime\Exception\PropelException('rmventchc cannot be null');
                }
            }
            $chc->setCreatedAt(array_key_exists('created_at', $data) ? $data['created_at'] : null);
            $chc->setUpdatedAt(array_key_exists('updated_at', $data) ? $data['updated_at'] : null);
            $chc->save($connection);
        } catch (\Propel\Runtime\Exception\PropelException $e) {
            Illuminate\Support\Facades\Log::debug($e);
            return false;
        }
        return $chc;
    }

    public static function dsplstchcenttbl(\Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $allchc = \ChcenttblQuery::create();
        $allchc = $allchc
            ->select(array('uuid'))
            ->withColumn('uuid', 'label')
            ->withColumn('uuid', 'value')
            ->find();

        if (!$allchc) return false;

        return $allchc;
    }
    public static function dspchcenttbl($filidnentcrs, $filidnentusr, $filuidentcrs, $filuidentusr, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $allchc = \ChcenttblQuery::create();
        if ($filidnentcrs != 0) {
            $allchc = $allchc->filterByIdnentcrs($filidnentcrs);
        }
        if ($filidnentusr != 0) {
            $allchc = $allchc->filterByIdnentusr($filidnentusr);
        }
        if ($filuidentcrs != 0) {
            $allchc = $allchc->filterByUidentcrs($filuidentcrs);
        }
        if ($filuidentusr != 0) {
            $allchc = $allchc->filterByUidentusr($filuidentusr);
        }

        $allchc = $allchc
            ->useCrsenttblRelatedByUidentcrsQuery()
            ->withColumn('nmbentcrs', 'NombreCurso')
            ->enduse()
            ->useUsersRelatedByUidentusrQuery()
            ->withColumn('email', 'EmailUsuario')
            ->enduse()
            ->find();

        if (!$allchc) return false;

        return $allchc;
    }

    public static function fnochcenttbl($idnentchc, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $chc = \ChcenttblQuery::create()
            ->filterByIdnentchc($idnentchc)
            ->findOne($connection);

        if (!$chc) return false;

        return $chc;
    }

    public static function fnuchcenttbl($uuid, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $chc = \ChcenttblQuery::create()
            ->filterByUuid($uuid)
            ->findOne($connection);

        if (!$chc) return false;

        return $chc;
    }

    public static function fnfchcenttbl($Uidentusr, $Uidentcrs, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $chc = \ChcenttblQuery::create()
            ->filterByArray(["uidentusr" => $Uidentusr, "uidentcrs" => $Uidentcrs])
            ->findOne($connection);

        if (!$chc) return false;

        return $chc;
    }

    public static function fndusrcrs($id, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $chc = \ChcenttblQuery::create()
            ->filterByIdnentusr($id)
            ->find($connection);

        if (!$chc) return false;

        return $chc;
    }

    public static function fnousrenttbl($idnentcrs, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $crs = \ChcenttblQuery::create()
            ->filterByIdnentcrs($idnentcrs)
            ->useCrsenttblRelatedByIdnentcrsQuery()
            ->get("Idnentcrs")
            ->get("Nmbentcrs")
            ->get("Dscentcrs")
            ->endUse()
            ->findOne($connection);

        if (!$crs) return false;

        return $crs;
    }
}
    //TODO *CRUD Generator control separator line* (Don't remove this line!)
