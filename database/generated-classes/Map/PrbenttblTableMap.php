<?php

namespace Map;

use \Prbenttbl;
use \PrbenttblQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'prbenttbl' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PrbenttblTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PrbenttblTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'empconpro';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'prbenttbl';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Prbenttbl';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Prbenttbl';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 15;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 15;

    /**
     * the column name for the idnentprb field
     */
    const COL_IDNENTPRB = 'prbenttbl.idnentprb';

    /**
     * the column name for the uuid field
     */
    const COL_UUID = 'prbenttbl.uuid';

    /**
     * the column name for the idnentusr field
     */
    const COL_IDNENTUSR = 'prbenttbl.idnentusr';

    /**
     * the column name for the uidentusr field
     */
    const COL_UIDENTUSR = 'prbenttbl.uidentusr';

    /**
     * the column name for the idnentcrs field
     */
    const COL_IDNENTCRS = 'prbenttbl.idnentcrs';

    /**
     * the column name for the uidentcrs field
     */
    const COL_UIDENTCRS = 'prbenttbl.uidentcrs';

    /**
     * the column name for the videntprb field
     */
    const COL_VIDENTPRB = 'prbenttbl.videntprb';

    /**
     * the column name for the hrrentprb field
     */
    const COL_HRRENTPRB = 'prbenttbl.hrrentprb';

    /**
     * the column name for the stdentprb field
     */
    const COL_STDENTPRB = 'prbenttbl.stdentprb';

    /**
     * the column name for the rmventprb field
     */
    const COL_RMVENTPRB = 'prbenttbl.rmventprb';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'prbenttbl.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'prbenttbl.updated_at';

    /**
     * the column name for the emlentprb field
     */
    const COL_EMLENTPRB = 'prbenttbl.emlentprb';

    /**
     * the column name for the bndentprb field
     */
    const COL_BNDENTPRB = 'prbenttbl.bndentprb';

    /**
     * the column name for the vstentprb field
     */
    const COL_VSTENTPRB = 'prbenttbl.vstentprb';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idnentprb', 'Uuid', 'Idnentusr', 'Uidentusr', 'Idnentcrs', 'Uidentcrs', 'Videntprb', 'Hrrentprb', 'Stdentprb', 'Rmventprb', 'CreatedAt', 'UpdatedAt', 'Emlentprb', 'Bndentprb', 'Vstentprb', ),
        self::TYPE_CAMELNAME     => array('idnentprb', 'uuid', 'idnentusr', 'uidentusr', 'idnentcrs', 'uidentcrs', 'videntprb', 'hrrentprb', 'stdentprb', 'rmventprb', 'createdAt', 'updatedAt', 'emlentprb', 'bndentprb', 'vstentprb', ),
        self::TYPE_COLNAME       => array(PrbenttblTableMap::COL_IDNENTPRB, PrbenttblTableMap::COL_UUID, PrbenttblTableMap::COL_IDNENTUSR, PrbenttblTableMap::COL_UIDENTUSR, PrbenttblTableMap::COL_IDNENTCRS, PrbenttblTableMap::COL_UIDENTCRS, PrbenttblTableMap::COL_VIDENTPRB, PrbenttblTableMap::COL_HRRENTPRB, PrbenttblTableMap::COL_STDENTPRB, PrbenttblTableMap::COL_RMVENTPRB, PrbenttblTableMap::COL_CREATED_AT, PrbenttblTableMap::COL_UPDATED_AT, PrbenttblTableMap::COL_EMLENTPRB, PrbenttblTableMap::COL_BNDENTPRB, PrbenttblTableMap::COL_VSTENTPRB, ),
        self::TYPE_FIELDNAME     => array('idnentprb', 'uuid', 'idnentusr', 'uidentusr', 'idnentcrs', 'uidentcrs', 'videntprb', 'hrrentprb', 'stdentprb', 'rmventprb', 'created_at', 'updated_at', 'emlentprb', 'bndentprb', 'vstentprb', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idnentprb' => 0, 'Uuid' => 1, 'Idnentusr' => 2, 'Uidentusr' => 3, 'Idnentcrs' => 4, 'Uidentcrs' => 5, 'Videntprb' => 6, 'Hrrentprb' => 7, 'Stdentprb' => 8, 'Rmventprb' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, 'Emlentprb' => 12, 'Bndentprb' => 13, 'Vstentprb' => 14, ),
        self::TYPE_CAMELNAME     => array('idnentprb' => 0, 'uuid' => 1, 'idnentusr' => 2, 'uidentusr' => 3, 'idnentcrs' => 4, 'uidentcrs' => 5, 'videntprb' => 6, 'hrrentprb' => 7, 'stdentprb' => 8, 'rmventprb' => 9, 'createdAt' => 10, 'updatedAt' => 11, 'emlentprb' => 12, 'bndentprb' => 13, 'vstentprb' => 14, ),
        self::TYPE_COLNAME       => array(PrbenttblTableMap::COL_IDNENTPRB => 0, PrbenttblTableMap::COL_UUID => 1, PrbenttblTableMap::COL_IDNENTUSR => 2, PrbenttblTableMap::COL_UIDENTUSR => 3, PrbenttblTableMap::COL_IDNENTCRS => 4, PrbenttblTableMap::COL_UIDENTCRS => 5, PrbenttblTableMap::COL_VIDENTPRB => 6, PrbenttblTableMap::COL_HRRENTPRB => 7, PrbenttblTableMap::COL_STDENTPRB => 8, PrbenttblTableMap::COL_RMVENTPRB => 9, PrbenttblTableMap::COL_CREATED_AT => 10, PrbenttblTableMap::COL_UPDATED_AT => 11, PrbenttblTableMap::COL_EMLENTPRB => 12, PrbenttblTableMap::COL_BNDENTPRB => 13, PrbenttblTableMap::COL_VSTENTPRB => 14, ),
        self::TYPE_FIELDNAME     => array('idnentprb' => 0, 'uuid' => 1, 'idnentusr' => 2, 'uidentusr' => 3, 'idnentcrs' => 4, 'uidentcrs' => 5, 'videntprb' => 6, 'hrrentprb' => 7, 'stdentprb' => 8, 'rmventprb' => 9, 'created_at' => 10, 'updated_at' => 11, 'emlentprb' => 12, 'bndentprb' => 13, 'vstentprb' => 14, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('prbenttbl');
        $this->setPhpName('Prbenttbl');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Prbenttbl');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idnentprb', 'Idnentprb', 'INTEGER', true, 10, null);
        $this->addColumn('uuid', 'Uuid', 'CHAR', true, 36, null);
        $this->addForeignKey('idnentusr', 'Idnentusr', 'INTEGER', 'users', 'id', false, 10, null);
        $this->addForeignKey('uidentusr', 'Uidentusr', 'CHAR', 'users', 'uuid', false, 36, null);
        $this->addForeignKey('idnentcrs', 'Idnentcrs', 'INTEGER', 'crsenttbl', 'idnentcrs', false, 10, null);
        $this->addForeignKey('uidentcrs', 'Uidentcrs', 'CHAR', 'crsenttbl', 'uuid', false, 36, null);
        $this->addColumn('videntprb', 'Videntprb', 'INTEGER', true, null, null);
        $this->addColumn('hrrentprb', 'Hrrentprb', 'TIMESTAMP', false, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('stdentprb', 'Stdentprb', 'VARCHAR', false, 255, 'No visto');
        $this->addColumn('rmventprb', 'Rmventprb', 'BOOLEAN', true, 1, false);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('emlentprb', 'Emlentprb', 'VARCHAR', false, 255, null);
        $this->addColumn('bndentprb', 'Bndentprb', 'BOOLEAN', true, 1, false);
        $this->addColumn('vstentprb', 'Vstentprb', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CrsenttblRelatedByIdnentcrs', '\\Crsenttbl', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':idnentcrs',
    1 => ':idnentcrs',
  ),
), null, null, null, false);
        $this->addRelation('UsersRelatedByIdnentusr', '\\Users', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':idnentusr',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('CrsenttblRelatedByUidentcrs', '\\Crsenttbl', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':uidentcrs',
    1 => ':uuid',
  ),
), null, null, null, false);
        $this->addRelation('UsersRelatedByUidentusr', '\\Users', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':uidentusr',
    1 => ':uuid',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idnentprb', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idnentprb', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idnentprb', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idnentprb', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idnentprb', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idnentprb', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idnentprb', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PrbenttblTableMap::CLASS_DEFAULT : PrbenttblTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Prbenttbl object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PrbenttblTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PrbenttblTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PrbenttblTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PrbenttblTableMap::OM_CLASS;
            /** @var Prbenttbl $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PrbenttblTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PrbenttblTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PrbenttblTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Prbenttbl $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PrbenttblTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PrbenttblTableMap::COL_IDNENTPRB);
            $criteria->addSelectColumn(PrbenttblTableMap::COL_UUID);
            $criteria->addSelectColumn(PrbenttblTableMap::COL_IDNENTUSR);
            $criteria->addSelectColumn(PrbenttblTableMap::COL_UIDENTUSR);
            $criteria->addSelectColumn(PrbenttblTableMap::COL_IDNENTCRS);
            $criteria->addSelectColumn(PrbenttblTableMap::COL_UIDENTCRS);
            $criteria->addSelectColumn(PrbenttblTableMap::COL_VIDENTPRB);
            $criteria->addSelectColumn(PrbenttblTableMap::COL_HRRENTPRB);
            $criteria->addSelectColumn(PrbenttblTableMap::COL_STDENTPRB);
            $criteria->addSelectColumn(PrbenttblTableMap::COL_RMVENTPRB);
            $criteria->addSelectColumn(PrbenttblTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PrbenttblTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(PrbenttblTableMap::COL_EMLENTPRB);
            $criteria->addSelectColumn(PrbenttblTableMap::COL_BNDENTPRB);
            $criteria->addSelectColumn(PrbenttblTableMap::COL_VSTENTPRB);
        } else {
            $criteria->addSelectColumn($alias . '.idnentprb');
            $criteria->addSelectColumn($alias . '.uuid');
            $criteria->addSelectColumn($alias . '.idnentusr');
            $criteria->addSelectColumn($alias . '.uidentusr');
            $criteria->addSelectColumn($alias . '.idnentcrs');
            $criteria->addSelectColumn($alias . '.uidentcrs');
            $criteria->addSelectColumn($alias . '.videntprb');
            $criteria->addSelectColumn($alias . '.hrrentprb');
            $criteria->addSelectColumn($alias . '.stdentprb');
            $criteria->addSelectColumn($alias . '.rmventprb');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.emlentprb');
            $criteria->addSelectColumn($alias . '.bndentprb');
            $criteria->addSelectColumn($alias . '.vstentprb');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PrbenttblTableMap::DATABASE_NAME)->getTable(PrbenttblTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PrbenttblTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PrbenttblTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PrbenttblTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Prbenttbl or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Prbenttbl object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrbenttblTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Prbenttbl) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PrbenttblTableMap::DATABASE_NAME);
            $criteria->add(PrbenttblTableMap::COL_IDNENTPRB, (array) $values, Criteria::IN);
        }

        $query = PrbenttblQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PrbenttblTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PrbenttblTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the prbenttbl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PrbenttblQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Prbenttbl or Criteria object.
     *
     * @param mixed               $criteria Criteria or Prbenttbl object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrbenttblTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Prbenttbl object
        }

        if ($criteria->containsKey(PrbenttblTableMap::COL_IDNENTPRB) && $criteria->keyContainsValue(PrbenttblTableMap::COL_IDNENTPRB) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PrbenttblTableMap::COL_IDNENTPRB.')');
        }


        // Set the correct dbName
        $query = PrbenttblQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PrbenttblTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PrbenttblTableMap::buildTableMap();
