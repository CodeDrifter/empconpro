<?php

namespace Map;

use \Crsenttbl;
use \CrsenttblQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'crsenttbl' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class CrsenttblTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.CrsenttblTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'empconpro';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'crsenttbl';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Crsenttbl';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Crsenttbl';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the idnentcrs field
     */
    const COL_IDNENTCRS = 'crsenttbl.idnentcrs';

    /**
     * the column name for the uuid field
     */
    const COL_UUID = 'crsenttbl.uuid';

    /**
     * the column name for the nmbentcrs field
     */
    const COL_NMBENTCRS = 'crsenttbl.nmbentcrs';

    /**
     * the column name for the dscentcrs field
     */
    const COL_DSCENTCRS = 'crsenttbl.dscentcrs';

    /**
     * the column name for the prcentcrs field
     */
    const COL_PRCENTCRS = 'crsenttbl.prcentcrs';

    /**
     * the column name for the lnkentcrs field
     */
    const COL_LNKENTCRS = 'crsenttbl.lnkentcrs';

    /**
     * the column name for the rmventcrs field
     */
    const COL_RMVENTCRS = 'crsenttbl.rmventcrs';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'crsenttbl.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'crsenttbl.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idnentcrs', 'Uuid', 'Nmbentcrs', 'Dscentcrs', 'Prcentcrs', 'Lnkentcrs', 'Rmventcrs', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('idnentcrs', 'uuid', 'nmbentcrs', 'dscentcrs', 'prcentcrs', 'lnkentcrs', 'rmventcrs', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(CrsenttblTableMap::COL_IDNENTCRS, CrsenttblTableMap::COL_UUID, CrsenttblTableMap::COL_NMBENTCRS, CrsenttblTableMap::COL_DSCENTCRS, CrsenttblTableMap::COL_PRCENTCRS, CrsenttblTableMap::COL_LNKENTCRS, CrsenttblTableMap::COL_RMVENTCRS, CrsenttblTableMap::COL_CREATED_AT, CrsenttblTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('idnentcrs', 'uuid', 'nmbentcrs', 'dscentcrs', 'prcentcrs', 'lnkentcrs', 'rmventcrs', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idnentcrs' => 0, 'Uuid' => 1, 'Nmbentcrs' => 2, 'Dscentcrs' => 3, 'Prcentcrs' => 4, 'Lnkentcrs' => 5, 'Rmventcrs' => 6, 'CreatedAt' => 7, 'UpdatedAt' => 8, ),
        self::TYPE_CAMELNAME     => array('idnentcrs' => 0, 'uuid' => 1, 'nmbentcrs' => 2, 'dscentcrs' => 3, 'prcentcrs' => 4, 'lnkentcrs' => 5, 'rmventcrs' => 6, 'createdAt' => 7, 'updatedAt' => 8, ),
        self::TYPE_COLNAME       => array(CrsenttblTableMap::COL_IDNENTCRS => 0, CrsenttblTableMap::COL_UUID => 1, CrsenttblTableMap::COL_NMBENTCRS => 2, CrsenttblTableMap::COL_DSCENTCRS => 3, CrsenttblTableMap::COL_PRCENTCRS => 4, CrsenttblTableMap::COL_LNKENTCRS => 5, CrsenttblTableMap::COL_RMVENTCRS => 6, CrsenttblTableMap::COL_CREATED_AT => 7, CrsenttblTableMap::COL_UPDATED_AT => 8, ),
        self::TYPE_FIELDNAME     => array('idnentcrs' => 0, 'uuid' => 1, 'nmbentcrs' => 2, 'dscentcrs' => 3, 'prcentcrs' => 4, 'lnkentcrs' => 5, 'rmventcrs' => 6, 'created_at' => 7, 'updated_at' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('crsenttbl');
        $this->setPhpName('Crsenttbl');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Crsenttbl');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idnentcrs', 'Idnentcrs', 'INTEGER', true, 10, null);
        $this->addColumn('uuid', 'Uuid', 'CHAR', true, 36, null);
        $this->addColumn('nmbentcrs', 'Nmbentcrs', 'VARCHAR', true, 255, null);
        $this->addColumn('dscentcrs', 'Dscentcrs', 'LONGVARCHAR', true, null, null);
        $this->addColumn('prcentcrs', 'Prcentcrs', 'INTEGER', true, null, null);
        $this->addColumn('lnkentcrs', 'Lnkentcrs', 'VARCHAR', false, 255, null);
        $this->addColumn('rmventcrs', 'Rmventcrs', 'BOOLEAN', true, 1, false);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ChcenttblRelatedByIdnentcrs', '\\Chcenttbl', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':idnentcrs',
    1 => ':idnentcrs',
  ),
), null, null, 'ChcenttblsRelatedByIdnentcrs', false);
        $this->addRelation('ChcenttblRelatedByUidentcrs', '\\Chcenttbl', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':uidentcrs',
    1 => ':uuid',
  ),
), null, null, 'ChcenttblsRelatedByUidentcrs', false);
        $this->addRelation('PrbenttblRelatedByIdnentcrs', '\\Prbenttbl', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':idnentcrs',
    1 => ':idnentcrs',
  ),
), null, null, 'PrbenttblsRelatedByIdnentcrs', false);
        $this->addRelation('PrbenttblRelatedByUidentcrs', '\\Prbenttbl', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':uidentcrs',
    1 => ':uuid',
  ),
), null, null, 'PrbenttblsRelatedByUidentcrs', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idnentcrs', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idnentcrs', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idnentcrs', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idnentcrs', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idnentcrs', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idnentcrs', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idnentcrs', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CrsenttblTableMap::CLASS_DEFAULT : CrsenttblTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Crsenttbl object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CrsenttblTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CrsenttblTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CrsenttblTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CrsenttblTableMap::OM_CLASS;
            /** @var Crsenttbl $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CrsenttblTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CrsenttblTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CrsenttblTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Crsenttbl $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CrsenttblTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CrsenttblTableMap::COL_IDNENTCRS);
            $criteria->addSelectColumn(CrsenttblTableMap::COL_UUID);
            $criteria->addSelectColumn(CrsenttblTableMap::COL_NMBENTCRS);
            $criteria->addSelectColumn(CrsenttblTableMap::COL_DSCENTCRS);
            $criteria->addSelectColumn(CrsenttblTableMap::COL_PRCENTCRS);
            $criteria->addSelectColumn(CrsenttblTableMap::COL_LNKENTCRS);
            $criteria->addSelectColumn(CrsenttblTableMap::COL_RMVENTCRS);
            $criteria->addSelectColumn(CrsenttblTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(CrsenttblTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.idnentcrs');
            $criteria->addSelectColumn($alias . '.uuid');
            $criteria->addSelectColumn($alias . '.nmbentcrs');
            $criteria->addSelectColumn($alias . '.dscentcrs');
            $criteria->addSelectColumn($alias . '.prcentcrs');
            $criteria->addSelectColumn($alias . '.lnkentcrs');
            $criteria->addSelectColumn($alias . '.rmventcrs');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CrsenttblTableMap::DATABASE_NAME)->getTable(CrsenttblTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CrsenttblTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CrsenttblTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CrsenttblTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Crsenttbl or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Crsenttbl object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrsenttblTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Crsenttbl) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CrsenttblTableMap::DATABASE_NAME);
            $criteria->add(CrsenttblTableMap::COL_IDNENTCRS, (array) $values, Criteria::IN);
        }

        $query = CrsenttblQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CrsenttblTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CrsenttblTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the crsenttbl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CrsenttblQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Crsenttbl or Criteria object.
     *
     * @param mixed               $criteria Criteria or Crsenttbl object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrsenttblTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Crsenttbl object
        }

        if ($criteria->containsKey(CrsenttblTableMap::COL_IDNENTCRS) && $criteria->keyContainsValue(CrsenttblTableMap::COL_IDNENTCRS) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CrsenttblTableMap::COL_IDNENTCRS.')');
        }


        // Set the correct dbName
        $query = CrsenttblQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CrsenttblTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CrsenttblTableMap::buildTableMap();
