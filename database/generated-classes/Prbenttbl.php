<?php

use Base\Prbenttbl as BasePrbenttbl;
use Illuminate\Support\Facades\Log;

class Prbenttbl extends BasePrbenttbl
{
    public static function crtprbenttbl(array $data , \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $prb = new \Prbenttbl();
        try{
            if(array_key_exists('uuid', $data)){
                if(!is_null($data['uuid'])){
                    $prb->setUuid($data['uuid']);
                }else{
                    throw new \Propel\Runtime\Exception\PropelException('uuid cannot be null');
                }
            }
            if(array_key_exists('idnentusr', $data)){
                if(!is_null($data['idnentusr'])){
                    $prb->setIdnentusr($data['idnentusr']);
                }
            }
            if(array_key_exists('uidentusr', $data)){
                if(!is_null($data['uidentusr'])){
                    $prb->setUidentusr($data['uidentusr']);
                }
            }
            if(array_key_exists('idnentcrs', $data)){
                if(!is_null($data['idnentcrs'])){
                    $prb->setIdnentcrs($data['idnentcrs']);
                }
            }
            if(array_key_exists('uidentcrs', $data)){
                if(!is_null($data['uidentcrs'])){
                    $prb->setUidentcrs($data['uidentcrs']);
                }
            }
            if(array_key_exists('videntprb', $data)){
                if(!is_null($data['videntprb'])){
                    $prb->setVidentprb($data['videntprb']);
                }else{
                    throw new \Propel\Runtime\Exception\PropelException('videntprb cannot be null');
                }
            }
            if(array_key_exists('hrrentprb', $data)){
                if(!is_null($data['hrrentprb'])){
                    $prb->setHrrentprb($data['hrrentprb']);
                }else{
                    throw new \Propel\Runtime\Exception\PropelException('hrrentprb cannot be null');
                }
            }
            if(array_key_exists('stdentprb', $data)){
                if(!is_null($data['stdentprb'])){
                    $prb->setStdentprb($data['stdentprb']);
                }else{
                    throw new \Propel\Runtime\Exception\PropelException('stdentprb cannot be null');
                }
            }
            if(array_key_exists('emlentprb', $data)){
                if(!is_null($data['emlentprb'])){
                    $prb->setEmlentprb($data['emlentprb']);
                }else{
                    throw new \Propel\Runtime\Exception\PropelException('emlentprb cannot be null');
                }
            }
            if(array_key_exists('rmventprb', $data)){
                if(!is_null($data['rmventprb'])){
                    $prb->setRmventprb($data['rmventprb']);
                }else{
                    throw new \Propel\Runtime\Exception\PropelException('rmventprb cannot be null');
                }
            }
            if(array_key_exists('created_at', $data)){
                if(!is_null($data['created_at'])){
                    $prb->setCreatedAt($data['created_at']);
                }
            }
            if(array_key_exists('updated_at', $data)){
                if(!is_null($data['updated_at'])){
                    $prb->setUpdatedAt($data['updated_at']);
                }
            }
            $prb->save($connection);
        } catch (\Propel\Runtime\Exception\PropelException $e) {
            Illuminate\Support\Facades\Log::debug($e);
            return false;
        }
        return $prb;
    }

    public static function rmvprbenttbl($idnentprb, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $prb = \PrbenttblQuery::create()
            ->filterByIdnentprb($idnentprb)
            ->findOne($connection);

        if(!$prb) return false;

        try {
            $prb->delete($connection);
        } catch (\Propel\Runtime\Exception\PropelException $e) {
            Illuminate\Support\Facades\Log::debug($e);
            return false;
        }

        return true;
    }

    public static function updprbenttbl(array $data , \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $prb = \PrbenttblQuery::create()
            ->filterByIdnentprb($data['idnentprb'])
            ->findOne($connection);

        if(!$prb) return false;

        try{
            if(array_key_exists('uuid', $data)){
                if(!is_null($data['uuid'])){
                    $prb->setUuid($data['uuid']);
                }else{
                    throw new \Propel\Runtime\Exception\PropelException('uuid cannot be null');
                }
            }
            if(array_key_exists('videntprb', $data)){
                if(!is_null($data['videntprb'])){
                    $prb->setVidentprb($data['videntprb']);
                }else{
                    throw new \Propel\Runtime\Exception\PropelException('videntprb cannot be null');
                }
            }
            if(array_key_exists('hrrentprb', $data)){
                if(!is_null($data['hrrentprb'])){
                    $prb->setHrrentprb($data['hrrentprb']);
                }else{
                    throw new \Propel\Runtime\Exception\PropelException('hrrentprb cannot be null');
                }
            }
            if(array_key_exists('stdentprb', $data)){
                if(!is_null($data['stdentprb'])){
                    $prb->setStdentprb($data['stdentprb']);
                }else{
                    throw new \Propel\Runtime\Exception\PropelException('stdentprb cannot be null');
                }
            }
            if(array_key_exists('rmventprb', $data)){
                if(!is_null($data['rmventprb'])){
                    $prb->setRmventprb($data['rmventprb']);
                }else{
                    throw new \Propel\Runtime\Exception\PropelException('rmventprb cannot be null');
                }
            }
            if(array_key_exists('bndentprb', $data)){
                if(!is_null($data['bndentprb'])){
                    $prb->setBndentprb($data['bndentprb']);
                }
            }
            if(array_key_exists('vstentprb', $data)){
                if(!is_null($data['vstentprb'])){
                    $prb->setVstentprb($data['vstentprb']);
                }
            }
            if(array_key_exists('updated_at', $data)){
                if(!is_null($data['updated_at'])){
                    $prb->setUpdatedAt($data['updated_at']);
                }
            }
            $prb->save($connection);
        } catch (\Propel\Runtime\Exception\PropelException $e) {
            Illuminate\Support\Facades\Log::debug($e);
            return false;
        }

            return $prb;
    }

    public static function dspprbenttbl($filidnentcrs, $filidnentusr, $filuidentcrs, $filuidentusr, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $allprb = \PrbenttblQuery::create();if($filidnentcrs != 0){
            $allprb = $allprb->filterByIdnentcrs($filidnentcrs);
        }if($filidnentusr != 0){
            $allprb = $allprb->filterByIdnentusr($filidnentusr);
        }if($filuidentcrs != 0){
            $allprb = $allprb->filterByUidentcrs($filuidentcrs);
        }if($filuidentusr != 0){
            $allprb = $allprb->filterByUidentusr($filuidentusr);
        }

        $allprb = $allprb->find();

        if(!$allprb) return false;

        return $allprb;
    }

    public static function fnoprbenttbl($idnentprb, \Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $prb = \PrbenttblQuery::create()
            ->filterByIdnentprb($idnentprb)
            ->findOne($connection);

        if(!$prb) return false;

        return $prb;
    }

    public static function fnuprbenttbl($uuid,\Propel\Runtime\Connection\ConnectionInterface $connection = null)
    {
        $prb = \PrbenttblQuery::create()
            ->filterByUuid($uuid)
            ->findOne($connection);

        if(!$prb) return false;

        return $prb;
    }
}
    //TODO *CRUD Generator control separator line* (Don't remove this line!)
