<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::post('refresh', 'Auth\RefreshController@refresh');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');

});

Route::post('fetchCurso', 'CrsenttblController@fetchCurso');
Route::post('fetchCursosUser', 'CrsenttblController@fetchCursosUser');

Route::post('createRecibo/{cursouuid}/{useruuid}', 'ChcenttblController@create');
Route::post('createFree/{cursouuid}/{useruuid}', 'ChcenttblController@create');

Route::post('sendEmail', 'UsersController@sendEmail');

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('email/verify/{cursos}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});


//Checkout Route
Route::get('/Checkout/fetch', 'ChcenttblController@loadtable')->name('Checkout.fetch');
Route::get('/Checkout/fetch/list', 'ChcenttblController@loadList')->name('Checkout.fetch.list');
Route::post('/Checkout/fetch/both', 'ChcenttblController@findOneByBoth')->name('Checkout.fetch.both');
Route::post('/Checkout/find/one', 'ChcenttblController@findOne')->name('Checkout.find.one');
Route::post('/Checkout/submit', 'ChcenttblController@createHandler')->name('Checkout.submit');
Route::post('/Checkout/modify', 'ChcenttblController@update')->name('Checkout.modify');
Route::post('/Checkout/remove', 'ChcenttblController@destroy')->name('Checkout.remove');

//Pruebas Route
Route::get('/Pruebas/fetch', 'PrbenttblController@loadtable')->name('Pruebas.fetch');
Route::get('/Pruebas/fetch/list', 'PrbenttblController@loadList')->name('Pruebas.fetch.list');
Route::post('/Pruebas/fetch/video', 'PrbenttblController@fetchVideo')->name('Pruebas.fetch.video');
Route::post('/Pruebas/fetch/both', 'PrbenttblController@findOneByBoth')->name('Pruebas.fetch.both');
Route::post('/Pruebas/find/one', 'PrbenttblController@findOne')->name('Pruebas.find.one');
Route::post('/Pruebas/submit', 'PrbenttblController@create')->name('Pruebas.submit');
Route::post('/Pruebas/modify/check', 'PrbenttblController@check')->name('Pruebas.modify.check');
Route::post('/Pruebas/modify', 'PrbenttblController@update')->name('Pruebas.modify');
Route::post('/Pruebas/remove', 'PrbenttblController@destroy')->name('Pruebas.remove');

//Cursos Route
Route::get('/Cursos/fetch', 'CrsenttblController@loadtable')->name('Cursos.fetch');
Route::get('/Cursos/fetch/list', 'CrsenttblController@loadList')->name('Cursos.fetch.list');
Route::post('/Cursos/find/one', 'CrsenttblController@findOne')->name('Cursos.find.one');
Route::post('/Cursos/find/users', 'CrsenttblController@findUsersByCurso')->name('Cursos.find.users');
Route::post('/Cursos/submit', 'CrsenttblController@create')->name('Cursos.submit');
Route::post('/Cursos/modify', 'CrsenttblController@update')->name('Cursos.modify');
Route::post('/Cursos/remove', 'CrsenttblController@destroy')->name('Cursos.remove');

//Usuarios Route
Route::get('/Usuarios/fetch', 'UsersController@loadtable')->name('Usuarios.fetch');
Route::get('/Usuarios/fetch/list', 'UsersController@loadList')->name('Usuarios.fetch.list');
Route::post('/Usuarios/find/one', 'UsersController@findOne')->name('Usuarios.find.one');
Route::post('/Usuarios/submit', 'UsersController@create')->name('Usuarios.submit');
Route::post('/Usuarios/modify', 'UsersController@update')->name('Usuarios.modify');
Route::post('/Usuarios/remove', 'UsersController@destroy')->name('Usuarios.remove');

//TODO *CRUD Generator control separator line* (Don't remove this line!)




