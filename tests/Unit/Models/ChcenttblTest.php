<?php

namespace Tests\Unit\Models;

use App\TransactionHandler;
use Faker;
use Tests\TestCase;
use Illuminate\Support\Facades\Log;
use Propel\Runtime\Exception\PropelException;

class ChcenttblUnitTest extends TestCase
{
    /** @test */
    public function it_can_crud_Chcenttbl()
    {
//CREATE
        $faker = Faker\Factory::create();
        $trncnn = TransactionHandler::begin();

        $data = [
			'uuid' => $faker->text(36),
			'idnentusr' => $faker->numberBetween(0, 9999999999),
			'uidentusr' => $faker->text(36),
			'idnentcrs' => $faker->numberBetween(0, 9999999999),
			'uidentcrs' => $faker->text(36),
			'idnmrcpag' => $faker->numberBetween(0, 99999999999),
			'created_at' => $faker->iso8601(),
			'updated_at' => $faker->iso8601(),
		];
        $result = \Chcenttbl::crtchcenttbl($data, $trncnn);
        self::assertInstanceOf(\Chcenttbl::class, $result);
		self::assertEquals($data['uuid'], $result->getUuid(), 'Valor inserción no concuerda en uuid');
		self::assertEquals($data['idnentusr'], $result->getIdnentusr(), 'Valor inserción no concuerda en idnentusr');
		self::assertEquals($data['uidentusr'], $result->getUidentusr(), 'Valor inserción no concuerda en uidentusr');
		self::assertEquals($data['idnentcrs'], $result->getIdnentcrs(), 'Valor inserción no concuerda en idnentcrs');
		self::assertEquals($data['uidentcrs'], $result->getUidentcrs(), 'Valor inserción no concuerda en uidentcrs');
		self::assertEquals($data['idnmrcpag'], $result->getIdnmrcpag(), 'Valor inserción no concuerda en idnmrcpag');
       try {
			self::assertEquals($data['created_at'], $result->getCreatedAt(DATE_ISO8601), 'Valor inserción no concuerda en created_at');
       } catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato inserción no concuerda: created_at');
       }
       try {
			self::assertEquals($data['updated_at'], $result->getUpdatedAt(DATE_ISO8601), 'Valor inserción no concuerda en updated_at');
       } catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato inserción no concuerda: updated_at');
       }

// UPDATE
        $update = [
			'idnentchc' => $result->getIdnentchc(),
			'uuid' => $faker->text(36),
			'idnentusr' => null,
			'uidentusr' => null,
			'idnentcrs' => null,
			'uidentcrs' => null,
			'idnmrcpag' => $faker->numberBetween(0, 99999999999),
			'created_at' => null,
			'updated_at' => null,
		];
        $updated = \Chcenttbl::updchcenttbl($update, $trncnn);
        self::assertInstanceOf(\Chcenttbl::class, $updated);
		self::assertEquals($result->getIdnentchc(),$updated->getIdnentchc(), 'Llaves actualización no concuerdan');
		self::assertEquals($update['uuid'], $updated->getUuid(), 'Valor actualización no concuerda en uuid');
		self::assertEquals(null, $updated->getIdnentusr(), 'Valor actualización no concuerda en idnentusr');
		self::assertEquals(null, $updated->getUidentusr(), 'Valor actualización no concuerda en uidentusr');
		self::assertEquals(null, $updated->getIdnentcrs(), 'Valor actualización no concuerda en idnentcrs');
		self::assertEquals(null, $updated->getUidentcrs(), 'Valor actualización no concuerda en uidentcrs');
		self::assertEquals($update['idnmrcpag'], $updated->getIdnmrcpag(), 'Valor actualización no concuerda en idnmrcpag');
		try {
			self::assertEquals(null, $updated->getCreatedAt(DATE_ISO8601), 'Valor actualización no concuerda en created_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato actualización no concuerda: created_at');
        }
		try {
			self::assertEquals(null, $updated->getUpdatedAt(DATE_ISO8601), 'Valor actualización no concuerda en updated_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato actualización no concuerda: updated_at');
        }

//DISPLAY ONE CVE
		$found = \Chcenttbl::fnochcenttbl($updated->getIdnentchc(),$trncnn);
		self::assertInstanceOf(\Chcenttbl::class, $found);
		self::assertEquals($result->getIdnentchc(),$found->getIdnentchc(), 'Llaves búsqueda no concuerdan');
		self::assertEquals($update['uuid'], $found->getUuid(), 'Valor búsqueda no concuerda en uuid');
		self::assertEquals($update['idnentusr'], $found->getIdnentusr(), 'Valor búsqueda no concuerda en idnentusr');
		self::assertEquals($update['uidentusr'], $found->getUidentusr(), 'Valor búsqueda no concuerda en uidentusr');
		self::assertEquals($update['idnentcrs'], $found->getIdnentcrs(), 'Valor búsqueda no concuerda en idnentcrs');
		self::assertEquals($update['uidentcrs'], $found->getUidentcrs(), 'Valor búsqueda no concuerda en uidentcrs');
		self::assertEquals($update['idnmrcpag'], $found->getIdnmrcpag(), 'Valor búsqueda no concuerda en idnmrcpag');
		try {
			self::assertEquals($update['created_at'], $found->getCreatedAt(DATE_ISO8601), 'Valor búsqueda no concuerda en created_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: created_at');
        }
		try {
			self::assertEquals($update['updated_at'], $found->getUpdatedAt(DATE_ISO8601), 'Valor búsqueda no concuerda en updated_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: updated_at');
        }

//DISPLAY ONE UUID
		$foundU = \Chcenttbl::fnuchcenttbl($updated->getUuid(),$trncnn);
		self::assertInstanceOf(\Chcenttbl::class, $foundU);
		self::assertEquals($result->getIdnentchc(),$foundU->getIdnentchc(), 'Llaves búsqueda uuid no concuerdan');
		self::assertEquals($update['uuid'], $foundU->getUuid(), 'Valor búsqueda uuid no concuerda en uuid');
		self::assertEquals($update['idnentusr'], $foundU->getIdnentusr(), 'Valor búsqueda uuid no concuerda en idnentusr');
		self::assertEquals($update['uidentusr'], $foundU->getUidentusr(), 'Valor búsqueda uuid no concuerda en uidentusr');
		self::assertEquals($update['idnentcrs'], $foundU->getIdnentcrs(), 'Valor búsqueda uuid no concuerda en idnentcrs');
		self::assertEquals($update['uidentcrs'], $foundU->getUidentcrs(), 'Valor búsqueda uuid no concuerda en uidentcrs');
		self::assertEquals($update['idnmrcpag'], $foundU->getIdnmrcpag(), 'Valor búsqueda uuid no concuerda en idnmrcpag');
		try {
			self::assertEquals($update['created_at'], $foundU->getCreatedAt(DATE_ISO8601), 'Valor búsqueda uuid no concuerda en created_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: created_at');
        }
		try {
			self::assertEquals($update['updated_at'], $foundU->getUpdatedAt(DATE_ISO8601), 'Valor búsqueda uuid no concuerda en updated_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: updated_at');
        }

//DISPLAY ALL
		$all = \Chcenttbl::dspchcenttbl($trncnn);
		self::assertInstanceOf(\Propel\Runtime\Collection\Collection::class, $all);
		self::assertTrue($all->count() > 0);

//REMOVE ONE
		$destroyed = \Chcenttbl::rmvchcenttbl($result->getidnentchc(),$trncnn);
		self::assertTrue($destroyed);
        TransactionHandler::rollback($trncnn);
    }
}