<?php

namespace Tests\Unit\Models;

use App\TransactionHandler;
use Faker;
use Tests\TestCase;
use Illuminate\Support\Facades\Log;
use Propel\Runtime\Exception\PropelException;

class CrsenttblUnitTest extends TestCase
{
    /** @test */
    public function it_can_crud_Crsenttbl()
    {
//CREATE
        $faker = Faker\Factory::create();
        $trncnn = TransactionHandler::begin();

        $data = [
			'uuid' => $faker->text(36),
			'nmbentcrs' => $faker->text(255),
			'dscentcrs' => $faker->text(255),
			'prcentcrs' => $faker->numberBetween(0, 99999999999),
			'created_at' => $faker->iso8601(),
			'updated_at' => $faker->iso8601(),
		];
        $result = \Crsenttbl::crtcrsenttbl($data, $trncnn);
        self::assertInstanceOf(\Crsenttbl::class, $result);
		self::assertEquals($data['uuid'], $result->getUuid(), 'Valor inserción no concuerda en uuid');
		self::assertEquals($data['nmbentcrs'], $result->getNmbentcrs(), 'Valor inserción no concuerda en nmbentcrs');
		self::assertEquals($data['dscentcrs'], $result->getDscentcrs(), 'Valor inserción no concuerda en dscentcrs');
		self::assertEquals($data['prcentcrs'], $result->getPrcentcrs(), 'Valor inserción no concuerda en prcentcrs');
       try {
			self::assertEquals($data['created_at'], $result->getCreatedAt(DATE_ISO8601), 'Valor inserción no concuerda en created_at');
       } catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato inserción no concuerda: created_at');
       }
       try {
			self::assertEquals($data['updated_at'], $result->getUpdatedAt(DATE_ISO8601), 'Valor inserción no concuerda en updated_at');
       } catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato inserción no concuerda: updated_at');
       }

// UPDATE
        $update = [
			'idnentcrs' => $result->getIdnentcrs(),
			'uuid' => $faker->text(36),
			'nmbentcrs' => $faker->text(255),
			'dscentcrs' => $faker->text(255),
			'prcentcrs' => $faker->numberBetween(0, 99999999999),
			'created_at' => null,
			'updated_at' => null,
		];
        $updated = \Crsenttbl::updcrsenttbl($update, $trncnn);
        self::assertInstanceOf(\Crsenttbl::class, $updated);
		self::assertEquals($result->getIdnentcrs(),$updated->getIdnentcrs(), 'Llaves actualización no concuerdan');
		self::assertEquals($update['uuid'], $updated->getUuid(), 'Valor actualización no concuerda en uuid');
		self::assertEquals($update['nmbentcrs'], $updated->getNmbentcrs(), 'Valor actualización no concuerda en nmbentcrs');
		self::assertEquals($update['dscentcrs'], $updated->getDscentcrs(), 'Valor actualización no concuerda en dscentcrs');
		self::assertEquals($update['prcentcrs'], $updated->getPrcentcrs(), 'Valor actualización no concuerda en prcentcrs');
		try {
			self::assertEquals(null, $updated->getCreatedAt(DATE_ISO8601), 'Valor actualización no concuerda en created_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato actualización no concuerda: created_at');
        }
		try {
			self::assertEquals(null, $updated->getUpdatedAt(DATE_ISO8601), 'Valor actualización no concuerda en updated_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato actualización no concuerda: updated_at');
        }

//DISPLAY ONE CVE
		$found = \Crsenttbl::fnocrsenttbl($updated->getIdnentcrs(),$trncnn);
		self::assertInstanceOf(\Crsenttbl::class, $found);
		self::assertEquals($result->getIdnentcrs(),$found->getIdnentcrs(), 'Llaves búsqueda no concuerdan');
		self::assertEquals($update['uuid'], $found->getUuid(), 'Valor búsqueda no concuerda en uuid');
		self::assertEquals($update['nmbentcrs'], $found->getNmbentcrs(), 'Valor búsqueda no concuerda en nmbentcrs');
		self::assertEquals($update['dscentcrs'], $found->getDscentcrs(), 'Valor búsqueda no concuerda en dscentcrs');
		self::assertEquals($update['prcentcrs'], $found->getPrcentcrs(), 'Valor búsqueda no concuerda en prcentcrs');
		try {
			self::assertEquals($update['created_at'], $found->getCreatedAt(DATE_ISO8601), 'Valor búsqueda no concuerda en created_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: created_at');
        }
		try {
			self::assertEquals($update['updated_at'], $found->getUpdatedAt(DATE_ISO8601), 'Valor búsqueda no concuerda en updated_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: updated_at');
        }

//DISPLAY ONE UUID
		$foundU = \Crsenttbl::fnucrsenttbl($updated->getUuid(),$trncnn);
		self::assertInstanceOf(\Crsenttbl::class, $foundU);
		self::assertEquals($result->getIdnentcrs(),$foundU->getIdnentcrs(), 'Llaves búsqueda uuid no concuerdan');
		self::assertEquals($update['uuid'], $foundU->getUuid(), 'Valor búsqueda uuid no concuerda en uuid');
		self::assertEquals($update['nmbentcrs'], $foundU->getNmbentcrs(), 'Valor búsqueda uuid no concuerda en nmbentcrs');
		self::assertEquals($update['dscentcrs'], $foundU->getDscentcrs(), 'Valor búsqueda uuid no concuerda en dscentcrs');
		self::assertEquals($update['prcentcrs'], $foundU->getPrcentcrs(), 'Valor búsqueda uuid no concuerda en prcentcrs');
		try {
			self::assertEquals($update['created_at'], $foundU->getCreatedAt(DATE_ISO8601), 'Valor búsqueda uuid no concuerda en created_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: created_at');
        }
		try {
			self::assertEquals($update['updated_at'], $foundU->getUpdatedAt(DATE_ISO8601), 'Valor búsqueda uuid no concuerda en updated_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: updated_at');
        }

//DISPLAY ALL
		$all = \Crsenttbl::dspcrsenttbl($trncnn);
		self::assertInstanceOf(\Propel\Runtime\Collection\Collection::class, $all);
		self::assertTrue($all->count() > 0);

//REMOVE ONE
		$destroyed = \Crsenttbl::rmvcrsenttbl($result->getidnentcrs(),$trncnn);
		self::assertTrue($destroyed);
        TransactionHandler::rollback($trncnn);
    }
}