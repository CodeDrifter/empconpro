<?php

namespace Tests\Unit\Models;

use App\TransactionHandler;
use Faker;
use Tests\TestCase;
use Illuminate\Support\Facades\Log;
use Propel\Runtime\Exception\PropelException;

class PrbenttblUnitTest extends TestCase
{
    /** @test */
    public function it_can_crud_Prbenttbl()
    {
//@CREATE
        $faker = Faker\Factory::create();
        $trncnn = TransactionHandler::begin();

        $data = [
			'uuid' => $faker->text(36),
			'idnentusr' => $faker->numberBetween(0, 9999999999),
			'uidentusr' => $faker->text(36),
			'idnentcrs' => $faker->numberBetween(0, 9999999999),
			'uidentcrs' => $faker->text(36),
			'videntprb' => $faker->numberBetween(0, 99999999999),
			'hrrentprb' => $faker->iso8601(),
			'stdentprb' => $faker->numberBetween(0, 99999999999),
			'rmventprb' => $faker->boolean(),
			'created_at' => $faker->iso8601(),
			'updated_at' => $faker->iso8601(),
		];
        $result = \Prbenttbl::crtprbenttbl($data, $trncnn);
		self::assertEquals($data['uuid'], $result->getUuid(), 'Valor inserción no concuerda en uuid');
		self::assertEquals($data['idnentusr'], $result->getIdnentusr(), 'Valor inserción no concuerda en idnentusr');
		self::assertEquals($data['uidentusr'], $result->getUidentusr(), 'Valor inserción no concuerda en uidentusr');
		self::assertEquals($data['idnentcrs'], $result->getIdnentcrs(), 'Valor inserción no concuerda en idnentcrs');
		self::assertEquals($data['uidentcrs'], $result->getUidentcrs(), 'Valor inserción no concuerda en uidentcrs');
		self::assertEquals($data['videntprb'], $result->getVidentprb(), 'Valor inserción no concuerda en videntprb');
       try {
			self::assertEquals($data['hrrentprb'], $result->getHrrentprb(DATE_ISO8601), 'Valor inserción no concuerda en hrrentprb');
       } catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato inserción no concuerda: hrrentprb');
       }
		self::assertEquals($data['stdentprb'], $result->getStdentprb(), 'Valor inserción no concuerda en stdentprb');
		self::assertEquals($data['rmventprb'], $result->getRmventprb(), 'Valor inserción no concuerda en rmventprb');
       try {
			self::assertEquals($data['created_at'], $result->getCreatedAt(DATE_ISO8601), 'Valor inserción no concuerda en created_at');
       } catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato inserción no concuerda: created_at');
       }
       try {
			self::assertEquals($data['updated_at'], $result->getUpdatedAt(DATE_ISO8601), 'Valor inserción no concuerda en updated_at');
       } catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato inserción no concuerda: updated_at');
       }
//@UPDATE
        $update = [
			'idnentprb' => $result->getIdnentprb(),
			'uuid' => $faker->text(36),
			'idnentusr' => null,
			'uidentusr' => null,
			'idnentcrs' => null,
			'uidentcrs' => null,
			'videntprb' => $faker->numberBetween(0, 99999999999),
			'hrrentprb' => $faker->iso8601(),
			'stdentprb' => $faker->numberBetween(0, 99999999999),
			'rmventprb' => $faker->boolean(),
			'created_at' => null,
			'updated_at' => null,
		];
        $updated = \Prbenttbl::updprbenttbl($update, $trncnn);
        self::assertInstanceOf(\Prbenttbl::class, $updated);
		self::assertEquals($result->getIdnentprb(),$updated->getIdnentprb(), 'Llaves actualización no concuerdan');
		self::assertEquals($update['uuid'], $updated->getUuid(), 'Valor actualización no concuerda en uuid');
		self::assertEquals(null, $updated->getIdnentusr(), 'Valor actualización no concuerda en idnentusr');
		self::assertEquals(null, $updated->getUidentusr(), 'Valor actualización no concuerda en uidentusr');
		self::assertEquals(null, $updated->getIdnentcrs(), 'Valor actualización no concuerda en idnentcrs');
		self::assertEquals(null, $updated->getUidentcrs(), 'Valor actualización no concuerda en uidentcrs');
		self::assertEquals($update['videntprb'], $updated->getVidentprb(), 'Valor actualización no concuerda en videntprb');
		try {
			self::assertEquals($update['hrrentprb'], $updated->getHrrentprb(DATE_ISO8601), 'Valor actualización no concuerda en hrrentprb');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato actualización no concuerda: hrrentprb');
        }
		self::assertEquals($update['stdentprb'], $updated->getStdentprb(), 'Valor actualización no concuerda en stdentprb');
		self::assertEquals($update['rmventprb'], $updated->getRmventprb(), 'Valor actualización no concuerda en rmventprb');
		try {
			self::assertEquals(null, $updated->getCreatedAt(DATE_ISO8601), 'Valor actualización no concuerda en created_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato actualización no concuerda: created_at');
        }
		try {
			self::assertEquals(null, $updated->getUpdatedAt(DATE_ISO8601), 'Valor actualización no concuerda en updated_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato actualización no concuerda: updated_at');
        }

//@DISPLAY ONE CVE
		$found = \Prbenttbl::fnoprbenttbl($updated->getIdnentprb(),$trncnn);
		self::assertInstanceOf(\Prbenttbl::class, $found);
		self::assertEquals($result->getIdnentprb(),$found->getIdnentprb(), 'Llaves búsqueda no concuerdan');
		self::assertEquals($update['uuid'], $found->getUuid(), 'Valor búsqueda no concuerda en uuid');
		self::assertEquals($update['idnentusr'], $found->getIdnentusr(), 'Valor búsqueda no concuerda en idnentusr');
		self::assertEquals($update['uidentusr'], $found->getUidentusr(), 'Valor búsqueda no concuerda en uidentusr');
		self::assertEquals($update['idnentcrs'], $found->getIdnentcrs(), 'Valor búsqueda no concuerda en idnentcrs');
		self::assertEquals($update['uidentcrs'], $found->getUidentcrs(), 'Valor búsqueda no concuerda en uidentcrs');
		self::assertEquals($update['videntprb'], $found->getVidentprb(), 'Valor búsqueda no concuerda en videntprb');
		try {
			self::assertEquals($update['hrrentprb'], $found->getHrrentprb(DATE_ISO8601), 'Valor búsqueda no concuerda en hrrentprb');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: hrrentprb');
        }
		self::assertEquals($update['stdentprb'], $found->getStdentprb(), 'Valor búsqueda no concuerda en stdentprb');
		self::assertEquals($update['rmventprb'], $found->getRmventprb(), 'Valor búsqueda no concuerda en rmventprb');
		try {
			self::assertEquals($update['created_at'], $found->getCreatedAt(DATE_ISO8601), 'Valor búsqueda no concuerda en created_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: created_at');
        }
		try {
			self::assertEquals($update['updated_at'], $found->getUpdatedAt(DATE_ISO8601), 'Valor búsqueda no concuerda en updated_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: updated_at');
        }

//@DISPLAY ONE UUID
		$foundU = \Prbenttbl::fnuprbenttbl($updated->getUuid(),$trncnn);
		self::assertInstanceOf(\Prbenttbl::class, $foundU);
		self::assertEquals($result->getIdnentprb(),$foundU->getIdnentprb(), 'Llaves búsqueda uuid no concuerdan');
		self::assertEquals($update['uuid'], $foundU->getUuid(), 'Valor búsqueda uuid no concuerda en uuid');
		self::assertEquals($update['idnentusr'], $foundU->getIdnentusr(), 'Valor búsqueda uuid no concuerda en idnentusr');
		self::assertEquals($update['uidentusr'], $foundU->getUidentusr(), 'Valor búsqueda uuid no concuerda en uidentusr');
		self::assertEquals($update['idnentcrs'], $foundU->getIdnentcrs(), 'Valor búsqueda uuid no concuerda en idnentcrs');
		self::assertEquals($update['uidentcrs'], $foundU->getUidentcrs(), 'Valor búsqueda uuid no concuerda en uidentcrs');
		self::assertEquals($update['videntprb'], $foundU->getVidentprb(), 'Valor búsqueda uuid no concuerda en videntprb');
		try {
			self::assertEquals($update['hrrentprb'], $foundU->getHrrentprb(DATE_ISO8601), 'Valor búsqueda uuid no concuerda en hrrentprb');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: hrrentprb');
        }
		self::assertEquals($update['stdentprb'], $foundU->getStdentprb(), 'Valor búsqueda uuid no concuerda en stdentprb');
		self::assertEquals($update['rmventprb'], $foundU->getRmventprb(), 'Valor búsqueda uuid no concuerda en rmventprb');
		try {
			self::assertEquals($update['created_at'], $foundU->getCreatedAt(DATE_ISO8601), 'Valor búsqueda uuid no concuerda en created_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: created_at');
        }
		try {
			self::assertEquals($update['updated_at'], $foundU->getUpdatedAt(DATE_ISO8601), 'Valor búsqueda uuid no concuerda en updated_at');
		} catch(PropelException $e) {
            Log::debug($e);
            self::assertTrue(false, 'Formato búsqueda no concuerda: updated_at');
        }

//@DISPLAY ALL
		$all = \Prbenttbl::dspprbenttbl(0, 0, 0, 0, $trncnn);
		self::assertInstanceOf(\Propel\Runtime\Collection\Collection::class, $all);
		self::assertTrue($all->count() > 0);

//@REMOVE ONE
		$destroyed = \Prbenttbl::rmvprbenttbl($result->getidnentprb(),$trncnn);
		self::assertTrue($destroyed);
        TransactionHandler::rollback($trncnn);
    }
}