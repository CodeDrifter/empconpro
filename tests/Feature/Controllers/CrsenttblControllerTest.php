<?php

namespace Tests\Feature\Controllers;

use App\TransactionHandler;
use Faker;
use Tests\TestCase;
use Illuminate\Support\Facades\Log;
use Propel\Runtime\Exception\PropelException;

class CrsenttblFeatureTest extends TestCase
{
    /** @test */
    public function it_can_crud_Crsenttbl() {

    //CREATE
        $faker = Faker\Factory::create();
        $trncnn = TransactionHandler::begin();

        $data = [
			'Uuid' => $faker->uuid,
			'Nmbentcrs' => $faker->text(255),
			'Dscentcrs' => $faker->text(255),
			'Prcentcrs' => $faker->numberBetween(0, 99999999999),
			'CreatedAt' => $faker->iso8601(),
			'UpdatedAt' => $faker->iso8601(),
		];
		$this
			->post(route('Crsenttbl.submit'), $data)
			->assertStatus(200)
			->assertSee('"success":true');

// UPDATE
        $update = [
			'Uuid' => $data['Uuid'],
			'Nmbentcrs' => $faker->text(255),
			'Dscentcrs' => $faker->text(255),
			'Prcentcrs' => $faker->numberBetween(0, 99999999999),
			'created_at' => null,
			'updated_at' => null,
		];
		$this
			->post(route('Crsenttbl.modify'), $update)
			->assertStatus(200)
			->assertSee('"success":true');



//DELETE
		$this
			->post(route('Crsenttbl.remove'), $update)
			->assertStatus(200)
			->assertSee('"success":true');
	}
}