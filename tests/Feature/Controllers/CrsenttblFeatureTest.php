<?php

namespace Tests\Feature\Controllers;

use App\TransactionHandler;
use Faker;
use Tests\TestCase;
use Illuminate\Support\Facades\Log;
use Propel\Runtime\Exception\PropelException;

class CrsenttblFeatureTest extends TestCase
{
    /** @test */
    public function it_can_crud_Crsenttbl() {

    //CREATE
        $faker = Faker\Factory::create();

        $data = [
			'Uuid' => $faker->uuid,
			'Nombre' => $faker->text(255),
			'Descripcion' => $faker->text(255),
			'Precio' => $faker->numberBetween(0, 99999999999),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
			'Link' => $faker->text(255),
			'Borrado' => $faker->boolean(),
		];
		$this
			->post(route('Crsenttbl.submit'), $data)
			->assertStatus(200)
			->assertSee('"success":true');

// UPDATE
        $update = [
			'Uuid' => $data['Uuid'],
			'Nombre' => $faker->text(255),
			'Descripcion' => $faker->text(255),
			'Precio' => $faker->numberBetween(0, 99999999999),
			'Creado' => null,
			'Actualizado' => null,
			'Link' => null,
			'Borrado' => $faker->boolean(),
		];
		$this
			->post(route('Crsenttbl.modify'), $update)
			->assertStatus(200)
			->assertSee('"success":true');



//DELETE
		$this
			->post(route('Crsenttbl.remove'), $update)
			->assertStatus(200)
			->assertSee('"success":true');
	}
}