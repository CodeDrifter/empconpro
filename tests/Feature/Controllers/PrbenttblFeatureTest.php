<?php

namespace Tests\Feature\Controllers;

use App\TransactionHandler;
use Faker;
use Tests\TestCase;
use Illuminate\Support\Facades\Log;
use Propel\Runtime\Exception\PropelException;

class PrbenttblFeatureTest extends TestCase
{
    /** @test */
    public function it_can_crud_Prbenttbl() {
        $faker = Faker\Factory::create();
//FOREIGNERS

//VALIDATOR
		//Uuid.required
        $data = [
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Uuid.uuid
        $data = [
            'Uuid' => "not a uuid",
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Uuid.size
        $data = [
            'Uuid' => "*************************************",
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

		//Internal.integer
        $data = [
            'Internal' => "not integer",
			'Uuid' => $faker->text(36),
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Internal.min
        $data = [
            'Internal' => "-1",
			'Uuid' => $faker->text(36),
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Internal.max
        $data = [
            'Internal' => "99999999999",
			'Uuid' => $faker->text(36),
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Internal.max
        $data = [
            'Internal' => "*************************************",
			'Uuid' => $faker->text(36),
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

		//Internal.integer
        $data = [
            'Internal' => "not integer",
			'Uuid' => $faker->text(36),
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Internal.min
        $data = [
            'Internal' => "-1",
			'Uuid' => $faker->text(36),
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Internal.max
        $data = [
            'Internal' => "99999999999",
			'Uuid' => $faker->text(36),
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Internal.max
        $data = [
            'Internal' => "*************************************",
			'Uuid' => $faker->text(36),
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

		//Video.integer
        $data = [
            'Video' => "not integer",
			'Uuid' => $faker->text(36),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Video.min
        $data = [
            'Video' => "-1",
			'Uuid' => $faker->text(36),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Video.max
        $data = [
            'Video' => "999999999999",
			'Uuid' => $faker->text(36),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Horario.date_format ISO
        $data = [
            'Horario' => date('Y-m-d'),
			'Uuid' => $faker->text(36),
			'Video' => $faker->numberBetween(0, 99999999999),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

		//Estado.integer
        $data = [
            'Estado' => "not integer",
			'Uuid' => $faker->text(36),
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Estado.min
        $data = [
            'Estado' => "-1",
			'Uuid' => $faker->text(36),
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Estado.max
        $data = [
            'Estado' => "999999999999",
			'Uuid' => $faker->text(36),
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Borrado.boolean
        $data = [
            'Borrado' => 'not boolean'
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
			'Uuid' => $faker->text(36),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Creado.date_format ISO
        $data = [
            'Creado' => date('Y-m-d'),
			'Uuid' => $faker->text(36),
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Actualizado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

        //Actualizado.date_format ISO
        $data = [
            'Actualizado' => date('Y-m-d'),
			'Uuid' => $faker->text(36),
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
        ];
        $this
            ->post(route('Prbenttbl.submit'), $data)
            ->assertStatus(200)
            ->assertSee('"success":false');

//CREATE
        $data = [
			'Uuid' => $faker->uuid,
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
		];
		$this
			->post(route('Prbenttbl.submit'), $data)
			->assertStatus(200)
			->assertSee('"success":true');

// UPDATE
        $update = [
			'Uuid' => $data['Uuid'],
			'Video' => $faker->numberBetween(0, 99999999999),
			'Horario' => $faker->iso8601(),
			'Estado' => $faker->numberBetween(0, 99999999999),
			'Borrado' => $faker->boolean(),
			'Creado' => null,
			'Actualizado' => null,
		];
		$this
			->post(route('Prbenttbl.modify'), $update)
			->assertStatus(200)
			->assertSee('"success":true');



//DELETE
		$this
			->post(route('Prbenttbl.remove'), $update)
			->assertStatus(200)
			->assertSee('"success":true');
	}
}