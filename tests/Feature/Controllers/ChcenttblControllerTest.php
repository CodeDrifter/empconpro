<?php

namespace Tests\Feature\Controllers;

use App\TransactionHandler;
use Faker;
use Tests\TestCase;
use Illuminate\Support\Facades\Log;
use Propel\Runtime\Exception\PropelException;

class ChcenttblFeatureTest extends TestCase
{
    /** @test */
    public function it_can_crud_Chcenttbl() {

    //CREATE
        $faker = Faker\Factory::create();
        $trncnn = TransactionHandler::begin();

        $data = [
			'Uuid' => $faker->uuid,
			'Idnentusr' => $faker->numberBetween(0, 9999999999),
			'Uidentusr' => $faker->text(36),
			'Idnentcrs' => $faker->numberBetween(0, 9999999999),
			'Uidentcrs' => $faker->text(36),
			'Idnmrcpag' => $faker->numberBetween(0, 99999999999),
			'CreatedAt' => $faker->iso8601(),
			'UpdatedAt' => $faker->iso8601(),
		];
		$this
			->post(route('Chcenttbl.submit'), $data)
			->assertStatus(200)
			->assertSee('"success":true');

// UPDATE
        $update = [
			'Uuid' => $data['Uuid'],
			'idnentusr' => null,
			'uidentusr' => null,
			'idnentcrs' => null,
			'uidentcrs' => null,
			'Idnmrcpag' => $faker->numberBetween(0, 99999999999),
			'created_at' => null,
			'updated_at' => null,
		];
		$this
			->post(route('Chcenttbl.modify'), $update)
			->assertStatus(200)
			->assertSee('"success":true');



//DELETE
		$this
			->post(route('Chcenttbl.remove'), $update)
			->assertStatus(200)
			->assertSee('"success":true');
	}
}