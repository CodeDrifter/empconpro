<?php

namespace Tests\Feature\Controllers;

use App\TransactionHandler;
use Faker;
use Tests\TestCase;
use Illuminate\Support\Facades\Log;
use Propel\Runtime\Exception\PropelException;

class ChcenttblFeatureTest extends TestCase
{
    /** @test */
    public function it_can_crud_Chcenttbl() {

    //CREATE
        $faker = Faker\Factory::create();

        $data = [
			'Uuid' => $faker->uuid,
			'Creado' => $faker->iso8601(),
			'Actualizado' => $faker->iso8601(),
			'Borrado' => $faker->boolean(),
		];
		$this
			->post(route('Chcenttbl.submit'), $data)
			->assertStatus(200)
			->assertSee('"success":true');

// UPDATE
        $update = [
			'Uuid' => $data['Uuid'],
			'Creado' => null,
			'Actualizado' => null,
			'Borrado' => $faker->boolean(),
		];
		$this
			->post(route('Chcenttbl.modify'), $update)
			->assertStatus(200)
			->assertSee('"success":true');



//DELETE
		$this
			->post(route('Chcenttbl.remove'), $update)
			->assertStatus(200)
			->assertSee('"success":true');
	}
}