import Vue from 'vue'
import Router from 'vue-router'
import { scrollBehavior } from '~/utils'

Vue.use(Router)

const page = path => () => import(`~/pages/${path}`).then(m => m.default || m)

const routes = [
  { path: '/', name: 'welcome', component: page('welcome.vue') },
  { path: '/mensaje/:tipo/:mensaje', name: 'welcome.mensaje', component: page('welcome.vue') },

  { path: '/login', name: 'login', component: page('auth/login.vue') },
  { path: '/login/:vivo/:uuid', name: 'login.vivo.curso', component: page('auth/login.vue') },
  { path: '/register', name: 'register', component: page('auth/register.vue') },
  { path: '/register/:uuid', name: 'register.curso', component: page('auth/register.vue') },
  { path: '/register/:vivo/:uuid', name: 'register.vivo.curso', component: page('auth/register.vue') },

  { path: '/password/reset', name: 'password.request', component: page('auth/password/email.vue') },
  { path: '/password/reset/:token', name: 'password.reset', component: page('auth/password/reset.vue') },
  { path: '/email/verify/:id', name: 'verification.verify', component: page('auth/verification/verify.vue') },
  { path: '/email/resend', name: 'verification.resend', component: page('auth/verification/resend.vue') },


  { path: '/cursos', name: 'lista', component: page('cursos/lista.vue') },
  { path: '/cursos/pagados', name: 'pagados', component: page('cursos/pagados.vue') },
  { path: '/curso/:uuid', name: 'presentacion', component: page('cursos/presentacion.vue') },
  { path: '/curso/pregunta/:video/:uuid', name: 'formulario', component: page('cursos/vivo/form.vue') },
  { path: '/curso/vivo/:uuid', name: 'vivo', component: page('cursos/vivo/video.vue') },
  { path: '/curso/videos/:uuid', name: 'curso', component: page('cursos/video.vue') },

  { path: '/admin/curso/crear', name: 'curso.crear', component: page('admin/curso/crear.vue') },
  { path: '/admin/curso/leer', name: 'curso.leer', component: page('admin/curso/leer.vue') },
  { path: '/admin/curso/modificar', name: 'curso.modificar', component: page('admin/curso/modificar.vue') },
  { path: '/admin/curso/borrar', name: 'curso.eliminar', component: page('admin/curso/borrar.vue') },
  { path: '/admin/curso/users', name: 'curso.users', component: page('admin/curso/users.vue') },

  { path: '/admin/checkout/crear', name: 'checkout.crear', component: page('admin/checkout/crear.vue') },
  { path: '/admin/checkout/leer', name: 'checkout.leer', component: page('admin/checkout/leer.vue') },
  { path: '/admin/checkout/modificar', name: 'checkout.modificar', component: page('admin/checkout/modificar.vue') },
  { path: '/admin/checkout/borrar', name: 'checkout.eliminar', component: page('admin/checkout/borrar.vue') },

  { path: '/admin/user/crear', name: 'user.crear', component: page('admin/user/crear.vue') },
  { path: '/admin/user/leer', name: 'user.leer', component: page('admin/user/leer.vue') },
  { path: '/admin/user/modificar', name: 'user.modificar', component: page('admin/user/modificar.vue') },
  { path: '/admin/user/borrar', name: 'user.eliminar', component: page('admin/user/borrar.vue') },

  { path: '/settings',
    component: page('settings/index.vue'),
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: page('settings/profile.vue') },
      { path: 'password', name: 'settings.password', component: page('settings/password.vue') }
    ] }
]

export function createRouter () {
  return new Router({
    routes,
    scrollBehavior,
    mode: 'history'
  })
}
