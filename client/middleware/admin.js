export default ({ store, redirect }) => {
  if (!store.getters['auth/check']) {
    return redirect('/login')
  } else if (store.getters['auth/user'].rol !== 'admin') {
    return redirect('/')
  }
}
