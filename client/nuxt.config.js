require('dotenv').config()

module.exports = {
  // mode: 'spa',

  srcDir: __dirname,

  env: {
    apiUrl: process.env.API_URL || process.env.APP_URL + '/api',
    appName: process.env.APP_NAME || 'Nuxt',
    appLocale: process.env.APP_LOCALE || 'es'
  },

  server: {
    port: 50000, // default: 3000
    host: 'emprendedoresconproposito.online' // default: localhost
  },

  head: {
    title: process.env.APP_NAME,
    titleTemplate: process.env.APP_NAME,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/logo.ico' }
    ]
  },

  loading: { color: '#1a174d' },

  router: {
    middleware: ['locale', 'check-auth']
  },

  css: [
    { src: '~assets/sass/app.scss', lang: 'scss' },
    { src: '~assets/vendor/nucleo/css/nucleo.css', lang: 'css' },
    { src: '~assets/vendor/font-awesome/css/font-awesome.css', lang: 'css' },
    { src: '~assets/scss/argon.scss', lang: 'scss' }
  ],

  plugins: [
    '~components/global',
    '~plugins/i18n',
    '~plugins/vform',
    '~plugins/axios',
    '~plugins/fontawesome',
    '~plugins/argon-kit',
    '~plugins/element-ui',
    '~plugins/globalDirectives',
    '~plugins/globalComponents',
    // '~plugins/nuxt-client-init',
    { src: '~plugins/toastr', mode: 'client' },
    { src: '~plugins/bootstrap', mode: 'client' }
  ],

  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/router',
    '@nuxtjs/axios',
    'nuxt-facebook-pixel-module'
  ],

  facebook: {
    track: 'ViewContent',
    pixelId: '606690429889135',
    disabled: false
  },

  axios: {
    baseURL: process.env.AXIOS_BASE_URL || 'https://myurl.com',
    common: {
      'Accept': 'XMLHttpRequest'
    }
  },

  bootstrapVue: {
    componentPlugins: [
      'ModalPlugin'
    ]
  },

  build: {
    extractCSS: true
  }
}
