import Vue from 'vue'
import Videojs from 'video.js'
import 'video.js/dist/video-js.css'
import '@videojs/themes/dist/forest/index.css';


export default () => {
  Vue.use(Videojs)
}

