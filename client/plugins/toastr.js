import Vue from 'vue'
import VueToastr from 'vue-toastr'

export default ({ app}) => {
  if (process.browser) {
    Vue.use(VueToastr)
  }
}
