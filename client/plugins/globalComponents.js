import Vue from 'vue'
import Card from '../components/Card.vue'
import Icon from '../components/Icon.vue'
import BaseInput from '../components/BaseInput.vue'
import BaseCheckbox from '../components/BaseCheckbox.vue'
import BaseButton from '../components/BaseButton.vue'

Vue.component(Card.name, Card)
Vue.component(Icon.name, Icon)
Vue.component(BaseInput.name, BaseInput)
Vue.component(BaseCheckbox.name, BaseCheckbox)
Vue.component(BaseButton.name, BaseButton)
